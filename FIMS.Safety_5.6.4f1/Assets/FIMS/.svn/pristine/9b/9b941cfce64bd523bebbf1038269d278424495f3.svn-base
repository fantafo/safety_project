﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class AverageQuaternion
{
    Quaternion[] queue;
    int index;
    int length;

    public Quaternion last
    {
        get
        {
            if (length == 0)
                return Quaternion.identity;
            return queue[(index + queue.Length - 1) % queue.Length];
        }
    }

    public AverageQuaternion(int count)
    {
        queue = new Quaternion[count];
        Reset();
    }

    public void Push(Quaternion quart)
    {
        queue[index++] = quart;

        length = Mathf.Max(index, length);
        if (index == queue.Length)
            index = 0;
    }

    public Quaternion Average()
    {
        Quaternion avrg = Quaternion.identity;
        int len = (queue.Length == length) ? length : index;
        float ratio = 1f / len;

        for (int i = 0; i < len; i++)
        {
            avrg *= Quaternion.Lerp(Quaternion.identity, queue[i], ratio);
        }

        return avrg;
    }

    public void Reset()
    {
        index = 0;
        length = 0;
    }
    public void Set(Quaternion val)
    {
        Reset();
        Push(val);
    }


    public static AverageQuaternion operator +(AverageQuaternion av3, Quaternion val)
    {
        av3.Push(val);
        return av3;
    }
    public static implicit operator Quaternion(AverageQuaternion av3)
    {
        return av3.Average();
    }
}
