﻿Shader "FTF/Character"
{
    Properties
    {
		_Color("Main Color", Color) = (1,1,1,1)
        [NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
		_ShadMin("Shadow Min", range(0,1)) = 0.7
		_ShadMax("Shadow Max", range(0,1)) = 0.9
		_Bri("Brightness", range(0,2)) = 1
    }
    SubShader
    {
        Pass
        {
            // indicate that our pass is the "base" pass in forward
            // rendering pipeline. It gets ambient and main directional
            // light data set up; light direction in _WorldSpaceLightPos0
            // and color in _LightColor0
            Tags {"LightMode"="ForwardBase"}
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc" // for UnityObjectToWorldNormal
            #include "UnityLightingCommon.cginc" // for _LightColor0

            struct v2f
            {
                float2 uv : TEXCOORD0;
                fixed4 diff : COLOR0; // diffuse lighting color
                float4 vertex : SV_POSITION;
            };

			fixed _ShadMin;
			fixed _ShadMax;
			fixed _Bri;

            v2f vert (appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;

				// 라이트 계산
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));

				// enough가 0이상이면 무조건 nl값을 표현된다.
				fixed enough = max(0, nl - _ShadMax);
				nl = ((nl - enough) / _ShadMax) * (_ShadMax - _ShadMin) + _ShadMin + enough;

                // factor in the light color
                o.diff = nl * _LightColor0;
                return o;
            }
            
            sampler2D _MainTex;
			fixed4 _Color;

            fixed4 frag (v2f i) : SV_Target
            {
                // sample texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // multiply by lighting
                col *= i.diff;
				return fixed4((_Color * col).rgb * _Bri, 1);
            }
            ENDCG
        }
    }
}
