﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

namespace FTF
{
    /// <summary>
    /// PlayerInfo정보를 이용하여 모델을 구상한다. 이곳에서 머리, 몸통의 모델을 생성하고,
    /// 서버상의 위치, 회전 정보를 모델에 반영하기도 한다.
    /// </summary>
    public class PlayerModel : SMonoBehaviour
    {
        public PlayerInfo info;

        public Transform headContainer;
        public Transform bodyContainer;
        public Transform handContainer;
        public Transform floatingContainer;
        public DummyLaser laser;

        public float speed = 1; // 트래킹 정보가 적용된 곳에서 따라가기위한 속도
        public float tall = 1.6f; // 유저키 값

        public Vector3 headOffset;

        internal Vector3 headPosition = new Vector3(0, 0);
        internal bool useHand = false;
        internal Vector3 handPosition = new Vector3(0.17f, -0.5f, 0.26f);
        internal Quaternion handRotation = Quaternion.identity;
        internal Vector3 reticlePosition = Vector3.zero;
        internal bool UseReticle = false;/*
#if FTF_OBSERVER
        AverageQuaternion headRoationCalculator = new AverageQuaternion(30);
        internal Quaternion headRotation
        {
            get { return headRoationCalculator; }
            set
            {
                headRoationCalculator += headRotation;
            }
        }
#else
#endif
*/
        internal Quaternion headRotation = Quaternion.identity;


        internal PlayerHead head;
        internal PlayerBody body;

        private bool modelInitialized;
        private int headModelType;
        private int bodyModelType;

        public int HeadModelType
        {
            get { return headModelType; }
            set
            {
                if (headModelType != value)
                {
                    headModelType = value;
                    if (head)
                    {
                        Destroy(head.gameObject);
                        head = null;
                    }
                    if (headModelType > 0)
                    {
                        GameObject prefab;
                        if (info.instance.IsRoomMaster)
                        {
                            prefab = Resources.Load<GameObject>("Character/Head/0");
                        }
                        else
                        {
                            prefab = Resources.Load<GameObject>("Character/Head/" + headModelType);
                        }

                        var go = Instantiate(prefab, headContainer.transform);
                        go.transform.ResetLocal();
                        head = go.GetComponent<PlayerHead>();

                        if (floatingContainer != null)
                            floatingContainer.gameObject.SetActive(true);

                        if (!PlayerInstance.IsObserver && info.IsMainPlayer)
                        {
                            List<Collider> list = ListPool<Collider>.Take();
                            go.GetComponentsInChildren<Collider>(list);
                            for (int i = 0; i < list.Count; i++)
                                list[i].enabled = false;
                            list.Release();
                        }
                    }
                    else
                    {
                        if (floatingContainer != null)
                            floatingContainer.gameObject.SetActive(false);
                    }
                }
            }
        }
        public int BodyModelType
        {
            get { return bodyModelType; }
            set
            {
                if (bodyModelType != value)
                {
                    bodyModelType = value;
                    if (body)
                    {
                        Destroy(body.gameObject);
                        body = null;
                    }
                    if (bodyModelType > 0)
                    {
                        var prefab = Resources.Load<GameObject>("Character/Body/" + bodyModelType);
                        var go = Instantiate(prefab, bodyContainer.transform);
                        go.transform.ResetLocal();
                        body = go.GetComponent<PlayerBody>();

                        if (!PlayerInstance.IsObserver && info.IsMainPlayer)
                        {
                            List<Collider> list = ListPool<Collider>.Take();
                            go.GetComponentsInChildren<Collider>(list);
                            for (int i = 0; i < list.Count; i++)
                                list[i].enabled = false;
                            list.Release();
                        }

                        if (info.instance.IsRoomMaster)
                        {
                            var lineRenderer = laser.GetComponent<LineRenderer>();
                            lineRenderer.startColor = new Color(1, .3f, .3f, lineRenderer.startColor.a);
                            lineRenderer.endColor = new Color(1, .3f, .3f, lineRenderer.endColor.a);

                            laser.Reticle.GetComponent<Renderer>().material.color = new Color(1, .3f, .3f, 1);
                        }
                    }
                }
            }
        }

        private void OnDisable()
        {
            HeadModelType = 0;
            BodyModelType = 0;
        }
        private void Update()
        {
            float ratio = speed * Time.deltaTime;
            if (info.IsMainPlayer && !PlayerInstance.IsObserver)
                ratio = 1;

            if (!modelInitialized)
            {
                modelInitialized = true;
                ratio = 1;
            }

            // head transforming
            {
                headContainer.transform.localRotation = Quaternion.Lerp(headContainer.transform.localRotation, headRotation, ratio);
                Vector3 headPos = new Vector3(0, tall, 0) - headOffset + (headContainer.transform.localRotation * headOffset);
                headContainer.transform.localPosition = Vector3.Lerp(headContainer.transform.localPosition, headPos, ratio);

                if (head != null && floatingContainer != null)
                {
                    floatingContainer.position = (transform.position + new Vector3(0, tall) + head.topPosition.localPosition);
                }
            }

            // body transforming
            {
                bodyContainer.transform.localPosition = Vector3.Lerp(bodyContainer.transform.localPosition, new Vector3(0, tall, 0), ratio);
            }

            // Hand
            handContainer.transform.localPosition = Vector3.Lerp(handContainer.transform.localPosition, handPosition + new Vector3(0, tall, 0), ratio);
            handContainer.transform.localRotation = Quaternion.Lerp(handContainer.transform.localRotation, handRotation, ratio);

            // Laser Transform
            if (UseReticle != laser.gameObject.activeSelf)
            {
                laser.gameObject.SetActive(UseReticle);
            }
        }
    }
}