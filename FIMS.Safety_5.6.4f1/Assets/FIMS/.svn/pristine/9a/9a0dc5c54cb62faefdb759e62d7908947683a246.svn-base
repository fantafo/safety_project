﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenTextExtension
{
    private static readonly Action<Text, float> _changeTextLineSpacing = __changeTextLineSpacing;
    private static void __changeTextLineSpacing(Text text, float val) { text.lineSpacing = val; }
    public static STweenState twnLineSpacing(this Text text, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Text>, Text, float>(text, text.lineSpacing, to, duration, _changeTextLineSpacing);
    }
    public static STweenState twnLineSpacing(this Text text, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Text>, Text, float>(text, from, to, duration, _changeTextLineSpacing);
    }


    private static readonly Action<Text, int> _changeTextFontSize = __changeTextFontSize;
    private static void __changeTextFontSize(Text text, int val) { text.fontSize = val; }
    public static STweenState twnFontSize(this Text text, int to, int duration)
    {
        return STween.Play<ActionIntObject<Text>, Text, int>(text, text.fontSize, to, duration, _changeTextFontSize);
    }
    public static STweenState twnFontSize(this Text text, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<Text>, Text, int>(text, from, to, duration, _changeTextFontSize);
    }


    private static readonly Action<Text, int> _changeTextResizeTextMaxSize = __changeTextResizeTextMaxSize;
    private static void __changeTextResizeTextMaxSize(Text text, int val) { text.resizeTextMaxSize = val; }
    public static STweenState twnResizeTextMaxSize(this Text text, int to, int duration)
    {
        return STween.Play<ActionIntObject<Text>, Text, int>(text, text.resizeTextMaxSize, to, duration, _changeTextResizeTextMaxSize);
    }
    public static STweenState twnResizeTextMaxSize(this Text text, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<Text>, Text, int>(text, from, to, duration, _changeTextResizeTextMaxSize);
    }


    private static readonly Action<Text, int> _changeTextResizeTextMinSize = __changeTextResizeTextMinSize;
    private static void __changeTextResizeTextMinSize(Text text, int val) { text.resizeTextMinSize = val; }
    public static STweenState twnResizeTextMinSize(this Text text, int to, int duration)
    {
        return STween.Play<ActionIntObject<Text>, Text, int>(text, text.resizeTextMinSize, to, duration, _changeTextResizeTextMinSize);
    }
    public static STweenState twnResizeTextMinSize(this Text text, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<Text>, Text, int>(text, from, to, duration, _changeTextResizeTextMinSize);
    }



}
#endif