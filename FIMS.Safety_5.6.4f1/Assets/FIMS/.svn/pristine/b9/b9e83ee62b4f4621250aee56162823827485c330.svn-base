﻿using FTF.CommandAuto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// CommandReceiver가 사용하기 어려운 경우 Unity의 SendMessage와 비슷한 용도로 사용할 수 있게 제공하는 클래스다.
    /// [OnCommandListener]를 사용한 메서드의 경우에만 SendMessage같이 사용할 수 있게된다.
    /// 또한 OnCommandListener로 지정된 메서드의 변수는 primity 타입 혹은 Vector, Quaternion, Rect, Bounds만 사용할 수 있다.
    /// </summary>
    public class AutoCommander : SMonoBehaviour
    {
        public AutoCommandReceiverImpl impl;

        protected virtual void Awake()
        {
            impl = new AutoCommandReceiverImpl(this);
        }

        protected virtual void OnEnable()
        {
            impl.Enable();
        }

        protected virtual void OnDisable()
        {
            impl.Disable();
        }

        public virtual void Broadcast(string methodName, params object[] param)
        {
            impl.Broadcast(methodName, param);
        }

        public virtual void Send(PlayerInstance player, string methodName, params object[] param)
        {
            impl.Send(player, methodName, param);
        }
    }
}
