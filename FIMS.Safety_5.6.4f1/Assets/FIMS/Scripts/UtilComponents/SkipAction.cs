﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    public class SkipAction : SMonoBehaviour
    {

        public Text counter;
        public int duration;

        bool processing;

        public void Skip()
        {
            if (processing) return;
            processing = true;

            gameObject.SetActive(true);
            StartCoroutine(Skipping());
        }


        private IEnumerator Skipping()
        {
            for (int i = 0; i < duration; i++)
            {
                counter.text = (duration - i).ToString();
                yield return Waits.Wait(1);
            }

            GameObject.Find("SafetyTrainingSceneManager").GetComponent<SafetyTrainingSceneManager>()._isSkip = true;



            processing = false;
            gameObject.SetActive(false);
        }
    }

}