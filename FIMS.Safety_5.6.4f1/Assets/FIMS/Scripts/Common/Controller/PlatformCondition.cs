﻿using System.Collections;
using UnityEngine;
using UnityEngine.VR;

[Description("VR 플렛폼 상태에 따라서 대상 오브젝트를 활성화한다.")]
public class PlatformCondition : SMonoBehaviour
{
    public Object target;
    public bool Oculus;
    public bool Daydream;
    public bool Cardboard;

    void Start()
    {
        bool active = false;
        active |= (Oculus && VRInput.isOculus);
        active |= (Daydream && VRInput.isDaydream);
        active |= (Cardboard && VRInput.isCardboard);

        if (target)
        {
            if (target is GameObject)
            {
                ((GameObject)target).SetActive(active);
            }
            else if(target is Behaviour)
            {
                ((Behaviour)target).enabled = active;
            }
        }
    }
}