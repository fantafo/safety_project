﻿using System;
using UnityEngine;
using System.Collections;
using Pvr_UnitySDKAPI;
using FTF;

/*Pvr_ControllerDemo를 복사해서 작성
 * 컨트롤러를 사용한다면 실행됩니다.
 * 아이커서 혹은 headsetcontroller일 경우 
 * FTFReticlePvrPointer를 사용합니다.
 */
public class FTFPvr_Controller : MonoBehaviour
{
    public GameObject HeadSetController;
    
    private Ray ray;
    private GameObject currentController;

    private Transform lastHit;
    private Transform currentHit;

    private bool noClick;

    // Use this for initialization
    void Start()
    {
        ray = new Ray();
        Pvr_ControllerManager.PvrServiceStartSuccessEvent += ServiceStartSuccess;
        Pvr_ControllerManager.SetControllerStateChangedEvent += ControllerStateListener;
        Pvr_ControllerManager.ControllerStatusChangeEvent += CheckControllerStateForGoblin;
        
        if (Pvr_UnitySDKManager.SDK != null)
        {
            if (Pvr_UnitySDKManager.pvr_UnitySDKSensor != null)
            {
                Pvr_UnitySDKManager.pvr_UnitySDKSensor.ResetUnitySDKSensor();
            }
            else
            {
                Pvr_UnitySDKManager.SDK.pvr_UnitySDKEditor.ResetUnitySDKSensor();
            }

        }
        //ray.origin = transform.position;
    }

    void OnDestroy()
    {
        Pvr_ControllerManager.PvrServiceStartSuccessEvent -= ServiceStartSuccess;
        Pvr_ControllerManager.SetControllerStateChangedEvent -= ControllerStateListener;
        Pvr_ControllerManager.ControllerStatusChangeEvent -= CheckControllerStateForGoblin;
    }

    // Update is called once per frame
    void Update()
    {

    }



    private void ServiceStartSuccess()
    {
        if (Controller.UPvr_GetControllerState(0) == ControllerState.Connected ||
            Controller.UPvr_GetControllerState(1) == ControllerState.Connected)
        {
            //HeadSetController.SetActive(false);
        }

        HeadSetController.SetActive(true);
    }

    private void ControllerStateListener(string data)
    {

        if (Controller.UPvr_GetControllerState(0) == ControllerState.Connected ||
            Controller.UPvr_GetControllerState(1) == ControllerState.Connected)
        {
            //HeadSetController.SetActive(false);
        }
        HeadSetController.SetActive(true);
    }

    private void CheckControllerStateForGoblin(string state)
    {
        HeadSetController.SetActive(!Convert.ToBoolean(Convert.ToInt16(state)));
    }

}
