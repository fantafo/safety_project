using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class DummyDaydreamController : AbsDummyController
{
    public Color touchPadColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1);
    public Color appButtonColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1);
    public Color systemButtonColor = new Color(20f / 255f, 20f / 255f, 20f / 255f, 1);
    public Vector4 controllerInfo = new Vector4(1, 0, 0, 0); // conAlpha / tpScale / App / Sys
    public Vector4 touchInfo = new Vector4(0, 0, 0, 0); // tpX / tpY / tpSize / bat

    private Renderer controllerRenderer;
    private MaterialPropertyBlock materialPropertyBlock;

    public override float Alpha { get { return controllerInfo.x; } set { controllerInfo.x = value; } }
    public override float BackButtonPress { get { return controllerInfo.z; } set { controllerInfo.z = value; } }
    public override float SystemButtonPress { get { return controllerInfo.w; } set { controllerInfo.w = value; } }
    public override float Battery { get { return touchInfo.w; } set { touchInfo.w = value; } }

    public override float TouchPointX { get { return touchInfo.x; } set { touchInfo.x = value; } }
    public override float TouchPointY { get { return touchInfo.y; } set { touchInfo.y = value; } }
    public override float TouchPointAlpha { get { return touchInfo.z; } set { touchInfo.z = value; controllerInfo.y = 0; } }
    public override float TouchpadAlpha { get { return touchInfo.z; } set { touchInfo.z = value; controllerInfo.y = 1; } }


    protected override void Initialize()
    {
        if (controllerRenderer == null)
        {
            controllerRenderer = GetComponent<Renderer>();
        }
        if (materialPropertyBlock == null)
        {
            materialPropertyBlock = new MaterialPropertyBlock();
        }

        materialPropertyBlock.SetColor("_GvrAppButtonColor", appButtonColor);
        materialPropertyBlock.SetColor("_GvrSystemButtonColor", systemButtonColor);
        materialPropertyBlock.SetColor("_GvrTouchPadColor", touchPadColor);

        materialPropertyBlock.SetColor("_GvrTouchInfo", touchInfo);
        materialPropertyBlock.SetVector("_GvrControllerAlpha", controllerInfo);

        controllerRenderer.SetPropertyBlock(materialPropertyBlock);
    }

    public override void Validate()
    {
        materialPropertyBlock.SetColor("_GvrTouchInfo", touchInfo);
        materialPropertyBlock.SetVector("_GvrControllerAlpha", controllerInfo);
        controllerRenderer.SetPropertyBlock(materialPropertyBlock);
    }
}

