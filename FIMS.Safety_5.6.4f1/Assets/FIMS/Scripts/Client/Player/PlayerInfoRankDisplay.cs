﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class PlayerInfoRankDisplay : MonoBehaviour
{
    public PlayerInfo info;
    public GameObject nameTxt;

    int test = 0;

    private void OnEnable()
    {
        StartCoroutine(ChangeChecker());
    }
    private IEnumerator ChangeChecker()
    {
        Text text = GetComponent<Text>();

        while (true)
        {
            if (GameObject.Find("Warm-upQuiz_PR_360_01"))
                break;
            yield return Waits.Wait(1);
        }

        nameTxt.transform.Translate(new Vector3(0, -0.037f, 0));

        while (true)
        {
            string userRank = (info != null && info.instance != null) ?
                info.instance.ranking.ToString() + "등" : "0등";

            if (userRank != text.text)
            {
                text.text =  info.instance.ranking.ToString() + "등";
            }
            yield return Waits.Wait(1);
        }
    }
}
