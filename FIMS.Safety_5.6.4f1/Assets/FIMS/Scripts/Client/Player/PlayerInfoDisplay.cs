﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class PlayerInfoDisplay : MonoBehaviour
{
    public PlayerInfo info;

    int test = 0;

    private void OnEnable()
    {
        StartCoroutine(ChangeChecker());
    }
    private IEnumerator ChangeChecker()
    {
        Text text = GetComponent<Text>();
        while (true)
        {
            string userName = (info != null && info.instance != null) 
                ? info.instance.userName 
                : "임시이름";

            if(userName != text.text)
            {
                if (userName.IsEmpty())
                    text.text = "임시이름";
                else
                    text.text = info.instance.userName;
            }
            yield return Waits.Wait(1);
        }
    }
}
