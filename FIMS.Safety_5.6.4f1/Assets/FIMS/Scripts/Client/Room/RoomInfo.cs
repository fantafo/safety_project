﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace FTF
{
    /// <summary>
    /// 클라이언트에게 유일한 방의 정보를 가리킨다.
    /// 현재 접속해있는 방을 가리키기도 한다.
    /// </summary>
    public class RoomInfo : SMonoBehaviour, IReceivePacket
    {
        /// <summary>
        /// 현재 접속해있는 방의 정보를 가리킨다.
        /// </summary>
        public static RoomInfo main { get; private set; }

        ILog log = SLog.GetLogger("RoomInfo");
        //================================================================================================
        //
        //                                       Member Variables                                  
        //
        //================================================================================================

        public long num; // 서버에서 구별하는 방의 번호
        public string roomName; // 방의 이름
        public int maxUser; // 방에 접속가능한 최대 인원 수
        public int mapID; // 방에서 사용할 맵 번호
        public int mapStage; //safetymanager에서 동영상 재생번호 

        public int currentUserCount; // 실제 접속한 유저의 수. (목록에만 있고 접속하지 않은 유저와는 다르다)
        public PlayerInfo[] players; // 플레이어 리스트. (접속하지 않았거나 할당만 된 대상도 포함된 리스트)

        public UnityEvent onRecevieRoomInfo; // 룸 정보가 갱신되면 호출된다.
        public UnityEvent onLoadStart; // 룸의 세션이 시작되면 호출된다.

        // Safety 관련
        public int _contentIndex = 0;   // 연구실 별 컨텐츠
        //================================================================================================
        //
        //                                       Unity LifeCycle
        //
        //================================================================================================

        private void Awake()
        {
            if (main)
            {
                Destroy(main);
                return;
            }
            main = this;
        }


        //================================================================================================
        //
        //                                          Network
        //
        //================================================================================================

        public void OnReceivePacket(ServerOpcode code, BinaryReader reader)
        {
            switch (code)
            {
                //=====================================================
                // 룸 정보를 읽어옵니다.
                case ServerOpcode.Room_Info:
                    {
                        num = reader.ReadL();
                        roomName = reader.ReadS();
                        maxUser = reader.ReadC();
                        mapID = reader.ReadH();
                        mapStage = reader.ReadH();

                        if (players == null)
                            players = new PlayerInfo[maxUser];
                        else if (players.Length != maxUser)
                            Array.Resize(ref players, maxUser);

                        int userCount = reader.ReadC();
                        currentUserCount = 0;
                        for (int i = 0; i < players.Length && i < userCount; i++)
                        {
                            // 접속된 대상만 읽어온다
                            if (reader.ReadB())
                            {
                                currentUserCount++;

                                int instanceID = -1;
                                Vector3 headPos;
                                Quaternion headRot;
                                bool useHand;
                                Vector3 handPos = Vector3.zero;
                                Quaternion handRot = Quaternion.identity;
                                bool useReticle = false;
                                Vector3 retPos = Vector3.zero;

                                instanceID = reader.ReadD();
                                headPos = reader.ReadNormal();
                                headRot = Quaternion.Euler(reader.ReadEuler32());
                                useHand = reader.ReadB();
                                short ranking = reader.ReadH();         // 랭킹 추가 관련
                                //퀴즈 선택 정보 전달
                                short quiz = reader.ReadH();
                                if (useHand)
                                {
                                    handPos = reader.ReadNormal();
                                    handRot = Quaternion.Euler(reader.ReadEuler24());

                                    useReticle = reader.ReadB();
                                    if (useReticle)
                                    {
                                        retPos = reader.ReadVector3();
                                    }
                                }

                                try
                                {
                                    PlayerInstance player = null;
                                    if (instanceID != -1)
                                    {
                                        player = PlayerManager.GetPlayer(instanceID);
                                    }

                                    // instanceID가 자신의 id일 경우
                                    if (PlayerInstance.main.instanceId == instanceID)
                                    {
                                        // PlayerInstance의 ranking을 자신에게 적용
                                        if (null != PlayerInstance.lookMain)
                                        {
                                            PlayerInstance.lookMain.ranking = ranking;
                                        }
                                    }

                                    // 플레이어가 존재하던 중에 사라졌다면
                                    if (player == null && players[i] != null)
                                    {
                                        players[i].gameObject.Destroy();
                                        players[i] = null;
                                    }

                                    if (PlayerInfoManager.main != null)
                                    {
                                        // 플레이어 모델이 없다면 생성한다.
                                        if (players[i] == null)
                                        {
                                            players[i] = PlayerInfoManager.main.TakePlayer();
                                        }

                                        // 플레이어정보가 현재 들어온 정보와 다르다면
                                        // 플레이어 정보를 교환해준다.
                                        if (players[i].instance != player)
                                        {
                                            // 플레이어가 이미 할당 돼 있는 상태라면?
                                            if (player.info != null && player.info.instance == player)
                                            {
                                                // 만약 리스트에 할당된 PlayerInfo가 있다면 각각의 위치를 바꿔준다.
                                                int indexOf = Array.IndexOf(players, player.info);
                                                if (indexOf != -1)
                                                {
                                                    var temp = players[i];
                                                    players[i] = players[indexOf];
                                                    players[indexOf] = temp;

                                                    continue;
                                                }
                                            }

                                            // index 위치의 플레이어에게 player를 할당한다.
                                            players[i].instance = player;
                                            player.info = players[i];
                                            player.info.gameObject.SetActive(true);
                                        }

                                        // 플레이어정보가 들어와 있는 상태라면
                                        if (player.info != null && player.info.model != null)
                                        {
                                            player.roomIndex = i;
                                            //퀴즈 정보는 캐릭터에 붙기 때문에 여기에
                                            player.info.instance.quiz = quiz;
                                            player.info.instance.ranking = ranking;
                                            player.info.model.headPosition = headPos;
                                            player.info.model.headRotation = headRot;
                                            player.info.model.useHand = useHand;
                                            player.info.model.handPosition = handPos;
                                            player.info.model.handRotation = handRot;
                                            player.info.model.UseReticle = useReticle;
                                            player.info.model.reticlePosition = retPos;
                                            

                                            if (player.info.model.headRotation.w == 0)
                                                player.info.model.headRotation = Quaternion.identity;
                                            if (player.info.model.handRotation.w == 0)
                                                player.info.model.handRotation = Quaternion.identity;
                                        }
                                    }
                                }catch(Exception e)
                                {
                                    e.PrintStackTrace();
                                }
                            }
                        }

                        if (onRecevieRoomInfo != null)
                            onRecevieRoomInfo.Invoke();

                        if (Networker.State < NetworkState.Room)
                            Networker.State = NetworkState.Room;
                    }
                    break;

                //=====================================================
                // 방이 시작됐기때문에 읽기 시작하라고 알려줍니다.
                case ServerOpcode.Room_LoadStart:
                    {
                        //Networker.State = NetworkState.Loading;
                        //if (onLoadStart != null)
                        //{
                        //    onLoadStart.Invoke();
                        //}
                        Networker.State = NetworkState.Loading;
                        // 해당 값에 따라 Safety 매니저가 특정 콘텐츠를 실행. 1 생명, 2 화학, 3 전기
                        if (Application.platform == RuntimePlatform.Android)
                        {
                            int index = reader.ReadH();
                            
                            _contentIndex = index;
                            
                            log.Debug("Room_LoadStart: index" + _contentIndex);
                        }
                        var scene = SceneManager.GetActiveScene();
                        if (null != onLoadStart && scene.buildIndex != 1)
                        {
                            onLoadStart.Invoke();
                        }
                        
                    }
                    break;

                case ServerOpcode.Room_Exit:
                    {

                    }
                    break;
            }
        }
    }
}
