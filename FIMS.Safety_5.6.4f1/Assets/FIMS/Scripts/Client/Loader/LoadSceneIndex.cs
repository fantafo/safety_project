﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    /// <summary>
    /// LoadBase를 기반으로 하여 씬의 index번호를 이용하여 씬을 읽어온다.
    /// </summary>
    public class LoadSceneIndex : LoadBase
    {
        public int levelIndex;

        public void Load(int levelIndex)
        {
            this.levelIndex = levelIndex;
            Load();
        }
        public void Load()
        {
            LoadStart();
        }

        protected override AsyncOperation LoadAsync()
        {
            FimsCommonData.PushFlag(FimsCommonData.FLAG_LOADING);
            return SceneManager.LoadSceneAsync(levelIndex, LoadSceneMode.Single);
        }

        protected override bool LoadValidate()
        {
            return true;
        }
    }
}