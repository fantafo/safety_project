﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// byte 배열을 보기쉽게 string으로 변환해줍니다.
    /// </summary>
    public class HexString
    {
        public static string ToString(byte[] data)
        {
            return ToString(data, 0, data.Length);
        }
        public static string ToString(byte[] data, int length)
        {
            return ToString(data, 0, length);
        }
        public static string ToString(byte[] data, int offset, int length)
        {
            int len, i, j;
            char c;

            StringBuilder sb = new StringBuilder();
            for (i = 0; i < length; i += 16)
            {
                sb.AppendFormat("{0:X4}:  ", i);
                len = Math.Min(length - i, 16);
                for (j = 0; j < 16; j++)
                {
                    if (j < len)
                    {
                        sb.AppendFormat("{0:X2} ", data[i + offset + j]);
                    }
                    else
                    {
                        sb.Append("   ");
                    }
                    if (j % 4 == 3)
                        sb.Append(" ");
                    if (j == 7)
                        sb.Append(" ");
                }

                sb.Append("     ");
                for (j = 0; j < len; j++)
                {
                    c = (char)data[i + offset + j];
                    if (0x20 <= c && c <= 0x7F)
                    {
                        sb.Append(c);
                    }
                    else
                    {
                        sb.Append('.');
                    }
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}
