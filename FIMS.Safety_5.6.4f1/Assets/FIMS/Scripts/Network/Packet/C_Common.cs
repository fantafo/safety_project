﻿using FTF.VoIP;
using System.Collections.Generic;
using UnityEngine;

namespace FTF.Packet
{
    /// <summary>
    /// 초기화, 백그라운드 등의 뚜렷하게 눈에 보이지 않고 공통적, 일반적으로
    /// 사용되는 패킷을 모아둔 클래스
    /// </summary>
    public class C_Common
    {

        public static byte[] Version(string identity, int clientVersion, bool isDummy)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_ClientVersion);
            w.WriteS(identity);
            w.WriteD(clientVersion);
            w.WriteD(FTF.Version.CommonPacketVersion);

            //-- Client Type
#if UNITY_EDITOR
            w.WriteB(true); // Editor
#else
            w.WriteB(false); // No Editor
#endif
            w.WriteB(isDummy);

            return w.ToBytes();
        }

        public static byte[] Login(string id, string pw)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Login);
            w.WriteC(0); // ID, PW를 통한 로그인
            w.WriteS(id);
            w.WriteS(pw);
            return w.ToBytes();
        }

        public static byte[] Login(long sn)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Login);
            w.WriteC(240); // ID, PW를 통한 로그인
            w.WriteL(sn);
            return w.ToBytes();
        }

        public static byte[] LoginObserver()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Login);
            w.WriteC(255); // ID, PW를 통한 로그인
            return w.ToBytes();
        }

        public static byte[] Quit()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Quit);

            return w.ToBytes();
        }

        public static byte[] UpdateScore()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            //w.WriteO(ClientOpcode.C_Quit);

            return w.ToBytes();
        }

        public static byte[] RequestUserInfo(int instanceID)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Request_UserInfo);
            w.WriteD(instanceID);
            return w.ToBytes();
        }

        public static byte[] TrackingPosition(Vector3 head, Quaternion headR, bool useController, Vector3 cont, Quaternion contQ, bool useReticle, Vector3 reticle)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_TrackPosition);
            w.WriteNormal(head);
            w.WriteEuler32(headR.eulerAngles);
            w.WriteB(useController);
            if (useController)
            {
                w.WriteNormal(cont);
                w.WriteEuler24(contQ.eulerAngles);
                w.WriteB(useReticle);
                if (useReticle)
                {
                    w.WriteV3(reticle);
                }
            }

            return w.ToBytes();
        }

        public static byte[] VoIPSetup(string ip, int port)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_VoIPSetup);
            w.WriteS(ip);
            w.WriteD(port);

            return w.ToBytes();
        }
    }
}