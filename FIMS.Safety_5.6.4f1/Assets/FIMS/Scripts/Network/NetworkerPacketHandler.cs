﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// Networker에서 전달된 데이터를 논리적으로 처리하는 일을 한다.
    /// 서버 버전에 있어서부터 로그인, 방접속, 생성 등의 모든 패킷을 처리한다.
    /// 이중 일부는 IReceivePacket를 상속하고있는 Instance 혹은 System에게 넘겨주고 처리하도록 한다.
    /// 일반적으로 대신 처리하는 패킷은 RoomInfo와 PlayerInstance가 있다.
    /// </summary>
    public class NetworkerPacketHandler : AbstractPacketHandler
    {

        #region Enums
        public enum LoginResultType : byte
        {
            OK,
            WrongID,
            WrongPassword,
        }
        public enum MessageType : byte
        {
            Alert,
            Warning,
            Error,
        }
        public enum DisconnectType
        {
            Unkown,
            Message
        }
        #endregion

        protected override void Handling()
        {
            //log.Debug("Handling num: " + ((ServerOpcode)Enum.ToObject(typeof(ServerOpcode), code)).ToString());
            switch (code)
            {
                //----------------------------------------
                // Common Packets
                case ServerOpcode.C_Command:
                    OnCommand();
                    break;
                case ServerOpcode.C_UserInfo:
                    OnUserInfo();
                    break;
                case ServerOpcode.C_ServerVersion:
                    OnServerVersion();
                    break;
                case ServerOpcode.C_LoginResult:
                    OnLoginResult();
                    break;
                case ServerOpcode.C_Disconnect:
                    OnDisconnect();
                    break;
                case ServerOpcode.C_SystemMessage:
                    OnSystemMessage();
                    break;

                //----------------------------------------
                // Room Packets
                case ServerOpcode.Room_List:
                    OnRoomList();
                    break;
                case ServerOpcode.Room_Info:
                    OnRoomInfo();
                    break;
                case ServerOpcode.Room_Exit:
                    OnRoomExit();
                    break;
                case ServerOpcode.Room_LoadStart:
                    OnRoomLoadStart();
                    break;
                case ServerOpcode.Room_Ban:
                    OnRoomBan();
                    break;

                //----------------------------------------
                // Scene Packets
                case ServerOpcode.Scene_Start:
                    OnSceneStart();
                    break;
                case ServerOpcode.Scene_End:
                    OnSceneEnd();
                    break;
                case ServerOpcode.Scene_InitializeTo:
                    OnSceneInitializeTo();
                    break;
                case ServerOpcode.Scene_SetData:
                    OnSceneSetData();
                    break;
                case ServerOpcode.Scene_SetupCommanderID:
                    SetupCommanderID();
                    break;

                //----------------------------------------
                // Safety Packets
                case ServerOpcode.Safety_Next_Quiz:
                    Debug.Log("========== ServerOpcode.Safety_Next_Quiz ==========");
                    SummaryQuizManager.main.ActiveQuiz(reader.ReadH());
                    break;
                case ServerOpcode.Safety_DetailMapStage:
                    SummaryQuizManager.main.QuizStart();
                    SummaryQuizManager.main.ActiveQuiz(reader.ReadH());                    
                    break;
                case ServerOpcode.Safety_MapStage_Start:
                    // 현재 사용하지 않음
                    short index = reader.ReadH();

                    SafetyTrainingSceneManager.main.PlayMediaPlayerVideo();
                    break;
            }
        }

        #region ### Common Packets ###
        protected virtual void OnServerVersion()
        {
            Networker.State = NetworkState.Validated;

            int version = reader.ReadD();
            Networker.main.GetSocket().ReceiveTimeout = reader.ReadD();
            Networker.main.GetSocket().SendTimeout = reader.ReadD();
        }

        protected virtual void OnLoginResult()
        {
            LoginResultType resultType = (LoginResultType)reader.ReadC();
            switch (resultType)
            {
                case LoginResultType.OK:
                    Networker.State = NetworkState.Logined;
                    break;

                case LoginResultType.WrongID:
                case LoginResultType.WrongPassword:
                    Networker.State = NetworkState.Validated;
                    throw new MessageException("로그인에 실패했습니다. " + resultType);
            }
        }

        protected virtual void OnUserInfo()
        {
            int instanceID = reader.ReadD();
            PlayerInstance player = PlayerManager.GetPlayer(instanceID);

            bool newCharacter = (player == null);
            if (newCharacter)
                player = PlayerManager.NewPlayer(instanceID);

            player.OnReceivePacket(code, reader);

            if (newCharacter)
            {
                PlayerManager.AddPlayer(player);
                if (player == PlayerInstance.main)
                {
                    // 이전에 종료한 게임이 있고 아직 게임이 끝나지 않았다면
                    // 해당 게임에 다시 참여한다.
                    if (player.roomID != 0)
                    {
                        Networker.State = NetworkState.RoomJoining;
                        Networker.Send(C_Channel.Join(player.roomID, ConnectInfo.RoomName));
                    }
                }
            }
        }

        protected virtual void OnCommand()
        {
            int count = reader.ReadC();
            for (int i = 0; i < count; i++)
            {
                CommandListener.OnReceive(reader);
            }
        }

        protected virtual void OnSystemMessage()
        {
            MessageType type = (MessageType)reader.ReadC();
            string msg = reader.ReadS();
            Debug.LogWarning(type + ": " + msg);
        }

        protected virtual void OnDisconnect()
        {
            DisconnectType type = (DisconnectType)reader.ReadC();
            string msg = reader.ReadS();
            Networker.Disconnect(true);

            AppExiter.Exit("NetworkerPacketHandler.OnDisconnect.Quit " + type + " / " + msg);
        }
        #endregion ### Common Packets ###

        #region ### Room Packets ###
        protected virtual void OnRoomList()
        {
            long autoJoinRoomID = ConnectInfo.RoomID;

            int length = reader.ReadD();
            for (int i = 0; i < length; i++)
            {
                long num = reader.ReadL();
                string name = reader.ReadS();
                short mapID = reader.ReadH();
                byte playerCnt = reader.ReadC();
                byte maxCnt = reader.ReadC();
                bool started = reader.ReadB();

                if (autoJoinRoomID == num)
                {
                    if (playerCnt < maxCnt)
                    {
                        Networker.Send(C_Channel.Join(num, ConnectInfo.RoomName));
                        return;
                    }
                    else
                    {
                        throw new MessageException("방이 모두 찼습니다. 점원에게 문의해주세요.");
                    }
                }
            }

            // 자동으로 들어가야할 방번호가 없다면 새로 방을 만든다.
            if (autoJoinRoomID != -1)
            {
                Networker.Send(C_Channel.Create(ConnectInfo.RoomName, ConnectInfo.RoomName, (short)ConnectInfo.MapID, (byte)ConnectInfo.UserCount, false));
            }
        }

        protected virtual void OnRoomInfo()
        {
            RoomInfo.main.OnReceivePacket(code, reader);
        }

        protected virtual void OnRoomLoadStart()
        {
            RoomInfo.main.OnReceivePacket(code, reader);
        }

        protected virtual void OnRoomExit()
        {
            RoomInfo.main.OnReceivePacket(code, reader);
        }

        protected virtual void OnRoomBan()
        {
            RoomInfo.main.OnReceivePacket(code, reader);
        }
        #endregion ### Room Packets ###

        #region ### Scene Packets ###
        protected virtual void OnSceneStart()
        {
            Networker.State = NetworkState.Playing;
        }

        protected virtual void OnSceneEnd()
        {
            bool stopApp = reader.ReadB();
            if (stopApp)
            {
                // 앱이 종료될 때 아래경로의 파일 삭제
                // 해당 파일이 존재하면
                /*
                if (File.Exists("/storage/emulated/0/Fantafo/Flags/" + "alloc"))
                {
                    // 파일 삭제
                    File.Delete("/storage/emulated/0/Fantafo/Flags/" + "alloc");
                }               
                */
                ScreenFader.main.FadeOut(() =>
                {
                    if (OnQuit != null)
                    {
                        OnQuit(_quit);
                    }
                    else
                    {
                        _quit();
                    }
                });
            }
            else
            {
                var loadSystem = gameObject.AddComponent<LoadSceneIndex>();
                loadSystem.Load(0);
            }
        }

        protected virtual void OnSceneInitializeTo()
        {
            byte[] data = new byte[] { (byte)ServerOpcode.Scene_Start };
            int playerCount = reader.ReadC();
            for (int i = 0; i < playerCount; i++)
            {
                PlayerManager.GetPlayer(reader.ReadD(), player =>
                {
                    if (BaseSceneInitializer.main != null)
                        BaseSceneInitializer.main.InitializeTo(player);

                    //Networker.Send(C_Scene.InitializeCompleted(player.instanceId, wakeup:false));
                });
            }
        }

        private void OnSceneSetData()
        {
            var count = reader.ReadC();
            for (int i = 0; i < count; i++)
            {
                var type = (C_Data.TYPE)reader.ReadC();
                var key = reader.ReadS();

                switch (type)
                {
                    case C_Data.TYPE.String:
                        {
                            Data.SetString(key, reader.ReadS(), false);
                        }
                        break;
                    case C_Data.TYPE.Int:
                        {
                            Data.SetInt(key, reader.ReadD(), false);
                        }
                        break;
                    case C_Data.TYPE.Float:
                        {
                            Data.SetFloat(key, reader.ReadF(), false);
                        }
                        break;
                    case C_Data.TYPE.Bool:
                        {
                            Data.SetBool(key, reader.ReadB(), false);
                        }
                        break;
                    case C_Data.TYPE.Vector3:
                        {
                            Data.SetVector3(key, reader.ReadVector3(), false);
                        }
                        break;
                    case C_Data.TYPE.Color:
                        {
                            Data.SetColor(key, reader.ReadColorRGBA(), false);
                        }
                        break;
                }
            }

            Data.ProcDelegate();
        }
        private void SetupCommanderID()
        {
            string path = reader.ReadS();
            string type = reader.ReadS();
            uint id = reader.ReadUInt32();

            Type typeObj = Type.GetType(type);
            if(typeObj == null)
            {
                log.ErrorFormat("Setup CommanderID Error: Not found type '{0}'.", type);
                return;
            }

            var go = GameObject.Find('/' + path);
            if(go == null)
            {
                log.ErrorFormat("Setup CommanderID Error: Not found object '{0}'.", path);
                return;
            }

            var target = go.GetComponent(typeObj) as IdentityCommander;
            if(target == null)
            {
                log.ErrorFormat("Setup CommanderID Error: Target is Not IdentityCommander '{0}/{1}'.", path, type);
                return;
            }

            target.TargetId = id;
            Debug.LogFormat(go, "Setup Commander ID '{0}' -> '{1}/{2}'", id, path, type);
        }
        #endregion ### Scene Packets ###

        #region ### Safety Packets ###
        protected virtual void NextQuiz()
        {
            Networker.State = NetworkState.Playing;
        }
        #endregion ### Safety Packets ###

        void _quit()
        {
            if (Networker.State == NetworkState.Playing)
            {
                // save data
                Properties prop = new Properties();
                prop.SetString("name", PlayerInstance.main.userName);
                prop.SetInt("score", PlayerInstance.main.score);
                prop.Save(FimsCommonData.SCORE_PATH, "");
            }

            if (File.Exists(FimsCommonData.CONNECT_PATH))
                File.Delete(FimsCommonData.CONNECT_PATH);

            AppExiter.Exit("NetworkerPacketHandler._quit.Quit");
        }
    }
}