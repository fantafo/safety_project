﻿using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using uRandom = UnityEngine.Random;

namespace FTF.Dummy
{
    /// <summary>
    /// 서버에 더미 클라이언트를 접속시킨다.
    /// </summary>
    public class DummyClient : SMonoBehaviour
    {
#if UNITY_EDITOR
        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        ILog log = SLog.GetLogger("Dummy");



        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Member Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        [Header("서버 연결정보")]
        public bool getConnectInfo = true;
        public bool autoConnectOnStart = true;
        public bool autoLoadComplete = true;
        [Header("Client Identifier")]
        public int clientPacketVersion;
        public string clientIdentifier;
        [Header("Connect Info")]
        public string ipAddress = "127.0.0.1";
        public int port = 40000;
        public string userName = "Unkown";
        public bool waitForNetworker;

        // 네트워크 관련 정보들
        private bool _isRun = false;
        private Thread _socketThread;
        private Thread _trackThread;
        private Socket _socket;
        private NetworkStream _stream;
        private BinaryReader _reader;
        private BinaryWriter _writer;
        private Cipher _cipher;

        private NetworkState State;
        public event Action onBeginConnect; // MainThread에서 호출됨
        public event Action onConnected; // ConnectThread에서 호출도됨
        public event Action onDisconnected; // ConnectThread에서 호출도됨

        public NetworkStream Stream { get { return _stream; } set { _stream = value; } }



        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Life Cycle                         //
        //                                                    //
        ////////////////////////////////////////////////////////

        private void Reset()
        {
            var networker = GameObject.FindObjectOfType<Networker>();
            clientPacketVersion = networker.clientPacketVersion;
            clientIdentifier = networker.clientIdentifier;
        }
        private void Awake()
        {
            State = NetworkState.None;

            log.Tag = name;
            log.DebugFormat("Initialize Networker ({0}/{1})", clientIdentifier, clientPacketVersion);
            transform.SetParent(null, true);
            DontDestroyOnLoad(gameObject);
        }
        private IEnumerator Start()
        {
            yield return null; // 다른 컴포넌트들이 세팅될 때까지 기다린다.
            if (getConnectInfo)
            {
                ipAddress = ConnectInfo.ConnectIP;
                port = ConnectInfo.ConnectPort;
            }

            if (autoConnectOnStart)
                Connect();
        }

        private void OnEnable()
        {
            if (_socketThread != null)
            {
                if (autoConnectOnStart)
                {
                    Connect();
                }
            }
            StartCoroutine(SendTrackingData());
        }

        // 게임을 종료시킬때
        private void OnDisable()
        {
            Disconnect(true);
        }
        private void OnApplicationQuit()
        {
            Disconnect(true);
        }

        private void OnDestroy()
        {
            Disconnect(true);
        }

        private void Update()
        {
            if (!Application.isPlaying)
            {
                _isRun = false;
            }
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Operation Functions                //
        //                                                    //
        ////////////////////////////////////////////////////////
        [Button]
        public void Connect()
        {
            if (!_isRun)
            {
                log.Debug("Begin Connecting");

                _isRun = true;
                State = NetworkState.None;

                _socketThread = new Thread(_run);
                _socketThread.Start();
            }
        }

        [Button]
        public void Disconnect()
        {
            Disconnect(true);
        }
        [ContextMenu("Disconnect NoPacket")]
        public void DisconnectNoPacket()
        {
            Disconnect(false);
        }
        [ContextMenu("Disconnect NoClose")]
        public void DisconnectNoClose()
        {
            _isRun = false;
            try
            { _socketThread.Abort(); }
            catch { }
            try
            { _trackThread.Abort(); }
            catch { }

            _socket = null;
            _reader = null;
            _writer = null;
            Stream = null;
            _socketThread = null;
            _trackThread = null;
            _cipher = null;
            onBeginConnect = null;
            onConnected = null;
            onDisconnected = null;
        }
        public void Disconnect(bool packet)
        {
            // 쓰레드 중지 신호
            _isRun = false;
            if (_isRun)
            {
                log.Debug("Begin Disconnect");

                // 게임종료 패킷 전송
                if (State >= NetworkState.Validating && packet)
                {
                    Send(C_Common.Quit());
                }

                // 소켓 강제종료
                try
                { _writer.Close(); }
                catch { }
                try
                { _reader.Close(); }
                catch { }
                try
                { Stream.Close(); }
                catch { }
                try
                { _socket.Close(); }
                catch { }

                _socket = null;
                Stream = null;
                _reader = null;
                _writer = null;

                log.Debug("Disconnect Complete");
            }
        }

        [ContextMenu("Random Name")]
        public void RandomName()
        {
            name = userName = string.Format("Dummy{0:0000}", transform.GetSiblingIndex());
        }

        [ContextMenu("Send Load Complete")]
        public void SendLoadComplete()
        {
            Send(C_Room.LoadComplete());
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                  Network                           //
        //                                                    //
        ////////////////////////////////////////////////////////

        /// <summary>
        /// 클라이언트 수신용 쓰레드
        /// </summary>
        private void _run()
        {
            while (_isRun)
            {
                while (waitForNetworker && Networker.State < NetworkState.Room)
                {
                    Thread.Sleep(100);
                }

                try
                {
                    // 서버 IP 할당
                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ConnectInfo.ConnectIP), ConnectInfo.ConnectPort);
                    _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _socket.NoDelay = true;
                    log.Debug("Connecting.. " + endPoint);

                    State = NetworkState.Connecting;
                    if (onBeginConnect != null)
                        onBeginConnect();

                    // 서버에 접속한다. 접속하지 못한다면 1초 간격으로 재접속을 시도한다.
                    while (true)
                    {
                        try
                        {
                            _socket.Connect(endPoint);
                            break;
                        }
                        catch
                        {
                            if (!_isRun)
                                return;

                            log.Debug("Reconnecting.. " + endPoint);
                            Thread.Sleep(1000);
                        }
                    }
                    log.Debug("Connected");

                    State = NetworkState.Connected;
                    if (onConnected != null)
                        onConnected();

                    // 접속이 완료됐다면 송수신을 위한 스트림을 연다.
                    Stream = new NetworkStream(_socket);
                    _reader = new BinaryReader(Stream, Encoding.UTF8);
                    _writer = new BinaryWriter(Stream, Encoding.UTF8);

                    // 서버에서보내온 Seed 패킷을 분석하고 적용한다.
                    byte[] seedPacket = new byte[32];
                    int readSize = _reader.Read(seedPacket, 0, seedPacket.Length);

                    long seed = BitConverter.ToInt64(seedPacket, 14);
                    _cipher = new Cipher(seed);
                    Debug.Log("[Client] Seed 0x" + seed.ToString("X16"));

                    // 클라이언트 버전 정보 전송
                    State = NetworkState.Validating;
                    Send(C_Common.Version(clientIdentifier, clientPacketVersion, true));

                    // 패킷 읽기
                    ReadPackets();
                }
                catch (Exception e)
                {
                    if (_isRun)
                    {
                        log.Exception(e);
                    }
                }
                State = NetworkState.Connecting;
                Thread.Sleep(1000);
            }
            State = NetworkState.Disconnect;
        }

        private void ReadPackets()
        {
            short offset, size;
            byte[] data;

            while (_isRun)
            {
                try
                {
                    size = _reader.ReadInt16();
                    offset = 0;

                    // Read
                    try
                    {
                        data = new byte[size];// BytePool.Take(size);

                    }
                    catch (Exception e)
                    {
                        e.PrintStackTrace();
                        data = new byte[size];
                    }

                    while (size > 0)
                    {
                        short read = (short)Stream.Read(data, offset, Math.Min((int)size, 1024));
                        size -= read;
                        offset += read;
                    }
                    size = offset;

                    // Decryption
                    data = _cipher.Decrypt(data, size);
                    if (data[0] == (byte)ServerOpcode.C_Ping)
                    {
                        Send(new byte[] { (byte)ClientOpcode.C_Ping });
                    }
                    else
                    {
                        PacketHandling(data);
                        //BytePool.Release(data);
                    }
                }
                catch (IOException) { break; }
                catch (Exception e)
                {
                    if (_isRun)
                    {
                        log.Exception(e);
                    }
                }
            }
        }

        public void Send(byte[] data, bool flush = true)
        {
            if (State < NetworkState.Validating)
                return;

            lock (this)
            {
                try
                {
                    // Encryption
                    data = _cipher.Encrypt(data, data.Length);

                    _writer.Write((ushort)data.Length);
                    _writer.Write(data);
                    if (flush)
                        _writer.Flush();
                }
                catch (Exception e)
                {
                    if (_isRun)
                    {
                        e.PrintStackTrace();
                    }
                }
            }
        }

        byte[] buffer;
        MemoryStream memoryStream;
        BinaryReader reader;
        int playerInstanceID;
        int playerIndex;
        public void PacketHandling(byte[] data)
        {
            if (reader == null)
            {
                buffer = new byte[ushort.MaxValue];
                memoryStream = new MemoryStream(buffer, true);
                reader = new BinaryReader(memoryStream, Encoding.UTF8);
            }
            Array.Copy(data, buffer, data.Length);
            memoryStream.Position = 0;

            ServerOpcode opcode = reader.ReadServerCode();
            switch (opcode)
            {
                case ServerOpcode.C_ServerVersion:
                    {
                        Send(C_Common.Login(userName, userName));
                        State = NetworkState.Loging;
                    }
                    break;
                case ServerOpcode.C_LoginResult:
                    {
                        playerInstanceID = -1;
                        switch ((NetworkerPacketHandler.LoginResultType)reader.ReadC())
                        {
                            case NetworkerPacketHandler.LoginResultType.OK:
                                {

                                    State = NetworkState.RoomJoining;
                                    Send(C_Channel.Join(RoomInfo.main.num, RoomInfo.main.roomName));
                                }
                                break;
                        }
                    }
                    break;
                case ServerOpcode.C_UserInfo:
                    {
                        if (playerInstanceID == -1)
                        {
                            playerInstanceID = reader.ReadD();
                        }
                    }
                    break;
                case ServerOpcode.Room_Info:
                    {
                        State = NetworkState.Room;

                        var num = reader.ReadD();
                        var roomName = reader.ReadS();
                        var maxUser = reader.ReadC();
                        var mapID = reader.ReadH();
                        var mapStage = reader.ReadH();

                        int userCount = reader.ReadC();
                        for (int i = 0; i < userCount; i++)
                        {
                            if (playerInstanceID == reader.ReadD())
                            {
                                playerIndex = i;
                            }
                            reader.ReadVector3();
                            reader.ReadQuaternion();
                            reader.ReadVector3();
                            reader.ReadQuaternion();
                            reader.ReadB();
                            reader.ReadVector3();
                        }
                    }
                    break;
                case ServerOpcode.Room_LoadStart:
                    {
                        //if (autoLoadComplete)
                        //{
                        //    Send(C_Room.LoadComplete());
                        //}

                        // 해당 값에 따라 Safety 매니저가 특정 콘텐츠를 실행. 1 생명, 2 화학, 전기
                        int index = reader.ReadH();
                        if (autoLoadComplete)
                        {
                            Send(C_Room.LoadComplete());
                        }
                    }
                    break;
                case ServerOpcode.Scene_Start:
                    {
                        State = NetworkState.Playing;
                    }
                    break;
            }
        }

        private IEnumerator SendTrackingData()
        {
            Quaternion rot = Quaternion.identity;
            Vector3 cont = new Vector3(0.3f, -0.5f, 0);
            Quaternion contQ = Quaternion.identity;
            Vector3 ray = cont;
            bool isAction = false;

            WaitForSeconds waits = new WaitForSeconds(0.05f);
            while (true)
            {
                if (!isAction)
                {
                    isAction = true;

                    Vector3 dir = new Vector3(uRandom.Range(-1f, 1f), uRandom.Range(-1f, 1f), 1);
                    STween.value(rot, Quaternion.LookRotation(dir), 1f, v => rot = v).SetOnComplete(() => isAction = false);

                    dir = new Vector3(uRandom.Range(-1f, 1f), uRandom.Range(-1f, 1f), 1);
                    STween.value(rot, Quaternion.LookRotation(dir), 1f, v =>
                    {
                        contQ = v;
                        cont = new Vector3(.2f, -.4f, -0.1f) + contQ * new Vector3(0, 0, .2f);
                    }).SetOnComplete(() => isAction = false);
                }

                if (State >= NetworkState.Room)
                {
                    Vector3 bodyPos = Vector3.zero;
                    Quaternion bodyRot = Quaternion.identity;

                    Vector3 headPos = new Vector3(0, 1.2f, 0);
                    if (UserLocationManager.main != null)
                    {
                        var locate = UserLocationManager.main[playerIndex];
                        if (locate != null)
                        {
                            bodyPos = locate.transform.position;
                            bodyRot = locate.transform.rotation;
                        }
                        else
                        {
                            bodyPos = Vector3.zero;
                            bodyRot = Quaternion.identity;
                        }
                    }

                    var tempRay = new Ray(bodyPos + headPos + cont, bodyRot * (contQ * Quaternion.Euler(-15, 0, 0)) * Vector3.forward);
                    RaycastHit hit;
                    bool isHit = Physics.Raycast(tempRay, out hit, 100f, -1);

                    Vector3 point;
                    if (isHit)
                        point = hit.point;
                    else
                        point = tempRay.GetPoint(10);

                    Send(C_Common.TrackingPosition(Vector3.zero, rot, true, cont, contQ, ReticleVisible, point));
                    yield return waits;
                }
                yield return null;
            }
        }
        public bool ReticleVisible;
#endif
    }
}
