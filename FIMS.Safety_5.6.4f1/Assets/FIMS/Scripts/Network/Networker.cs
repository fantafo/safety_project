﻿#if UNITY_EDITOR || UNITY_STANDALONE
#define VISIBLEPACKET
#endif

using FTF.Packet;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF
{
    public enum NetworkState
    {
        Disconnect = -1, // Multi
        None = 0,
        Connecting, // Multi
        Connected, // Multi
        Validating, // Multi
        Validated,
        Loging,
        Logined,
        Channel,
        RoomJoining,
        Room,
        Loading,
        Playing,
    }

    /// <summary>
    /// 서버와 연결하기 위한 클라이언트 소켓을 관리한다.
    /// 서버에 접속, 데이터 읽기, 데이터 쓰기의 IO관리를 실시한다.
    /// 이곳에서는 단순 바이너리 데이터를 관리하고 처리하며,
    /// 논리 처리는 PacketHandler에서 이뤄진다.
    /// 이곳에서 읽어온 데이터는 PacketHandler.Push를 통해서 입력되게 된다.
    /// </summary>
    public class Networker : SMonoBehaviour
    {
        ILog log = SLog.GetLogger("Networker");
        public static Networker main { get; private set; }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Static Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        /// <summary>
        /// 클라이언트의 아이피 주소를 가리킵니다.
        /// </summary>
        public static string ipAddress { get; private set; }

        /// <summary>
        /// 네트워크의 연결상태 및 클라이언트가 어떤 단계까지 진행했는지에 대해서 알 수 있습니다.
        /// </summary>
        private static NetworkState _state;
        public static NetworkState State { get { return _state; } set { if (_state != value) { if (onChangedNetworkState != null) onChangedNetworkState(_state, value); _state = value; } } }
        public static bool IsConnected { get { return State < NetworkState.Connected; } }

        /// <summary>
        /// 네트워크가 실행된 Id를 가리키며 네트워크 처리중 재접속이 됐을 때, 이전 연결과 다음 연결을 구별하기 위해 사용됩니다.
        /// </summary>
        public static int ConnectionId { get; private set; }

        /// <summary>
        /// 접속관련 이벤트를 호출한다. 이 이벤트는 ConnectThread에서 처리되기에 Unity 관련메서드를 주의해야한다.
        /// </summary>
        public static event Action onBeginConnect; // ConnectThread에서 호출됨
        public static event Action onConnected; // ConnectThread에서 호출도됨
        public static event Action onDisconnected; // ConnectThread에서 호출도됨

        /// <summary>
        /// 네트워크 상태가 변경됐을 때 호출된다.
        /// </summary>
        public static event Action<NetworkState, NetworkState> onChangedNetworkState;

        /// <summary>
        /// 네트워크의 작동을 나타내는 플레그
        /// </summary>
        private static bool _isRun = false;

        public static void Send(byte[] data, bool flush = true)
        {
            if (main != null && State >= NetworkState.Validating)
            {
                main.send(data, flush);
            }
        }

        public static void Disconnect(bool packet)
        {
            if (main != null)
                main.disconnect(packet);
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Member Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        [Header("서버 연결정보")]
        public bool autoConnectOnStart = true;
        public bool autoSendVersionPacket = true;
        [Header("Client Identifier")]
        [Tooltip("방을 생성하거나 검색, 입장할때의 조건이 된다. 이 값이 동일한 대상과만 같은 방을 이룰 수 있다.")]
        public int clientPacketVersion;
        [NonSerialized]
        public string clientIdentifier;
        [Header("Component")]
        public NetworkerPacketHandler packetHandler;

#if UNITY_EDITOR || VISIBLEPACKET
        [Header("VisibleList")]
        public bool visibleSendPacketAll;
        public bool visibleReceivePacketAll;
        public ClientOpcode[] visibleSendPacket;
        public ServerOpcode[] visibleReceivePacket;
        public ClientOpcode[] invisibleSendPacket;
        public ServerOpcode[] invisibleReceivePacket;
#endif

        // 네트워크 관련 정보들
        private Thread _socketThread;
        private Socket _socket;
        private NetworkStream _stream;
        private BinaryReader _reader;
        private BinaryWriter _writer;
        private Cipher _cipher;
        public Socket GetSocket()
        {
            return _socket;
        }


        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Life Cycle                         //
        //                                                    //
        ////////////////////////////////////////////////////////

        private void Awake()
        {
            if (main)
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
                return;
            }

            ipAddress = Network.player.ipAddress;
            State = NetworkState.None;
            ConnectionId = 0;

            clientIdentifier = ConnectInfo.Identifier;

            log.DebugFormat("Initialize Networker ({0}/{1})", clientIdentifier, clientPacketVersion);
            main = this;
            transform.SetParent(null, true);
            DontDestroyOnLoad(gameObject);
        }
        private IEnumerator TStart()
        {
            yield return null; // 다른 컴포넌트들이 세팅될 때까지 기다린다.

            if (autoConnectOnStart)
                connect();
        }
        private void OnEnable()
        {
            log.Debug("OnEnable");
            StartCoroutine(TStart());
        }

        // 게임을 종료시킬때
        private void OnDisable()
        {
            log.Debug("OnDisable");
            disconnect(true);
        }
        private void OnApplicationQuit()
        {
            disconnect(true);
        }

        private void OnDestroy()
        {
            log.Debug("OnDestroy");
            Disconnect(true);
            if (main == this)
            {
                main = null;
                ipAddress = null;
                State = NetworkState.None;
                ConnectionId = 0;
                _isRun = false;

                onBeginConnect = null;
                onConnected = null;
                onDisconnected = null;
            }
        }


        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Operation Functions                //
        //                                                    //
        ////////////////////////////////////////////////////////

        public void connect()
        {
            if (!_isRun)
            {
                log.Debug("Begin Connecting");

                _isRun = true;
                State = NetworkState.None;

                _socketThread = new Thread(_run);
                _socketThread.Name = "NetworkThread";
                _socketThread.Priority = System.Threading.ThreadPriority.Highest;
                _socketThread.Start();
            }
        }

        public void disconnect(bool packet)
        {
            if (_isRun)
            {
                log.Debug("Begin Disconnect");

                // 쓰레드 중지 신호
                _isRun = false;

                // 게임종료 패킷 전송
                if (State >= NetworkState.Validating && packet)
                {
                    send(C_Common.Quit());
                }

                // 소켓 강제종료
                try
                { _writer.Close(); }
                catch { }
                try
                { _reader.Close(); }
                catch { }
                try
                { _stream.Close(); }
                catch { }
                try
                { _socket.Close(); }
                catch { }

                _socket = null;
                _stream = null;
                _reader = null;
                _writer = null;

                log.Debug("Disconnect Complete");
            }
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                  Network                           //
        //                                                    //
        ////////////////////////////////////////////////////////

        /// <summary>
        /// 서버에 접속하고 접속한 대상으로부터 정보를 일어오는 역할을 한다.
        /// 정보를 읽거나 주변 환경에 의해 소켓 연결이 끊어질 경우 재접속을 시도한다.
        /// 하지만 Networker 자체가 활동을 정지한 상태라면 재접속을 시도하지 않고 종료된다.
        /// </summary>
        private void _run()
        {
            while (_isRun)
            {
                try
                {
                    // 서버 IP 할당
                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ConnectInfo.ConnectIP), ConnectInfo.ConnectPort);
                    _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _socket.NoDelay = true;
                    _socket.ReceiveTimeout = 5000;

                    log.Debug("Connecting.. " + endPoint);
                    State = NetworkState.Connecting;
                    if (onBeginConnect != null)
                        onBeginConnect();

                    // 서버에 접속한다. 접속하지 못한다면 1초 간격으로 재접속을 시도한다.
                    while (true)
                    {
                        try
                        {
                            _socket.Connect(endPoint);
                            break;
                        }
                        catch
                        {
                            if (!_isRun)
                                return;

                            log.Debug("Reconnecting.. " + endPoint);
                            Thread.Sleep(1000);
                        }
                    }
                    log.Debug("Connected");
                    State = NetworkState.Connected;
                    if (onConnected != null)
                        onConnected();

                    // 접속이 완료됐다면 송수신을 위한 스트림을 연다.
                    _stream = new NetworkStream(_socket);
                    _reader = new BinaryReader(_stream, Encoding.UTF8);
                    _writer = new BinaryWriter(_stream, Encoding.UTF8);

                    // 서버에서보내온 Seed 패킷을 분석하고 적용한다.
                    byte[] seedPacket = new byte[32];
                    int readSize = _reader.Read(seedPacket, 0, seedPacket.Length);

                    long seed = BitConverter.ToInt64(seedPacket, 14);
                    _cipher = new Cipher(seed);
                    Debug.Log("[Client] Seed 0x" + seed.ToString("X16"));

                    // 클라이언트 버전 정보 전송
                    State = NetworkState.Validating;
                    ConnectionId++;

                    if (autoSendVersionPacket)
                    {
                        send(C_Common.Version(clientIdentifier, clientPacketVersion, false));
                    }

                    // 패킷 읽기
                    ReadPackets();
                }
                catch (Exception e)
                {
                    if (_isRun)
                    {
                        log.Exception(e);
                    }
                }
                if (onDisconnected != null)
                    onDisconnected();

                State = NetworkState.Connecting;
                Thread.Sleep(1000);
            }
            State = NetworkState.Disconnect;
        }

        /// <summary>
        /// 네트워크 스트림에서 지속적으로 패킷을 읽어온다.
        /// 2byte: 길이 패킷
        /// 1byte: 옵코드
        /// ~~   : 데이터
        /// 
        /// 해당 데이터는 bytepool에 의해서 4의 배수로 가져와 데이터를 임시 저장한다.
        /// 임시저장된 데이터는 해당 지정된 PacketHandler로 전달되어 처리된다.
        /// </summary>
        private void ReadPackets()
        {
            short offset, size;
            byte[] data;
            int skipCount = 0;

            while (_isRun)
            {
                try
                {
                    size = _reader.ReadInt16();
                    offset = 0;

                    if (size == 0)
                    {
                        log.Error("Size 0??");
                    }

                    // Read
                    try
                    {
                        data = BytePool.Take(size);
                    }
                    catch (Exception e)
                    {
                        e.PrintStackTrace();
                        data = new byte[size];
                    }

                    DateTime beginReadTime = DateTime.Now;
                    while (size > 0)
                    {
                        short read = (short)_stream.Read(data, offset, Math.Min((int)size, 1024));
                        size -= read;
                        offset += read;

                        if (size > 0)
                        {
                            if ((DateTime.Now - beginReadTime).TotalMilliseconds > _socket.ReceiveTimeout)
                            {
                                throw new TimeoutException("Receive Data is timeout. (Size:" + (size + offset) + ", Read:" + offset + ")");
                            }
                        }
                    }
                    size = offset;

                    AddReceiveMonitor(size + 2);

                    // Decryption
                    data = _cipher.Decrypt(data, size);

                    if (data[0] == (byte)ServerOpcode.C_Ping)
                    {
                        send(new byte[] { (byte)ClientOpcode.C_Ping });
                    }
                    else
                    {
                        PrintReadPacket(data);
                        packetHandler.Push(data);
                    }
                }
                catch (EndOfStreamException e)
                {
                    if (_isRun)
                    {
                        log.Exception(e);
                        break;
                    }
                }
                //catch (IOException){}
                catch (Exception e)
                {
                    if (_isRun)
                    {
                        log.Exception(e);
                        break;
                    }
                }
            }
            try
            {
                _socket.Close();
            }
            catch
            { }
            log.Debug("Disconnected Socket");
        }
        //[System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void PrintReadPacket(byte[] data)
        {
#if UNITY_EDITOR || VISIBLEPACKET
            if (visibleReceivePacketAll)
            {
                ServerOpcode code = (ServerOpcode)data[0];
                if (invisibleReceivePacket == null || invisibleReceivePacket.IndexOf(code) == -1)
                {
                    Debug.LogFormat("<color=#DD77AA>===[Receive]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                }
            }
            else if (!visibleReceivePacket.IsEmpty())
            {
                ServerOpcode code = (ServerOpcode)data[0];
                foreach (var op in visibleReceivePacket)
                {
                    if (op == code)
                    {
                        Debug.LogFormat("<color=#DD77AA>===[Receive]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                        break;
                    }
                }
            }
#endif
        }

        /// <summary>
        /// 서버에 전달할 패킷을 전송한다.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="flush"></param>
        public void send(byte[] data, bool flush = true)
        {
            if (State < NetworkState.Validating)
                return;

            AddSendMonitor(data.Length + 2);

            lock (this)
            {
                PrintSendPacket(data);
                try
                {
                    // Encryption
                    data = _cipher.Encrypt(data, data.Length);

                    _writer.Write((ushort)data.Length);
                    _writer.Write(data);
                    if (flush)
                        _writer.Flush();
                }
                catch (Exception e)
                {
                    if (_isRun)
                    {
                        e.PrintStackTrace();
                    }
                }
            }
        }
        //[System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void PrintSendPacket(byte[] data)
        {
#if UNITY_EDITOR || VISIBLEPACKET
            if (visibleSendPacketAll)
            {
                ClientOpcode code = (ClientOpcode)data[0];
                if (invisibleSendPacket == null || invisibleSendPacket.IndexOf(code) == -1)
                {
                    Debug.LogFormat("<color=#77AADD>===[Send]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                }
            }
            else if (!visibleSendPacket.IsEmpty())
            {
                ClientOpcode code = (ClientOpcode)data[0];
                foreach (var op in visibleSendPacket)
                {
                    if (op == code)
                    {
                        Debug.LogFormat("<color=#77AADD>===[Send]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                        break;
                    }
                }
            }
#endif
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Editor Network Monitor
        //
        //////////////////////////////////////////////////////////////////////////////////////////
#if UNITY_EDITOR
        [Space]
        [Header("Network Monitor")]
        public int _clearTime = 99999999;
        public int _totalSendPacketSize = 0;
        public int _averageSendPacketSize = 0;
        public int _totalReceivePacketSize = 0;
        public int _averageReceivePacketSize = 0;
        public DateTime _packetStartTime = new DateTime(0);
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size)
        {
            if (_packetStartTime.Ticks == 0)
                _packetStartTime = DateTime.Now;
            else if ((DateTime.Now - _packetStartTime).TotalSeconds > _clearTime)
            {
                _packetStartTime = DateTime.Now;
                _totalSendPacketSize = _totalReceivePacketSize = _averageReceivePacketSize = _averageSendPacketSize = 0;
            }

            _totalSendPacketSize += size + 2;
            _averageSendPacketSize = (int)(_totalSendPacketSize / (DateTime.Now - _packetStartTime).TotalSeconds);
        }
        void AddReceiveMonitor(int size)
        {
            if (_packetStartTime.Ticks == 0)
                _packetStartTime = DateTime.Now;
            else if ((DateTime.Now - _packetStartTime).TotalSeconds > _clearTime)
            {
                _packetStartTime = DateTime.Now;
                _totalSendPacketSize = _totalReceivePacketSize = _averageReceivePacketSize = _averageSendPacketSize = 0;
            }

            _totalReceivePacketSize += size + 2;
            _averageReceivePacketSize = (int)(_totalReceivePacketSize / (DateTime.Now - _packetStartTime).TotalSeconds);
        }
#else
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size){}
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddReceiveMonitor(int size){}
#endif
    }
}