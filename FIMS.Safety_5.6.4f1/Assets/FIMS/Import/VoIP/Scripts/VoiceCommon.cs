﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF.VoIP
{
    public static class VoiceOpcode
    {
        public const byte OPCODE_VOICE_INFO = 1;
        public const byte OPCODE_VOICE_DATA = 2;
    }

    public interface IVoiceSender
    {
        bool IsInitialized { get; }

        void RestartSender();
        void StartSender();
        void Broadcast(byte[] sendBuffer, int v, int length);
    }
}