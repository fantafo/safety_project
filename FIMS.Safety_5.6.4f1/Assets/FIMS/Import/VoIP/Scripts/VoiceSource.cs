﻿using POpusCodec;
using POpusCodec.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF.VoIP
{
    /// <summary>
    /// 입력된 ip, port로 접속하여 서버에서 데이터를 전송받는다.
    /// 전송받은 데이터를 AudioStreamPlayer를 통해서 디바이스에 송출한다.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu("Voice/Voice Source")]
    public class VoiceSource : MonoBehaviour
    {
        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Member Variables
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        //-- Inspector
        [SerializeField]
        private int _playDelayMs = 200;

        //-- AudioPlayer
        AudioStreamPlayer player;
        OpusDecoder decoder;
        bool isDirty;

        //-- public Properties
        public long LastRecvTime { get; private set; } // Time when last audio packet was received for the speaker.
        public bool IsPlaying { get { return this.player.IsPlaying; } } // Is the speaker playing right now.

        public bool InitAudioPlayer { get; set; }
        public int Frequency { get; set; }// 전송될 음성 주사율
        public int Channel { get; set; } // 전송될 음성 체널 수
        public int FrameSamplesPerChannel { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Unity Lifecycles
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void Awake()
        {
            this.player = new AudioStreamPlayer(GetComponent<AudioSource>(), "VoiceSpeaker:", false);
        }

        private void OnDisable()
        {
            this.player.Stop();
        }

        private void Update()
        {
            if (isDirty)
            {
                Init(Frequency, Channel, FrameSamplesPerChannel, _playDelayMs);
            }
            if(InitAudioPlayer)
            {
                this.player.Update();
            }
        }

        private void OnDestroy()
        {
            this.player.Stop();
        }

        private void OnApplicationQuit()
        {
            this.player.Stop();
        }

        private void OnValidate()
        {
            if (this.player != null && InitAudioPlayer)
            {
                isDirty = true;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Voice Helper
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        public void Init(int freq, int channel, int frameSamplesPerChannel, int playDelayMs)
        {
            this.player.Start(freq, channel, frameSamplesPerChannel, playDelayMs);
            decoder = new OpusDecoder((SamplingRate)freq, (Channels)channel);
            InitAudioPlayer = true;
        }

        public void OnAudioBuffer(byte[] pack)
        {
            this.LastRecvTime = DateTime.Now.Ticks;

            float[] pcm = decoder.DecodePacketFloat(pack);
            this.player.OnAudioFrame(pcm);
        }

        public void OnAudioFrame(float[] frame)
        {
            this.LastRecvTime = DateTime.Now.Ticks;
            this.player.OnAudioFrame(frame);
        }
    }
}