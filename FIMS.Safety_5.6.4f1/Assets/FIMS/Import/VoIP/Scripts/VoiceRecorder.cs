﻿using POpusCodec;
using POpusCodec.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF.VoIP
{

    /// <summary>
    /// 음성을 녹음해서 서버로 전송하는 역할을 한다.
    /// 음성은 마이크 혹은 AudioClip을 통해서 취득한다.
    /// 취득된 자료는 리셈플링, 레벨 조정, 목소리 검출 등의 과정을 거치고,
    /// 그 뒤, Opus Codec으로 압축한 뒤 VoiceServer를 통해 전송된다.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu("Voice/Voice Recorder")]
    public class VoiceRecorder : MonoBehaviour
    {
        public const int DATA_POOL_CAPACITY = 50; // TODO: may depend on data type and properties, set for average audio stream
        public const int DATA_POOL_CAPACITY_1 = DATA_POOL_CAPACITY - 1;

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Member Variables
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        //-- Inspector
        public AudioClip AudioClip;
        public bool LoopAudioClip = true;
        [Space]
        public SamplingRate SamplingRate = SamplingRate.Sampling24000;     // set in inspector
        public FrameDuration FrameDuration = FrameDuration.Frame20ms;   // set in inspector
        [Delayed]
        public int Bitrate = 30000;               // set in inspector
        public bool VoiceDetection = true;
        [Delayed]
        public float VoiceDetectionThreshold = 0.01f;

        //-- component
        IVoiceSender _sender;

        //-- Recording
        bool _isVoiceInitialized; // 녹음할 준비가 완료됨
        internal VoiceInfo _voiceInfo; // 녹음할 정보
        internal IBufferReader<float> _audioStream; // 녹음할 대상
        PrimitiveArrayPool<float> _pushDataBufferPool;

        //-- Record Frame
        int FrameSize;
        Framer<float> _framer;

        //-- process
        EncoderFloat _encoder;
        AudioUtil.LevelMeterFloat _levelMeter;
        AudioUtil.VoiceDetectorFloat _voiceDetector;
        AudioUtil.VoiceDetectorCalibration<float> _voiceDetectorCalibration;

        //-- thread
        bool _disposeThread = true;
        Thread _pushDataAsyncThread;
        Queue<float[]> _pushDataQueue;
        AutoResetEvent _pushDataQueueReady;

        //-- send Buffer
        byte[] _sendBuffer;
        MemoryStream _sendStream;
        BinaryWriter _sendWriter;

        //-- Processors
        int _preProcessorsCnt;
        List<IProcessor<float>> _processors;


        //-- 대상 마이크를 알아내거나, 대상 마이크를 바꾼다.
        string microphoneDevice = null;
        public string MicrophoneDevice
        {
            get { return this.microphoneDevice; }
            set
            {
                if (value != null && !Microphone.devices.Contains(value))
                {
                    Debug.LogErrorFormat("[Voice] {0} is not a valid microphone device.", value);
                    return;
                }

                this.microphoneDevice = value;
                InitializeRecordSet();
            }
        }




        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Unity Lifecycles
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void Awake()
        {
            _sender = GetComponent<IVoiceSender>();

            _sendBuffer = new byte[4096];
            _sendStream = new MemoryStream(_sendBuffer, true);
            _sendWriter = new BinaryWriter(_sendStream, Encoding.UTF8);

            _pushDataQueue = new Queue<float[]>();
            _pushDataQueueReady = new AutoResetEvent(false);
            _processors = new List<IProcessor<float>>();
        }

        private void OnEnable()
        {
            Application.RequestUserAuthorization(UserAuthorization.Microphone);
        }

        private void Start()
        {
            InitializeRecordSet();
        }

        private void OnDestroy()
        {
            DisposeMemories();

            if (_sendWriter != null)
                _sendWriter.Close();
        }

        private void Update()
        {
            if (_isVoiceInitialized)
            {
                float[] buffer = _pushDataBufferPool.AcquireOrCreate();
                while (_audioStream.Read(buffer))
                {
                    PushDataAsync(buffer);
                    buffer = _pushDataBufferPool.AcquireOrCreate();
                }
                _pushDataBufferPool.Release(buffer, buffer.Length);
            }
        }

        private void OnValidate()
        {
            if (_isVoiceInitialized)
            {
                InitializeRecordSet();
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                              Send Data Thread
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 녹음된 데이터가 들어온다.
        /// Unity.Update에서 audioStream에서 읽은 정보를 받는다.
        /// 이곳을 통해 전송 쓰레드에 입력된다.
        /// </summary>
        private void PushDataAsync(float[] buffer)
        {
            if (_disposeThread)
                return;

            lock (_pushDataQueue)
            {
                if (_pushDataQueue.Count < DATA_POOL_CAPACITY_1)
                {
                    _pushDataQueue.Enqueue(buffer);
                    _pushDataQueueReady.Set();
                }
                else
                {
                    _pushDataBufferPool.Release(buffer);
                }
            }
        }

        /// <summary>
        /// 전송 쓰레드의 전신으로 PushDataAsync로 들어온 데이터를 처리하여 전송한다.
        /// </summary>
        private void PushDataAsyncThread()
        {
            try
            {
                while (!_disposeThread)
                {
                    _pushDataQueueReady.WaitOne(); // Wait until data is pushed to the queue or Dispose signals.

                    if (_disposeThread)
                        break;

                    while (true) // Dequeue and process while the queue is not empty
                    {
                        float[] block = null;
                        lock (_pushDataQueue)
                        {
                            if (_pushDataQueue.Count > 0)
                            {
                                block = _pushDataQueue.Dequeue();
                            }
                        }
                        if (block != null)
                        {
                            ProcessData(block);
                            _pushDataBufferPool.Release(block);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[Voice] Exception in encode thread: " + e);
                throw e;
            }
        }

        /// <summary>
        /// 데이터를 프로세싱하여 서버에 전송한다.
        /// </summary>
        private void ProcessData(float[] buf)
        {
            foreach (var framed in _framer.Frame(buf))
            {
                float[] processed = processFrame(framed);
                if (processed != null)
                {
                    ArraySegment<byte> sendBuf = _encoder.EncodeAndGetOutput(processed);
                    SendFrame(sendBuf);
                }
            }
        }

        private void SendFrame(ArraySegment<byte> compressed)
        {
            _sendStream.SetLength(0);
            _sendWriter.Write((byte)VoiceOpcode.OPCODE_VOICE_DATA);
            _sendWriter.Write((ushort)compressed.Count);
            _sendWriter.Write(compressed.Array, compressed.Offset, compressed.Count);

            _sender.Broadcast(_sendBuffer, 0, (int)_sendStream.Length);
        }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                              PCM Processor
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private float[] processFrame(float[] buf)
        {
            foreach (var p in _processors)
            {
                buf = p.Process(buf);
                if (buf == null)
                {
                    break;
                }
            }
            return buf;
        }

        private void AddPostProcessor(IProcessor<float> processor)
        {
            this._processors.Add(processor);
        }

        private void AddPreProcessor(IProcessor<float> processor)
        {
            this._processors.Insert(_preProcessorsCnt++, processor);
        }


        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Helpers
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void DisposeMemories()
        {
            _disposeThread = true;

            if (_pushDataAsyncThread != null)
            {
                _pushDataQueueReady.Set();
                _pushDataAsyncThread.Join();
            }

            if (_audioStream != null)
                _audioStream.Dispose();

            if (_pushDataBufferPool != null)
                _pushDataBufferPool.Dispose();

            if (_encoder != null)
                _encoder.Dispose();

            lock (_pushDataQueue)
            {
                _pushDataQueue.Clear();
            }

            if (_processors != null)
                _processors.Clear();
            _preProcessorsCnt = 0;
        }

        private void InitializeRecordSet()
        {
            DisposeMemories();

            // 세팅할 대상으로 리더를 만들어낸다.
            // AudioClip이 존재할 경우 AudioClip에서 대상 Clip을 전송한다.
            // AudioClip이 존재하지 않을 경우에는 마이크에서 읽어 전송하게된다.
            int channels = 0;
            int sourceSamplingRate = 0;
            if (AudioClip == null)
            {
                if (Microphone.devices.Length == 0)
                {
                    Debug.LogError("[Voice] Not have support Mic from device");
                    return;
                }

                var mic = new MicWrapper(MicrophoneDevice, (int)SamplingRate);
                sourceSamplingRate = mic.SourceSamplingRate;
                channels = mic.Channels;
                this._audioStream = mic;
            }
            else
            {
                this._audioStream = new AudioClipWrapper(AudioClip);
                sourceSamplingRate = AudioClip.frequency;
                channels = AudioClip.channels;
                if (this.LoopAudioClip)
                {
                    ((AudioClipWrapper)_audioStream).Loop = true;
                }
            }

            // 레코딩 데이터를 정의한다.
            _voiceInfo = VoiceInfo.CreateAudioOpus(SamplingRate, sourceSamplingRate, channels, FrameDuration, Bitrate, -1);

            // 코덱(Opus Codec)을 생성한다.
            _encoder = new EncoderFloat(_voiceInfo);

            // 패킷(프레임)을 생성한다.
            FrameSize = _voiceInfo.SamplingRate != 0 ? _voiceInfo.FrameSize * _voiceInfo.SourceSamplingRate / _voiceInfo.SamplingRate : _voiceInfo.FrameSize;
            _framer = new Framer<float>(FrameSize);

            // 패킷 전송용 풀을 생성한다.
            _pushDataBufferPool = new PrimitiveArrayPool<float>(DATA_POOL_CAPACITY, "Data");
            _pushDataBufferPool.Init(FrameSize);

            // 전처리 프로세스를 생성한다.
            {
                if (_voiceInfo.SourceSamplingRate != _voiceInfo.SamplingRate)
                    AddPostProcessor(new AudioUtil.Resampler<float>(_voiceInfo.FrameSize, _voiceInfo.Channels));

                _levelMeter = new AudioUtil.LevelMeterFloat(_voiceInfo.SamplingRate, _voiceInfo.Channels);
                AddPostProcessor(_levelMeter);

                _voiceDetector = new AudioUtil.VoiceDetectorFloat(_voiceInfo.SamplingRate, _voiceInfo.Channels);
                _voiceDetector.On = VoiceDetection;
                _voiceDetector.Threshold = VoiceDetectionThreshold;
                AddPostProcessor(_voiceDetector);

                _voiceDetectorCalibration = new AudioUtil.VoiceDetectorCalibration<float>(_voiceDetector, _levelMeter, _voiceInfo.SamplingRate * _voiceInfo.Channels);
                AddPostProcessor(_voiceDetectorCalibration);
            }

            //-- 서버를 시작하거나 재정의한다.
            if (_sender.IsInitialized)
            {
                _sender.RestartSender();
            }
            else
            {
                _sender.StartSender();
            }

            //-- 읽기 쓰레드를 시작한다.
            _isVoiceInitialized = true;
            _disposeThread = false;
            _pushDataAsyncThread = new Thread(PushDataAsyncThread);
            _pushDataAsyncThread.Name = "Voice: Push Data Async Thread";
            _pushDataAsyncThread.Start();
        }
    }
}