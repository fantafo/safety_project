﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF.VoIP
{
    public abstract class ObjectPool<TType, TInfo> : IDisposable
    {
        protected int capacity;
        protected TInfo info;
        private TType[] freeObj = new TType[0];
        protected int pos;
        protected string name;
        private bool inited;
        abstract protected TType createObject(TInfo info);
        abstract protected void destroyObject(TType obj);
        abstract protected bool infosMatch(TInfo i0, TInfo i1);
        internal string LogPrefix { get { return "[ObjectPool] [" + name + "]"; } }

        public ObjectPool(int capacity, string name)
        {
            this.capacity = capacity;
            this.name = name;
        }

        public ObjectPool(int capacity, string name, TInfo info)
        {
            this.capacity = capacity;
            this.name = name;
            Init(info);
        }

        public void Init(TInfo info)
        {
            lock (this)
            {
                while (pos > 0)
                {
                    destroyObject(freeObj[--pos]);
                }
                this.info = info;
                this.freeObj = new TType[capacity];
                inited = true;
            }
        }

        // Creates from the info given in constructor if fails to get from pool.
        public TType AcquireOrCreate()
        {
            lock (this)
            {
                if (pos > 0)
                {
                    return freeObj[--pos];
                }
                if (!inited)
                {
                    throw new Exception(LogPrefix + " not initialized");
                }
            }
            return createObject(this.info);
        }

        // Acquires from pool only if info matches, otherwise creates object from passed info
        public TType AcquireOrCreate(TInfo info)
        {
            // TODO: this.info thread safety
            if (!infosMatch(this.info, info))
            {
                Init(info);
            }
            return AcquireOrCreate();
        }

        // Returns to pool only if info matches
        virtual public bool Release(TType obj, TInfo objInfo)
        {
            // TODO: this.info thread safety
            if (infosMatch(this.info, objInfo))
            {
                lock (this)
                {
                    if (pos < freeObj.Length)
                    {
                        freeObj[pos++] = obj;
                        return true;
                    }
                }
            }

            // destroy if can't reuse
            //UnityEngine.Debug.Log(LogPrefix + " Release(Info) destroy");
            destroyObject(obj);
            // TODO: log warning
            return false;
        }

        virtual public bool Release(TType obj)
        {
            lock (this)
            {
                if (pos < freeObj.Length)
                {
                    freeObj[pos++] = obj;
                    return true;
                }
            }

            // destroy if can't reuse
            //UnityEngine.Debug.Log(LogPrefix + " Release destroy " + pos);
            destroyObject(obj);
            // TODO: log warning
            return false;
        }
        public void Dispose()
        {
            lock (this)
            {
                while (pos > 0)
                {
                    destroyObject(freeObj[--pos]);
                }
                freeObj = new TType[0];
            }
        }
    }
    public class PrimitiveArrayPool<T> : ObjectPool<T[], int>
    {
        public PrimitiveArrayPool(int capacity, string name) : base(capacity, name) { }
        public PrimitiveArrayPool(int capacity, string name, int info) : base(capacity, name, info) { }
        protected override T[] createObject(int info)
        {
            //UnityEngine.Debug.Log(LogPrefix + " Create " + pos);
            return new T[info];
        }

        protected override void destroyObject(T[] obj)
        {
            //UnityEngine.Debug.Log(LogPrefix + " Dispose " + pos + " " + obj.GetHashCode());
        }

        protected override bool infosMatch(int i0, int i1)
        {
            return i0 == i1;
        }
    }
}
