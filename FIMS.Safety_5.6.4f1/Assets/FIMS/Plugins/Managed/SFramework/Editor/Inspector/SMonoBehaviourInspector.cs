﻿using UnityEngine;
using System.Linq;
using UnityEditor;
using uobject = UnityEngine.Object;
using System;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions.Editors
{
    [CanEditMultipleObjects, CustomEditor(typeof(SMonoBehaviour), true)]
    public class SMonoBehaviourInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDescription(target.GetType());
            DrawDefaultInspector();
            DrawInspectorButtons(targets);
        }

        public static void DrawDescription(Type type)
        {
            var atts = type.GetCustomAttributes(typeof(DescriptionAttribute), true);
            foreach(var att in atts)
            {
                EditorGUILayout.HelpBox(((DescriptionAttribute)att).description, MessageType.Info, true);
            }
        }

        public static void DrawInspectorButtons(uobject[] targets)
        {
            foreach (var m in targets[0].GetType().GetMethods())
            {
                foreach (var at in m.GetCustomAttributes(typeof(ButtonAttribute), true))
                {
                    ButtonAttribute attr = at as ButtonAttribute;
                    if (attr.shouldDraw)
                    {
                        if (attr.space != 0)
                            GUILayout.Space(attr.space);

                        if (string.IsNullOrEmpty(attr.labelName))
                        {
                            attr.labelName = m.Name.Replace('_', ' ');
                        }
                        if (GUILayout.Button(attr.labelName))
                        {
                            if (string.IsNullOrEmpty(attr.methodName))
                            {
                                foreach (var o in targets)
                                    m.Invoke(o, null);
                            }
                            else
                            {
                                var method = attr.GetMethod(targets);
                                if (method != null)
                                {
                                    foreach (var o in targets)
                                        method.Invoke(o, null);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}