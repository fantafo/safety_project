﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions
{
    public class CommonCommands
    {
        [MenuItem("Window/Close Window %w", priority = int.MaxValue)]
        public static void CloseWindow()
        {
            if (EditorWindow.focusedWindow)
            {
                EditorWindow.focusedWindow.Close();
            }
        }
    }
}