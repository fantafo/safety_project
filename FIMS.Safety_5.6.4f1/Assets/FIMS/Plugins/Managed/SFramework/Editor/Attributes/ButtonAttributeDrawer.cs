﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Reflection;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions
{
    [CustomPropertyDrawer(typeof(ButtonAttribute))]
    public class ButtonAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect _position, SerializedProperty _property, GUIContent _label)
        {
            var attr = (ButtonAttribute)attribute;
            if (attr.shouldDraw)
            {
                if (!string.IsNullOrEmpty(attr.labelName))
                    _label = new GUIContent(attr.labelName);

                var target = _property.serializedObject.targetObject;
                var method = attr.GetMethod(target);
                if (method != null && GUI.Button(_position, _label))
                {
                    method.Invoke(_property.serializedObject.targetObject, null);
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var attr = (ButtonAttribute)attribute;
            var method = attr.GetMethod(property.serializedObject.targetObject);

            if (method != null && attr.shouldDraw)
            {
                return base.GetPropertyHeight(property, label);
            }
            else
            {
                return 0;
            }
        }
    }
}