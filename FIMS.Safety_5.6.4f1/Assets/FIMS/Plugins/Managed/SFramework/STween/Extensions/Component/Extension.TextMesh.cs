﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenTextMeshExtension
{

    private static readonly Action<TextMesh, float> _changeCharacterSize = __changeCharacterSize;
    private static void __changeCharacterSize(TextMesh textMesh, float val) { textMesh.characterSize = val; }
    public static STweenState twnCharacterSize(this TextMesh textMesh, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, textMesh.characterSize, to, duration, _changeCharacterSize);
    }
    public static STweenState twnCharacterSize(this TextMesh textMesh, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, from, to, duration, _changeCharacterSize);
    }


    private static readonly Action<TextMesh, float> _changeLineSpacing = __changeLineSpacing;
    private static void __changeLineSpacing(TextMesh textMesh, float val) { textMesh.lineSpacing = val; }
    public static STweenState twnLineSpacing(this TextMesh textMesh, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, textMesh.lineSpacing, to, duration, _changeLineSpacing);
    }
    public static STweenState twnLineSpacing(this TextMesh textMesh, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, from, to, duration, _changeLineSpacing);
    }


    private static readonly Action<TextMesh, float> _changeOffsetZ = __changeOffsetZ;
    private static void __changeOffsetZ(TextMesh textMesh, float val) { textMesh.offsetZ = val; }
    public static STweenState twnOffsetZ(this TextMesh textMesh, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, textMesh.offsetZ, to, duration, _changeOffsetZ);
    }
    public static STweenState twnOffsetZ(this TextMesh textMesh, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, from, to, duration, _changeOffsetZ);
    }


    private static readonly Action<TextMesh, float> _changeTabSize = __changeTabSize;
    private static void __changeTabSize(TextMesh textMesh, float val) { textMesh.tabSize = val; }
    public static STweenState twnTabSize(this TextMesh textMesh, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, textMesh.tabSize, to, duration, _changeTabSize);
    }
    public static STweenState twnTabSize(this TextMesh textMesh, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<TextMesh>, TextMesh, float>(textMesh, from, to, duration, _changeTabSize);
    }


    private static readonly Action<TextMesh, int> _changeFontSize = __changeFontSize;
    private static void __changeFontSize(TextMesh textMesh, int val) { textMesh.fontSize = val; }
    public static STweenState twnFontSize(this TextMesh textMesh, int to, int duration)
    {
        return STween.Play<ActionIntObject<TextMesh>, TextMesh, int>(textMesh, textMesh.fontSize, to, duration, _changeFontSize);
    }
    public static STweenState twnFontSize(this TextMesh textMesh, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<TextMesh>, TextMesh, int>(textMesh, from, to, duration, _changeFontSize);
    }


    private static readonly Action<TextMesh, Color> _changeColor = __changeColor;
    private static void __changeColor(TextMesh textMesh, Color val) { textMesh.color = val; }
    public static STweenState twnColor(this TextMesh textMesh, Color to, float duration)
    {
        return STween.Play<ActionColorObject<TextMesh>, TextMesh, Color>(textMesh, textMesh.color, to, duration, _changeColor);
    }
    public static STweenState twnColor(this TextMesh textMesh, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<TextMesh>, TextMesh, Color>(textMesh, from, to, duration, _changeColor);
    }



}