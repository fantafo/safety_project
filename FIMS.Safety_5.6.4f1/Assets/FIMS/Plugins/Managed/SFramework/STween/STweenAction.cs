﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

namespace SFramework.TweenAction
{
    /// <summary>
    /// Abstract The Tween class action
    /// </summary>
    [System.Serializable]
    public abstract class BaseTweenAction
    {
        public STweenState _state;
        public System.Type _type;

        /// <summary>
        /// Initilaie Variables
        /// </summary>
        public virtual object Target { get { return ""; } }
        public virtual void Init() { }
        public virtual void Update(float ratio) { }
        public virtual void Reset() { }
        public virtual void SetArgument(object o) { }
    }

    /// <summary>
    /// Variable (from, to, diff) Action Class
    /// </summary>
    [System.Serializable]
    public abstract class VariableAction<Variable> : BaseTweenAction
    {
        [SerializeField]
        public Variable from;
        [SerializeField]
        public Variable to;
        protected Variable diff;

        public virtual void SetFrom(Variable v)
        {
            from = v;
            CalcDifference();
        }
        public virtual void SetTo(Variable v)
        {
            to = v;
            CalcDifference();
        }

        /// <summary>
        /// Calculate Diffence from (To-From)
        /// </summary>
        public virtual void CalcDifference() { }
    }

    /// <summary>
    /// proc Variable to Component
    /// </summary>
    [System.Serializable]
    public abstract class ComponentAction<Component, Variable> : VariableAction<Variable>
    {
        [SerializeField]
        public Component _target;
        public override object Target { get { return _target; } }

        public void SetComponent(Component comp)
        {
            _target = comp;
        }
    }

    /// <summary>
    /// class is stores the current value.
    /// </summary>
    public abstract class ComponentAction<Component, Variable, Current> : ComponentAction<Component, Variable>
    {
        protected Current _current;
    }

    /// <summary>
    /// Auto Calculate flaot difference
    /// </summary>
    public abstract class ComponentActionDiffFloat<Component, Current> : ComponentAction<Component, float, Current>
    {
        public override void CalcDifference()
        {
            diff = to - from;
        }
    }
}
