﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;

namespace SFramework.TweenAction
{
    [Serializable]
    public class ActionAnchored : ComponentAction<RectTransform, Vector3, Vector3>
    {
        public override void Init()
        {
            SetFrom(_current = _target.anchoredPosition3D);
        }

        public override void CalcDifference() { diff = to - from; }
        public override void Update(float ratio)
        {
            _current = from + (diff * ratio);
            _target.anchoredPosition3D = _current;
        }
    }

    #region RectTransform 1D AnchoredPosition(X,Y,Z)
    [Serializable]
    public class ActionAnchoredX : ComponentActionDiffFloat<RectTransform, Vector3>
    {
        public override void Init()
        {
            _current = _target.anchoredPosition3D;
            SetFrom(_current.x);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.x = v;
        }

        public override void Update(float ratio)
        {
            _current.x = from + (diff * ratio);
            _target.anchoredPosition3D = _current;
        }
    }
    [Serializable]
    public class ActionAnchoredY : ComponentActionDiffFloat<RectTransform, Vector3>
    {
        public override void Init()
        {
            _current = _target.anchoredPosition3D;
            SetFrom(_current.y);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.y = v;
        }

        public override void Update(float ratio)
        {
            _current.y = from + (diff * ratio);
            _target.anchoredPosition3D = _current;
        }
    }
    [Serializable]
    public class ActionAnchoredZ : ComponentActionDiffFloat<RectTransform, Vector3>
    {
        public override void Init()
        {
            _current = _target.anchoredPosition3D;
            SetFrom(_current.z);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.z = v;
        }

        public override void Update(float ratio)
        {
            _current.y = from + (diff * ratio);
            _target.anchoredPosition3D = _current;
        }
    }
    #endregion
}
#endif