﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 코루틴과 관련된 유틸들을 캐싱해 두는 곳이다.
/// </summary>
public class Waits
{
    public static WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();
    public static WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();

    private static WaitForSeconds[] waitForSeconds;
    static Waits()
    {
        waitForSeconds = new WaitForSeconds[1001];
        for (int i = 1; i < waitForSeconds.Length; i++)
        {
            waitForSeconds[i] = new WaitForSeconds(i * 0.01f);
        }
    }

    public static WaitForSeconds Wait(float time)
    {
        int idx = (int)(time * 100);
        if (idx < 1)
            return waitForSeconds[1];
        else if (idx < waitForSeconds.Length)
        {
            return waitForSeconds[idx];
        }
        else
        {
            return new WaitForSeconds(time);
        }
    }
    public static WaitForSeconds Wait(int time)
    {
        int idx = time * 100;
        if (idx < 1)
            return waitForSeconds[1];
        else if (idx < waitForSeconds.Length)
        {
            return waitForSeconds[idx];
        }
        else
        {
            return new WaitForSeconds(time);
        }
    }
}
