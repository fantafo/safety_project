﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class SMath
{
    public static Vector3 SubtractAngles(Vector3 a, Vector3 b)
    {
        return (Quaternion.Euler(a).Inverse() * Quaternion.Euler(b)).eulerAngles;
    }

    public static Vector3 StabilizeAngle(Vector3 eulerAngles)
    {
        eulerAngles.x = StabilizeAngle(eulerAngles.x);
        eulerAngles.y = StabilizeAngle(eulerAngles.y);
        eulerAngles.z = StabilizeAngle(eulerAngles.z);
        return eulerAngles;
    }

    public static float StabilizeAngle(float x)
    {
        return x > 180 ? x - 360 : x;
    }
}
