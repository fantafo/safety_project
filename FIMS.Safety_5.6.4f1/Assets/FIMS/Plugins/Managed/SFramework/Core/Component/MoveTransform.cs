﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

public class MoveTransform : SMonoBehaviour
{
    public Transform target;
    public Vector3 localPosition;
    public Vector3 localRotation;
    public Vector3 localScale = Vector3.one;

    private void Reset()
    {
        localPosition = transform.localPosition;
        localRotation = transform.localEulerAngles;
        localScale = transform.localScale;
    }

    private void Awake()
    {
        if (target != null)
        {
            transform.SetParent(target, false);
            transform.localPosition = localPosition;
            transform.localEulerAngles = localRotation;
            transform.localScale = localScale;
        }
        Destroy(this);
    }
}
