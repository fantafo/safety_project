﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCTitleControl : MonoBehaviour {

    public GameObject m_Title;
    public AudioSource m_TitleEffect;

	// Use this for initialization
	void Start () {

        m_Title.SetActive(true);
        m_TitleEffect.Play();
        StartCoroutine(TitleCo());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    private IEnumerator TitleCo()
    {
        WaitForSeconds wait = new WaitForSeconds(5.0f);
        yield return wait;
        m_Title.SetActive(false);        
    }
}
