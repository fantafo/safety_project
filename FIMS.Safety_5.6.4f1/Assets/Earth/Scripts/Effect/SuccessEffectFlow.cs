﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuccessEffectFlow : MonoBehaviour
{

    [SerializeField]
    float readyTime01 = 0.2f;

    [SerializeField]
    GameObject[] Sobjs;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(effectflow());
    }


    IEnumerator effectflow()
    {
        for (int i = 0; i < Sobjs.Length; i++)
        {
            yield return new WaitForSeconds(readyTime01);
            if(Sobjs[i])Sobjs[i].SetActive(true);
            LeanTween.scale(Sobjs[i], Vector3.one, 1f).setEaseOutBounce();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
