﻿using FTF;
using FTF.Packet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 일반 맵에서 사용하는 코인입니다.
/// </summary>
public class CoinPoint : AutoCommander
{
    public GameObject coin;
    public float percent = 0.4f;
    public int score = 1;
    public bool active;
    public bool used;

    protected override void Awake()
    {
        base.Awake();
        coin.SetActive(false);
        percent = 0.5f;
    }

    public void Start()
    {
        float v = Random.Range(0f, 1f);
        if (PlayerInstance.main.IsRoomMaster)
        {
            print(name + "/ Start/ " + v + " < " + percent);
            if (v <= percent)
            {
                print(name + "/ Start/ Request Activie!");
                STween.delayCall(1f, () =>
                {
                    if (this)
                    {
                        Broadcast("Active", name);
                    }
                });
            }
            else
            {
                Broadcast("DestroyCoin", name);
            }
        }
    }

    [OnCommand]
    private void Active(string tname)
    {
        print(tname + "/ Active/ " + tname + " == " + name);
        if (tname == name)
        {
            print(name + "/ Active/ Active!!");
            active = true;
            used = false;
            coin.SetActive(true);

            var entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback = new EventTrigger.TriggerEvent();
            entry.callback.AddListener((b) =>
            {
                Broadcast("Finding", PlayerInstance.main.instanceId, tname);
            });

            var trig = gameObject.AddComponent<EventTrigger>();
            trig.triggers.Add(entry);
            print(name + "/ Active/ Active complete!!");
        }
    }

    [OnCommand]
    private void Finding(int instanceID, string tname)
    {
        if(!active)
            return;

        // 사용된 적이 있는 코인인 경우에는 삭제처리한다.
        if (used)
        {
            print(name + "/ Finding/ it's used coin!");
            Broadcast("DestroyCoin", name);
            return;
        }

        print(name + "/ Finding/ Master:" + PlayerInstance.main.IsRoomMaster + " / " + tname + "==" + name);
        if (PlayerInstance.main.IsRoomMaster && tname == name)
        {
            print(name + "/ Finding/ Update Score " + score);
            used = true;

            Networker.Send(C_Scene.UpdateScore(instanceID, C_Scene.UpdateType.Relative, score));

            var player = PlayerManager.GetPlayer(instanceID);
            if (player != null)
            {
                Send(player, "CoinEffect", tname);
            }
            Broadcast("DestroyCoin", this.name);
        }
    }

    [OnCommand]
    private void CoinEffect(string tname)
    {
        if (!active)
            return;

        print(name + "/ CoinEffect " + tname + " == " + name);
        if (tname == name)
        {
            var go = Instantiate(Resources.Load("CoinEffect"), transform.position, transform.rotation, null);
            STween.delayCall(1.1f, () => Destroy(go));
        }
    }

    [OnCommand]
    private void DestroyCoin(string tname)
    {
        print(name + "/ DestroyCoin "+tname+" == "+name);
        if (tname == name)
        {
            used = true;
            print(name + "/ Destroied");
            Destroy(gameObject);

        }
    }
}
