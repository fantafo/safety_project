﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// 카탈로그의 개별아이템의 선택에 관련된 이벤트가 일어납니다.
    /// </summary>
    public class CatalogItem : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Catalog catalog;
        public Image thumbnail;
        public Text voteText;
        public Text locateText;
        public Text nameText;
        public GameObject marker;

        [Multiline(10)]
        public string scriptSource;

        private int _vote = 0;
        public int Vote { get { return _vote; } set { _vote = value; voteText.text = value.ToString(); } }

        private void OnEnable()
        {
            Vote = 0;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            var index = transform.GetSiblingIndex();

            if (PlayerInstance.lookMain.IsRoomMaster)
            {
                ResourceLoader.main.Broadcast("Show", DemoEarthController.main.currentCountry.GetPrefabPath(index));
            }
            else
            {
                if(catalog.voteTarget != -1)
                    catalog.Broadcast("UnvoteItem", catalog.name, catalog.voteTarget);
                catalog.Broadcast("VoteItem", catalog.name, catalog.voteTarget = index);

                foreach (var item in catalog.items)
                {
                    if (item && item != this && item.marker)
                        item.marker.SetActive(false);
                }
                marker.SetActive(true);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            GetComponent<Animator>().SetBool("hover", false);

            if (scriptSource.Exists())
            {
                var ss = catalog.GetComponent<ScriptSource>();
                if (ss != null)
                {
                    ScriptController.main.scriptText.text = ss.text;
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            GetComponent<Animator>().SetBool("hover", true);

            if(scriptSource.Exists())
            {
                ScriptController.main.scriptText.text = scriptSource;
            }
        }
    }
}