﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

namespace FTF.Earth
{
    /// <summary>
    /// 카탈로그 메인컴포넌트로
    /// 애니메이션이나, 선택및 투표에 관한 컴포넌트입니다.
    /// </summary>
    public class Catalog : AutoCommander
    {
        public Animator animator;
        public CatalogItem[] items;
        public int voteTarget; // 내가 몇번째를 추천했는지 나타냅니다.

        protected override void Awake()
        {
            base.Awake();

            items.SetGameObectActive(false);
            foreach (var it in items)
            {
                if (it)
                {
                    it.catalog = this;
                }
            }

            RenderSettings.skybox.SetFloat("_value", 0);
            Resources.UnloadUnusedAssets();
        }


        /////////////////////////////////////////////////////////////////////////////////////////
        //
        // ActiveForCountry에서 SendMessage로 보내준다.
        //
        /////////////////////////////////////////////////////////////////////////////////////////
        public void Show(ActiveArgs delay)
        {
            SetVisible(true);
        }
        public void Hide(ActiveArgs args)
        {
            SetVisible(false);
        }

        STweenState state;
        void SetVisible(bool active)
        {
            state.TryStop();
            state = null;

            if (active)
            {
                voteTarget = -1;
                animator.SetBool("Open", true);
                foreach(var item in items)
                {
                    if (item.marker)
                    {
                        item.marker.SetActive(false);
                    }
                }
            }
            else
            {
                animator.SetBool("Open", false);
                state = STween.delayCall(1, () => { gameObject.SetActive(false); state = null; });
            }
            Resources.UnloadUnusedAssets();
        }

        /////////////////////////////////////////////////////////////////////////////////////////
        //
        // Commands
        //
        /////////////////////////////////////////////////////////////////////////////////////////
        [OnCommand]
        public void VoteItem(string name, int index)
        {
            if (name == this.name)
            {
                Debug.Log("VoteItem " + index + " /" + items.Length);
                items[index].Vote++;
                Debug.Log("VoteItemComplete");
            }
        }

        [OnCommand]
        public void UnvoteItem(string name, int index)
        {
            if (name == this.name)
            {
                Debug.Log("UnvoiteItem " + index + " /" + items.Length);
                items[index].Vote--;
                Debug.Log("UnvoteItemComplete");
            }
        }
    }
}
