﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 나라별 설명을 하기위한 PPT와 같은 화면을 구성한다.
    /// 컴포넌트가 포함된 오브젝트의 자식 오브젝트가 각각 페이지가 된다.
    /// </summary>
    public class Infographic : AutoCommander
    {
        public int currentIndex;
        public float duration = 1;
        public float alphaDur = 0.7f;
        public SEasingType outer = SEasingType.easeX2Out, inner = SEasingType.easeX2Out;
        public ActiveForCountry activer;

        protected override void OnEnable()
        {
            base.OnEnable();
            activer = GetComponentInParent<ActiveForCountry>();
        }

        public void Show(ActiveArgs args)
        {
            gameObject.SetActive(true);
            for (int i = 0, len = transform.childCount; i < len; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
            SetPage(-1, currentIndex = 0);
            args.AddDelay(duration + 0.5f);
        }
        public void Hide(ActiveArgs args)
        {
            SetPage(currentIndex, 50000);
            STween.delayCall(duration, () => gameObject.SetActive(false));
            args.AddDelay(duration + 0.5f);
        }

        [Button]

        public void Next()
        {
            if (KeyStack.Is(activer))
            {
                var next = Mathf.Clamp(currentIndex + 1, 0, transform.childCount - 1);
                if (currentIndex != next)
                {
                    Broadcast("SetPage", currentIndex, next);
                    currentIndex = next;
                }
            }
        }

        [Button]
        public void Prev()
        {
            if (KeyStack.Is(activer))
            {
                var next = Mathf.Clamp(currentIndex - 1, 0, transform.childCount - 1);
                if (currentIndex != next)
                {
                    Broadcast("SetPage", currentIndex, next);
                    currentIndex = next;
                }
            }
        }

        [OnCommand]
        void SetPage(int currId, int nextId)
        {
            if (currId < nextId)
            {
                var current = GetChild(currId);
                var next = GetChild(nextId);

                if (current != null)
                {
                    STween.scale(current, Vector3.one, Vector3.one * 0.5f, duration).Ease(outer).Ease(outer);
                    STween.rotateLocal(current, Vector3.zero, new Vector3(0, -70, 0), duration).Ease(outer);
                    STween.moveLocal(current, new Vector3(0, 250, 0), duration).Ease(outer);
                    STween.alpha(current, 0, alphaDur).SetOnComplete(() => { current.gameObject.SetActive(false); });
                }
                if (next != null)
                {
                    next.gameObject.SetActive(true);
                    STween.scale(next, Vector3.one * 0.5f, Vector3.one, duration).Ease(outer).Ease(outer);
                    STween.rotateLocal(next, new Vector3(0, 45, 0), Vector3.zero, duration).Ease(outer);
                    STween.moveLocal(next, new Vector3(0, -250, 0), Vector3.zero, duration).Ease(inner);
                    STween.alpha(next, 0, 1, alphaDur);
                }
            }
            else
            {
                var current = GetChild(currId);
                var next = GetChild(nextId);

                if (current != null)
                {
                    STween.scale(current, Vector3.one, Vector3.one * 0.5f, duration).Ease(outer).Ease(outer);
                    STween.rotateLocal(current, Vector3.zero, new Vector3(0, -70, 0), duration).Ease(outer);
                    STween.moveLocal(current, new Vector3(0, -250, 0), duration).Ease(outer);
                    STween.alpha(current, 0, alphaDur).SetOnComplete(() => { current.gameObject.SetActive(false); });
                }
                if (next != null)
                {
                    next.gameObject.SetActive(true);
                    STween.scale(next, Vector3.one * 0.5f, Vector3.one, duration).Ease(outer).Ease(outer);
                    STween.rotateLocal(next, new Vector3(0, 45, 0), Vector3.zero, duration).Ease(outer);
                    STween.moveLocal(next, new Vector3(0, 250, 0), Vector3.zero, duration).Ease(inner);
                    STween.alpha(next, 0, 1, alphaDur);
                }
            }
            STween.UpdateAll(false);
            currentIndex = nextId;
        }
        Transform GetChild(int index)
        {
            if (index >= 0 && index < transform.childCount)
            {
                return transform.GetChild(index);
            }
            return null;
        }
    }
}
