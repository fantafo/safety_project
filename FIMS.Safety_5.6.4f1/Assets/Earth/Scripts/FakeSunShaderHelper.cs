﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 스핑크스, 아부심벨 등 모델의 빛방향이 고정되게끔만들어놓은 FakeSun Shader를 도와주는 컴포넌트입니다.
    /// FakeSun Shader를 재질로 갖는 모델에 컴포넌트를 추가해주셔야 정상적으로 작동하게됩니다.
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    public class FakeSunShaderHelper : SMonoBehaviour
    {
        public Vector3 sunPos;

        Material[] materials;
        Renderer rend;
        int id;

        private void Start()
        {
            if (materials == null)
            {
                rend = GetComponent<MeshRenderer>();
                if (rend != null)
                {
                    if (Application.isPlaying)
                        materials = rend.materials;
                    else
                        materials = rend.sharedMaterials;
                    id = Shader.PropertyToID("_lightDir");
                }
            }
        }

        private void LateUpdate()
        {
            if (transform.hasChanged)
            {
                var lightDir = (transform.rotation * this.sunPos).normalized;
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i].SetVector(id, lightDir);
                }
                transform.hasChanged = false;
            }
        }

        private void OnValidate()
        {
            if (!Application.isPlaying)
                Start();
            transform.hasChanged = true;
        }
    }
}
