﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WPM;

namespace FTF.Earth
{
    /// <summary>
    /// 가끔 알 수 없는 문제로 유저들과의 싱크가 맞지 않는 경우가 있어서,
    /// 모두 초기화 시키기 위한 컴포넌트입니다.
    /// </summary>
    public class ClearAll : AutoCommander
    {
        public void BroadcastExit()
        {
            Broadcast("Exit");
        }

        [OnCommand]
        void Exit()
        {
            ScreenFader.main.FadeOutIn(0.4f, () =>
            {
                ResourceLoader.main.Clear();
                ModelViewer.current = null;

                /*
                foreach (var afc in GameObject.FindObjectsOfType<ActiveForCountry>())
                {
                    afc.SetActive(false);
                }
                */
                ActiveForCountry[] afcs = GameObject.FindObjectsOfType<ActiveForCountry>();
                for (int i = 0; i < afcs.Length; ++i)
                {
                    afcs[i].SetActive(false);
                }

                KeyStack.Clear();

                DemoEarthController.main.gameObject.SetActive(true);
            });
        }
    }
}