﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 선생님의 포인터 옆에 따라오는 스크립트의 정보를 가지고 있는 컴포넌트이며,
/// 컴포넌트가 계속 포인터 옆에 있을 수 있도록 유지시킵니다.
/// </summary>
public class ScriptController : MonoBehaviour
{
    static ScriptController _main;
    public static ScriptController main
    {
        get
        {
            if(_main == null)
            {
                _main = GameObject.FindObjectOfType<ScriptController>();
            }
            return _main;
        }
    }

    public GameObject scriptObject;
    public ScrollRect scrollRect;
    public Text scriptText;
    public float distance;
    public float speed = 1;
    private Camera _mainCamera;

    void Awake()
    {
        _main = this;
    }

    private void Start()
    {
        _mainCamera = Camera.main;
    }

    private void Update()
    {
        if (_mainCamera != null && scriptObject != null)
        {
            Vector3 cursorPosition = FTFReticlePosition.Value;
#if FTF_OBSERVER
            if(PlayerInstance.lookMain != null && PlayerInstance.lookMain.info && PlayerInstance.lookMain.info.model)
            {
                cursorPosition = PlayerInstance.lookMain.info.model.laser.Reticle.position;
            }
#endif
            var ray = _mainCamera.ScreenPointToRay(_mainCamera.WorldToScreenPoint(cursorPosition));
            scriptObject.transform.position = ray.GetPoint(distance);
        }
    }

    //void Update()
    //{
    //    if (scriptObject.activeSelf)
    //    {
    //        if (ControllerDirHelper.isDown)
    //        {
    //            if (VRInput.ClickButtonDown)
    //            {
    //                scriptObject.SetActive(false);
    //            }

    //            if (VRInput.IsTouching)
    //            {
    //                float y = VRInput.TouchDir.y;
    //                if (Mathf.Abs(y) > 0.5)
    //                {
    //                    scrollRect.verticalNormalizedPosition -= y * Time.deltaTime * speed;
    //                }
    //            }
    //        }

    //        var ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(FTFReticlePosition.Value));
    //        scriptObject.transform.position = ray.GetPoint(distance);
    //    }
    //    else
    //    {
    //        if (ControllerDirHelper.isDown && VRInput.ClickButtonDown)
    //        {
    //            scriptObject.SetActive(true);
    //            scrollRect.verticalNormalizedPosition = 1;
    //        }
    //    }
    //}
}
