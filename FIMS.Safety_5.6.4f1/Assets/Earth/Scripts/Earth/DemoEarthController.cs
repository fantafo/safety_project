﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WPM;

namespace FTF.Earth
{
    /// <summary>
    /// 지구를 움직이기 위한 컴포넌트입니다.
    /// 처음 지구가 나타나는 애니메이션부터, 다른 나라를 선택하는 것까지 관여합니다.
    /// 앞으로 여행할 나라를 미리 선정해놓고 이용할 수 있습니다.
    /// </summary>
    public class DemoEarthController : AutoCommander
    {
        public static DemoEarthController main;

        [Header("Comp")]
        public WorldMapGlobe map;
        public Transform wrapper;
        public GameObject[] showActivators;
        public GameObject[] showDeactivors;
        public GameObject[] enableActivors;

        [Header("Data")]
        public int selectID = 0;
        public CountryData[] countriesData;
        public CountryData currentCountry { get { return countriesData[selectID]; } }

        [Header("ui")]
        public Color countryColor = Color.red;
        public Color frontiersColor = Color.green;
        public Color gridLineColor = Color.green;
        public Text displayCountryName;
        public Vector3 targetSunPosition;

        public bool isAnimating { get; private set; }
        public bool isInit { get; private set; }

        private Transform _mainCameraTr;

        ILog log;

        protected void Start()
        {
            log = new SLogger("EarthControl");

            main = this;
            map.showCursor = false;
            map.enableCountryHighlight = false;
            map.brightness = 0;

            showActivators.SetActive(false);
            enableActivors.SetActive(false);

            _mainCameraTr = Camera.main.transform;
        }
        protected override void OnEnable()
        {
            base.OnEnable();
            if(isInit)
            {
                enableActivors.SetActive(true);

                if(currentCountry != null)
                {
                    ScriptController.main.scriptText.text = currentCountry.script;
                }
            }

            // 남겨져있는 ResourceItem을 제거한다.
            /*
            var objs = GameObject.FindObjectsOfType<ResourceItem>();
            foreach(var obj in objs)
            {
                if (obj)
                {
                    Destroy(obj.gameObject);
                }
            }
            */
            ResourceItem[] objs = GameObject.FindObjectsOfType<ResourceItem>();
            for (int i = 0; i <  objs.Length; ++i)
            {
                if (objs[i])
                {
                    Destroy(objs[i].gameObject);
                }
            }
        }
        protected override void OnDisable()
        {
            if (isInit)
            {
                enableActivors.SetActive(false);
            }
            base.OnDisable();
        }

        public void BroadcastShow()
        {
            Broadcast("Show", selectID);
        }

        [Button]
        [OnCommand]
        public void Show(int select)
        {
            if (!isInit)
            {
                log.Debug("Show " + select);

                isInit = true;
                STween.value(frontiersColor.SetAlpha(0), frontiersColor, 2, (v) => map.frontiersColor = v).Delay(2);
                STween.value(gridLineColor.SetAlpha(0), gridLineColor, 2, (v) => map.gridLinesColor = v).Delay(2);
                STween.value(0, 1, 2, (v) => map.brightness = v);
                STween.value(Vector3.forward, targetSunPosition, 6, (v) =>
                {
                    map.earthScenicLightDirection = v;
                }).SetOnComplete(() =>
                {
                    map.showCursor = true;
                });
                showActivators.SetActive(true);
                showDeactivors.SetActive(false);
                enableActivors.SetActive(true);
                SelectCountry(select, 4, 3);
            }
        }

        [Button]
        public void NextCountry()
        {
            Debug.Log("[my]DemoEarthController.NextCountry()");
            if (isInit && ControllerDirHelper.isUp && !isAnimating && KeyStack.IsNone && isActiveAndEnabled)
            {
                selectID = (selectID + 1) % countriesData.Length;
                Broadcast("SelectCountry", selectID, 2f, 1f);
            }
        }
        [Button]
        public void PrevCountry()
        {
            Debug.Log("[my]DemoEarthController.PrevCountry()");
            if (isInit && ControllerDirHelper.isUp && !isAnimating && KeyStack.IsNone && isActiveAndEnabled)
            {
                selectID = (selectID + countriesData.Length - 1) % countriesData.Length;
                Broadcast("SelectCountry", selectID, 2f, 1f);
            }
        }

        private string beforeToggle;
        [OnCommand]
        public void SelectCountry(int index, float moveDuration, float delayToggle = 1)
        {
            if (!isInit || isAnimating)
                return;

            log.Debug("SelectCountry " + index);
            isAnimating = true;
            var cd = countriesData.TryGet(index);
            var mcd = map.GetCountry(cd.enName);
            if (cd != null && mcd != null)
            {
                // setting
                map.autoRotationSpeed = 0;
                map.enableCountryHighlight = false;

                // Toggle Target
                if (!string.IsNullOrEmpty(beforeToggle))
                    map.ToggleCountrySurface(beforeToggle, false, Color.white);

                if (displayCountryName)
                    displayCountryName.text = "";

                STween.delayCall(Mathf.Min(delayToggle, moveDuration), () =>
                {
                    map.ToggleCountrySurface(beforeToggle = cd.enName, true, countryColor);
                    if (displayCountryName)
                        displayCountryName.text = cd.displayNAme;
                });

                // Fly to Country
                var look = Quaternion.LookRotation(_mainCameraTr.position - map.transform.position);
                look *= Quaternion.LookRotation(mcd.sphereCenter).Inverse();
                STween.rotateQ(map, look, moveDuration).Set(ease: SEasingType.easeX3Out).SetOnComplete(()=> 
                {
                    isAnimating = false;
                });

                // apply script
                if (ScriptController.main)
                {
                    ScriptController.main.scriptText.text = cd.script;
                }
            }
            else
            {
                Debug.LogError(cd.enName + "을 찾을 수 없습니다.");
                isAnimating = false;
            }
        }

        [Button]
        public void PrintAllCoutnries()
        {
            /*
            string print = "";

            foreach (var c in map.countries)
            {
                print += c.name + "\r\n";
            }

            Debug.Log(print);
            */
            const string s = "\r\n";
            StringBuilder print = new StringBuilder();
            for (int i = 0; i < map.countries.Length; ++i)
            {
                print.Append(map.countries[i].name).Append(s);
            }
            Debug.Log(print);
        }
    }

    [Serializable]
    public class CountryData
    {
        public string displayNAme;
        public string enName;
        [Multiline(10)]
        public string script;

        public string GetPrefabPath(int index)
        {
            return "Country/" + enName + "/" + index;
            //return "360/Japan/"+index;
            //return "360/Japan/Thumb/" + index;
        }
    }
}