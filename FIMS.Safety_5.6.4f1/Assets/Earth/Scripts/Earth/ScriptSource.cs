﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 이 스크립트가 enable되면 ScriptController 를 찾아서 스크립트의 메세지를 변경시킵니다.
/// </summary>
public class ScriptSource : MonoBehaviour
{
    [Multiline(20)]
    public string text;

    private void OnEnable()
    {
        if(ScriptController.main != null)
        {
            ScriptController.main.scriptText.text = text;
        }
    }
}
