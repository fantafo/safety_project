﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 라인랜더를 사용하여 컨트롤러로 선을 긋는 용도로 사용합니다.
/// </summary>
public class FTFLineDrawer : AutoCommander
{
    public Material material;
    public float width = 0.001f;
    public float minDrawDistacne = 0.005f;
    public float lastClickStartTime = -100;
    public int smoothCount = 20;
    
    AverageVector3 avrg;
    Transform container;
    float currMinDrawDistance;
    Vector3 lastDrawPosition;

    private Camera _mainCamera;

    protected override void Awake()
    {
        base.Awake();
        avrg = new AverageVector3(smoothCount);
    }
    private void Start()
    {
        _mainCamera = Camera.main;
    }
    private void OnValidate()
    {
        avrg = new AverageVector3(smoothCount);
    }

    bool isStartDraw;
    float distance = 0;
    private void Update()
    {

        if ((VRInput.ClickButton || VRInput.ClickButtonUp || VRInput.ClickButtonDown) && ControllerDirHelper.isUp)
        {
            Ray ray = _mainCamera.ScreenPointToRay(_mainCamera.WorldToScreenPoint(FTFReticlePosition.Value));
            if (!isStartDraw)
            {
                isStartDraw = true;

                RaycastHit hit;
                Vector3 reticlePosition;
                if (Physics.Raycast(ray, out hit, _mainCamera.farClipPlane * 0.9f))
                {
                    distance = hit.distance;
                    avrg.Set(hit.point);
                }
                else
                {
                    distance = _mainCamera.farClipPlane * 0.9f;
                    avrg.Set(ray.GetPoint(distance));
                }
                reticlePosition = avrg;

                // .5초 안에 다시 클릭했다면
                if (Time.time - lastClickStartTime < .5f)
                {
                    Broadcast("Clear", PlayerInstance.main.instanceId);
                    lastClickStartTime = -100;
                }
                else
                {
                    Broadcast("Create", PlayerInstance.main.instanceId, reticlePosition, distance);
                    lastClickStartTime = Time.time;

                    float scale = Vector3.Distance(_mainCamera.transform.position, reticlePosition);
                    currMinDrawDistance = minDrawDistacne * scale;
                    lastDrawPosition = reticlePosition;
                }
            }
            else
            {
                avrg += ray.GetPoint(distance);
                Vector3 reticlePosition = avrg;

                if (VRInput.ClickButtonUp)
                {
                    isStartDraw = false;
                    Broadcast("Point", PlayerInstance.main.instanceId, reticlePosition);
                }
                else if (VRInput.ClickButton)
                {
                    if (Vector3.Distance(lastDrawPosition, reticlePosition) > currMinDrawDistance)
                    {
                        Broadcast("Point", PlayerInstance.main.instanceId, reticlePosition);
                        lastDrawPosition = reticlePosition;
                    }
                }
            }
        }
    }

    [OnCommand]
    public void Create(int instanceID, Vector3 position, float distance)
    {
        Transform container = transform.Find(instanceID.ToString());
        if(container == null)
        {
            container = new GameObject(instanceID.ToString()).transform;
            container.SetParent(transform);
        }

        var go = new GameObject("LineRenderer" + instanceID);
        go.transform.SetParent(container);

        var line = go.AddComponent<LineRenderer>();
        line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        line.receiveShadows = false;
        line.motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;
        line.useWorldSpace = true;
        line.widthMultiplier = (width * distance);
        line.sharedMaterial = material;

        line.positionCount = 1;
        line.SetPosition(0, position);
    }

    [OnCommand]
    public void Point(int instanceID, Vector3 position)
    {
        Transform container = transform.Find(instanceID.ToString());
        if(container == null)
        {
            SLog.Error("LineDrawer", "Point: "+instanceID + "의 컨테이너를 찾을 수 없습니다.");
            return;
        }

        var go = container.GetChild(container.childCount - 1);
        var line = go.GetComponent<LineRenderer>();
        if (line == null)
        {
            SLog.Error("LineDrawer", "Point: " + instanceID + "의 라인렌더러를 찾을 수 없습니다.");
            return;
        }

        int idx = line.positionCount;
        line.positionCount++;
        line.SetPosition(idx, position);
    }

    [OnCommand]
    public void Clear(int instanceID)
    {
        Transform container = transform.Find(instanceID.ToString());
        if (container == null)
        {
            SLog.Error("LineDrawer", "Clear: " + instanceID + "의 컨테이너를 찾을 수 없습니다.");
            return;
        }

        Destroy(container.gameObject);
    }

    [OnCommand]
    public void ClearAll()
    {
        // 동영상 전환될 때 화면에 있는 것들 지우면서 색맹 필터값도 원래대로 되돌림
        //_mainCamera.gameObject.GetComponent<Wilberforce.Colorblind>().Type = 0;
        //_mainCamera.gameObject.GetComponent<ColorBlindUtility.UGUI.ColorBlindFilter>().colorBlindMode = ColorBlindUtility.UGUI.ColorBlindMode.None;       

        transform.DestroyChildren();
    }
    /*
    [OnReceive(OPCODE)]
    public void OnPainting(BinaryReader reader)
    {
        reader.ReadC(); // read Opcode

        int instanceID = reader.ReadD();
        TYPE type = reader.ReadEC<TYPE>();
        Vector3 pos = reader.ReadVector3();

        switch (type)
        {
            case TYPE.Create: // 버튼 다운
                {
                    Create(instanceID, pos, reader.ReadSingle());
                }
                break;
            case TYPE.Release: // 버튼 업
                {
                    Release(instanceID, pos);
                }
                break;
            case TYPE.Move: // 이동
                {
                    Move(instanceID, pos);
                }
                break;
            case TYPE.EraseAll: // 삭제
                {
                    EraseAll();
                }
                break;
        }
    }

    public void Create(int instanceId, Vector3 position, float distance)
    {
        var trail = new GameObject("TrailTemp", typeof(TrailRenderer));
        //if (instanceId == PlayerInstance.lookMain.instanceId)
        //{
        //    trail.transform.SetParent(transform, false);
        //    trail.transform.ResetLocal();
        //}
        //else
        {
            if(container == null)
            {
                container = new GameObject("trailTempContailer").transform;
            }
            trail.transform.SetParent(container);
            trail.transform.position = position;
        }

        float scale = Vector3.Distance(_mainCamera.transform.position, position);

        var renderer = trail.GetComponent<TrailRenderer>();
        renderer.material = trailMaterial;
        renderer.minVertexDistance = minMoveDistacne * scale;
        renderer.time = lifeTime;
        renderer.startWidth = renderer.endWidth = (width * distance);
        renderer.autodestruct = autoDestruct;

        lines.Put(instanceId, trail);
    }

    public void Release(int instanceId, Vector3 position)
    {
        GameObject go;
        if (lines.TryGetValue(instanceId, out go) && go)
        {
            go.transform.position = position;
            lines.Remove(instanceId);
        }
    }

    public void Move(int instanceId, Vector3 position)
    {
        GameObject go;
        if (lines.TryGetValue(instanceId, out go) && go)
        {
            go.transform.position = position;
        }
    }

    public void EraseAll()
    {
        if (container != null)
        {
            container.gameObject.Destroy();
            lines.Clear();
            container = null;
        }
    }*/
}
