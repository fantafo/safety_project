﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// 플레이어의 이름을 보여주기 위한 컴포넌트입니다.
    /// 
    /// PlayerInfo 프리팹의 Floating menu 내에 사용되고 있습니다.
    /// </summary>
    public class DisplayPlayerName : SMonoBehaviour
    {
        public Text nameText;
        public Text scoreText;
        public Text rankingText;

        private IEnumerator Start()
        {
            string beforeName = null;
            int beforeScore = -1;
            int beforeRank = -1;
            while(true)
            {
                if (PlayerInstance.lookMain != null && PlayerInstance.lookMain.userName != beforeName)
                {
                    if (nameText != null)
                    {
                        nameText.text = beforeName = PlayerInstance.lookMain.userName;
                    }
                }
                if (PlayerInstance.lookMain != null && PlayerInstance.lookMain.score != beforeScore)
                {
                    scoreText.text = (beforeScore = PlayerInstance.lookMain.score).ToString();
                    scoreText.twnColor(Color.yellow, Color.white, 0.7f);
                    scoreText.twnScale(Vector3.one * 1.1f, Vector3.one, 0.7f);
                }
                if (PlayerInstance.lookMain != null && PlayerInstance.lookMain.ranking != beforeRank)
                {
                    rankingText.text = (beforeRank = PlayerInstance.lookMain.ranking).ToString();
                    rankingText.twnColor(Color.yellow, Color.white, 0.7f);
                    rankingText.twnScale(Vector3.one * 1.1f, Vector3.one, 0.7f);
                }
                yield return Waits.Wait(1);
            }
        }
    }
}