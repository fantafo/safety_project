﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// 360 프리팹 내에서 모델을 불러와서 보여줄 때 사용합니다.
    /// 처음에는 3D Model만을 위해서 만들었지만, 추후 그림도 사용하게됐습니다.
    /// 또한 useFade를 사용하여 페이드 처리를 할것인지도 선택가능합니다.
    /// </summary>
    public class ModelViewer : AutoCommander
    {
        public static ModelViewer current;
        public static List<ModelViewer> models = new List<ModelViewer>();

        public GameObject model;
        public bool active;
        public bool useFade = true;
        bool isAnimating;

        protected override void Awake()
        {
            base.Awake();
            models.Add(this);
            SLog.Debug("ModelView", "Regist " + name);
        }

        private void OnDestroy()
        {
            if (current == this)
                current = null;
            models.Remove(this);
            SLog.Debug("ModelView", "Unregist " + name);
        }

        public void Toggle()
        {
            if (PlayerInstance.main.IsRoomMaster && !isAnimating)
            {
                isAnimating = true;
                Broadcast("SetActive", name, !active);
            }
        }
        [OnCommand]
        public void SetActive(string name, bool active)
        {
            SLog.Debug("ModelView", "SetActive " + name + "/" + this.name + "  " + active);

            if (name != this.name)
                return;

            this.active = active;
            if (active)
            {
                KeyStack.Push(this);
            }
            else
            {
                KeyStack.Pop(this);
            }

            // 최종적으로 대상을 끄고 키는 엑션입니다.
            // Fade와 일반을 구분하기위해서 내부의 Action이 생성되며,
            // 이 action에서 이전에 선택된 대상을 끄는 루트또한 포함 돼 있습니다.
            var self = this;
            Action changeAction = () =>
            {
                if (active)
                {
                    if (current != null)
                    {
                        var before = current;
                        var befFade = before.useFade;
                        before.useFade = false;
                        before.SetActive(before.name, false);
                        before.useFade = befFade;
                    }
                    current = self;
                }
                else
                {
                    if (current == self)
                        current = null;
                }
                model.SetActive(active);
            };

            // 실제 엑션
            if (useFade)
            {
                ScreenFader.main.FadeOutIn(0.4f, changeAction, () =>
                {
                    isAnimating = false;
                });
            }
            else
            {
                changeAction();
                isAnimating = false;
            }
        }

        private void Update()
        {
            // 방장 && 빽키 && 현재 선택된 씬
            if (PlayerInstance.main.IsRoomMaster && VRInput.BackButton && KeyStack.Is(this) && active && !isAnimating)
            {
                Broadcast("SetActive", name, false);
            }
        }
    }
}