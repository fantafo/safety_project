﻿using FTF.Earth;
using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 방이 생성됐을 때, 방장이 전체 유저에게 방을 세팅해줄때 사용된다.
    /// 처음 방이 생성돼서 방에 접속하면 방장에게 InitalizeTo 메서드가 호출되고,
    /// 파라미터로 들어온 플레이어에게 초반세팅을 해주어야한다.
    /// 이것은 최초 방 생성 뿐 아니라, 중간에 난입하거나 재접속한 유저에게도 해당된다.
    /// 만약 방장이 바뀌거나 접속이 해제되면 다음 유저가 방장 대신 일을 일임하여 실행된다.
    /// 즉 모든 유저가 방장이 될 수 있다는 가정하에 데이터를 가지고 있어야한다.
    /// </summary>
    public class EarthInitializer : BaseSceneInitializer
    {
        public DemoEarthController earth;
        public UserLocationManager location;
        public ActiveForCountry info;
        public ActiveForCountry catalog;

        AutoCommandReceiverImpl impl;

        protected override void Awake()
        {
            base.Awake();

            impl = new AutoCommandReceiverImpl(this);
            impl.Enable();
        }

        public override void InitializeTo(PlayerInstance player)
        {
            impl.Send(player, "OnInitialize",
                /*Earth*/           earth.isInit, earth.selectID,
                /*userFormation*/   location.LocateIndex,
                /*resourceLoading*/ (ResourceLoader.main != null ? ResourceLoader.main.currentLoadPath : null),
                /*ModelView*/       (ModelViewer.current != null ? ModelViewer.current.name : null)
            );
        }

        [OnCommand]
        public void OnInitialize(
            bool initEarth, int selectCountry,
            int formation,
            string resourcePath,
            string modelView)
        {
            SLog.Debug("OnInitialize", "Begin");
            STween.INIT_DURATION_SCALE = 0.01f;
            if (initEarth && !earth.isInit)
            {
                earth.Show(selectCountry);
            }
            location.LocateIndex = formation;

            STween.INIT_DURATION_SCALE = 1;

            SLog.Debug("OnInitialize", "ResourcePath " + resourcePath);
            if (resourcePath.Exists())
            {
                SLog.Debug("OnInitialize", "LoadResource " + resourcePath);
                ResourceLoader.main.ShowDirect(resourcePath, () =>
                {
                    var model = ModelViewer.models.Find((a) => a.name == modelView);
                    SLog.Debug("OnInitialize", "Model Show " + modelView + " / Target: " + model);
                    if (model != null)
                    {
                        SLog.Debug("OnInitialize", "Model Show!");
                        var befFade = model.useFade;
                        model.useFade = false;
                        model.SetActive(modelView, true);
                        model.useFade = befFade;
                    }
                });
            }
        }
    }
}