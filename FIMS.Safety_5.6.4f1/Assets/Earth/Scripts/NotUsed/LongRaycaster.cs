﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// GoogleEvent시스템과 별개로 Raycasting하기 위해서 만들었습니다.
    /// 하지만 지금은 사용하지 않고 그냥 GoogleEvent 시스템의 raycast길이를 늘려서 사용하고있습니다.
    /// </summary>
    [Obsolete]
    public class LongRaycaster : SMonoBehaviour
    {
        public LayerMask includeLayerMask;
        public float raycastDistance = 1000;
        public RaycastHit[] hits = new RaycastHit[10];

        private Camera _mainCamera;

        private void Start()
        {
            _mainCamera = Camera.main;
        }

        void Update()
        {
            if (VRInput.ClickButtonDown)
            {
                var ray = _mainCamera.ScreenPointToRay(_mainCamera.WorldToScreenPoint(FTFReticlePosition.Value));
                int size = Physics.RaycastNonAlloc(ray, hits, raycastDistance, includeLayerMask);
                if (size != 0)
                {
                    for (int i = 0; i < hits.Length; i++)
                    {
                        var systemHandler = hits[i].collider.GetComponent<ILongRaycastTrigger>();
                        if (systemHandler != null)
                        {
                            systemHandler.Click();
                        }
                    }
                }
            }
        }
    }
}