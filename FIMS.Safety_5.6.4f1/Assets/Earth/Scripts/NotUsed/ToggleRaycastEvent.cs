﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    public class ToggleRaycastEvent : AutoCommander, IPointerClickHandler
    {
        public GameObject target;
        public int id = 0;

        [OnCommand]
        public void Toggle(int id)
        {
            if(this.id == id && target)
            {
                target.SetActive(!target.activeSelf);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Broadcast("Toggle", id);
        }
    }
}