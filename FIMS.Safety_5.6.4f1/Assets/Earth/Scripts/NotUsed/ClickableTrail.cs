﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// TrailRenderer를 이용해서 선생님이 레이저 포인터로 그림을 그리는 효과를 나타냅니다.
/// 하지만 현재는 사용하지 않고, FTFLineDrawer를 사용합니다.
/// </summary>
public class ClickableTrail : CommandReceiver
{
    const byte OPCODE = 1;
    enum TYPE
    {
        Create,
        Release,
        Move,
        EraseAll
    }


    public Transform pointer;
    public Material trailMaterial;
    public float lifeTime = 0;
    public float width = 0.001f;
    public float minMoveDistacne = 0.005f;
    public bool autoDestruct = true;
    public float lastClickStartTime = -100;
    public int smoothCount = 20;
    
    public Dictionary<int, GameObject> trails;
    AverageVector3 avrg;
    Transform container;

    private Camera _mainCamera;

    protected override void Awake()
    {
        base.Awake();
        trails = new Dictionary<int, GameObject>();
        avrg = new AverageVector3(smoothCount);
    }

    private void Start()
    {
        _mainCamera = Camera.main;
    }
    private void OnValidate()
    {
        avrg = new AverageVector3(smoothCount);
    }

    bool isStartDraw;
    float distance = 0;
    private void Update()
    {
        if ((VRInput.ClickButton || VRInput.ClickButtonUp || VRInput.ClickButtonDown) && ControllerDirHelper.isUp)
        {
            if (!isStartDraw)
            {
                isStartDraw = true;

                RaycastHit hit;
                Vector3 reticlePosition;
                Ray ray = _mainCamera.ScreenPointToRay(_mainCamera.WorldToScreenPoint(pointer.position));
                if (Physics.Raycast(ray, out hit, _mainCamera.farClipPlane * 0.9f))
                {
                    distance = hit.distance;
                    avrg.Set(hit.point);
                }
                else
                {
                    distance = _mainCamera.farClipPlane * 0.9f;
                    avrg.Set(ray.GetPoint(distance));
                }
                reticlePosition = avrg;

                // .5초 안에 다시 클릭했다면
                if (Time.time - lastClickStartTime < .5f)
                {
                    var cmd = Command.Take();
                    cmd.WriteC(OPCODE); //opcode
                    cmd.WriteD(PlayerInstance.main.instanceId);
                    cmd.WriteC(TYPE.EraseAll); // type
                    cmd.WriteV3(reticlePosition);
                    cmd.BroadcastAll();
                    lastClickStartTime = -100;
                }
                else
                {
                    var cmd = Command.Take();
                    cmd.WriteC(OPCODE); //opcode
                    cmd.WriteD(PlayerInstance.main.instanceId);
                    cmd.WriteC(TYPE.Create); // type
                    cmd.WriteV3(reticlePosition);
                    cmd.WriteF(distance);
                    cmd.BroadcastAll();
                    lastClickStartTime = Time.time;
                }

            }
            else
            {
                Ray ray = _mainCamera.ScreenPointToRay(_mainCamera.WorldToScreenPoint(pointer.position));
                avrg += ray.GetPoint(distance);
                Vector3 reticlePosition = avrg;

                if (VRInput.ClickButtonUp)
                {
                    isStartDraw = false;

                    var cmd = Command.Take();
                    cmd.WriteC(OPCODE); //opcode
                    cmd.WriteD(PlayerInstance.main.instanceId);
                    cmd.WriteC(TYPE.Release); // type
                    cmd.WriteV3(reticlePosition);
                    cmd.BroadcastAll();
                }
                else if (VRInput.ClickButton)
                {
                    var cmd = Command.Take();
                    cmd.WriteC(OPCODE); //opcode
                    cmd.WriteD(PlayerInstance.main.instanceId);
                    cmd.WriteC(TYPE.Move); // type
                    cmd.WriteV3(reticlePosition);
                    cmd.BroadcastAll();
                }
            }
        }
    }

    [OnReceive(OPCODE)]
    public void OnPainting(BinaryReader reader)
    {
        reader.ReadC(); // read Opcode

        int instanceID = reader.ReadD();
        TYPE type = reader.ReadEC<TYPE>();
        Vector3 pos = reader.ReadVector3();

        switch (type)
        {
            case TYPE.Create: // 버튼 다운
                {
                    Create(instanceID, pos, reader.ReadSingle());
                }
                break;
            case TYPE.Release: // 버튼 업
                {
                    Release(instanceID, pos);
                }
                break;
            case TYPE.Move: // 이동
                {
                    Move(instanceID, pos);
                }
                break;
            case TYPE.EraseAll: // 삭제
                {
                    EraseAll();
                }
                break;
        }
    }

    public void Create(int instanceId, Vector3 position, float distance)
    {
        var trail = new GameObject("TrailTemp", typeof(TrailRenderer));
        //if (instanceId == PlayerInstance.lookMain.instanceId)
        //{
        //    trail.transform.SetParent(transform, false);
        //    trail.transform.ResetLocal();
        //}
        //else
        {
            if(container == null)
            {
                container = new GameObject("trailTempContailer").transform;
            }
            trail.transform.SetParent(container);
            trail.transform.position = position;
        }

        float scale = Vector3.Distance(_mainCamera.transform.position, position);

        var renderer = trail.GetComponent<TrailRenderer>();
        renderer.material = trailMaterial;
        renderer.minVertexDistance = minMoveDistacne * scale;
        renderer.time = lifeTime;
        renderer.startWidth = renderer.endWidth = (width * distance);
        renderer.autodestruct = autoDestruct;

        trails.Put(instanceId, trail);
    }

    public void Release(int instanceId, Vector3 position)
    {
        GameObject go;
        if (trails.TryGetValue(instanceId, out go) && go)
        {
            go.transform.position = position;
            trails.Remove(instanceId);
        }
    }

    public void Move(int instanceId, Vector3 position)
    {
        GameObject go;
        if (trails.TryGetValue(instanceId, out go) && go)
        {
            go.transform.position = position;
        }
    }

    public void EraseAll()
    {
        if (container != null)
        {
            container.gameObject.Destroy();
            trails.Clear();
            container = null;
        }
    }
}
