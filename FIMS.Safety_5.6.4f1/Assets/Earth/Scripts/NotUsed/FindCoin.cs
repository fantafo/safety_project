﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// 코인맵의 이전버전 구동 코드입니다.
    /// </summary>
    [Obsolete]
    public class FindCoin : AutoCommander
    {
        public GameObject messageObject;
        public GameObject[] gameObjects;
        public GameObject[] onStartObjects;
        bool started;

        private void Update()
        {
            if(VRInput.ClickButtonDown && PlayerInstance.main.IsRoomMaster)
            {
                if(!started)
                {
                    started = true;
                    Broadcast("OnStart", UnityEngine.Random.Range(0, gameObjects.Length));
                }
            }
        }

        [OnCommand]
        void OnStart(int idx)
        {
            gameObjects[idx].SetActive(true);
            onStartObjects.SetActive(true);

            STween.alpha(messageObject, 0, 1).SetOnComplete(()=> messageObject.SetActive(false));
        }
    }
}