﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// 게임이 시작한 뒤, 진행상황을 보여주기 위한 컴포넌트입니다.
    /// 선생님 매뉴에 부착 돼 있습니다
    /// </summary>
    public class TimeUpdater : SMonoBehaviour
    {
        public Text text;
        float startupTime;
        int currTime = -1;
        
        void OnEnable()
        {
            startupTime = Time.time;
        }
        
        void Update()
        {
            var newTime = (int)(Time.time - startupTime);
            if(currTime != newTime)
            {
                currTime = newTime;

                text.text = string.Format("{0:00}:{1:00}", currTime / 60, currTime % 60);
            }
        }
    }
}