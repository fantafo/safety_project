﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

using UnityEngine.UI;

public class CoinChance : AutoCommander
{
    public CanvasGroup canvasGroup;
    public GameObject gameEndObj;
    public GameObject countdown;
    public CoinChanceItem[] items;                      // 코인들 참조할 배열
    private GameObject _container;
    public int _maxCoinCountPerOneTime;                 // 한 번에 출현할 코인 갯수
    bool started;

    private int _currentGearCount;

    public Animator _handAnimator;

    public GameObject _infoMessage;
    public float _messageShowingTime = 3.0f;
    private WaitForSeconds _delay;
    private Coroutine _co = null;
    private Coroutine _blinkCoroutine = null;

    private Coroutine _lightOnCo = null;
    private Coroutine _gameStartCo = null;
    private Coroutine _setItemAgainCoroutine = null;

    public Image[] _equipmentIcons;

    public AudioSource _fxAudioSource;
    public AudioSource _bgmAudioSource;
    public AudioClip _fx_item_01_Clip;
    public AudioClip _bgmClip;
    public AudioClip _fx_LightOn_Clip;

    public Light _directionalLight;
    private float _directionalLightOriginIntencity = 0.0f;
    public Light[] _pointLights;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        if (_container == null)
        {
            _container = GameObject.Find("Container");
        }
        
        for (int i = 0; i < items.Length; i++)
        {
            items[i].idx = i;
        }
        
        // 현재 장비 갯수
        _currentGearCount = items.Length;
        if (_currentGearCount <= 0)
        {
            _currentGearCount = 1;
        }

        // 장비 아이콘 안 보이게
        for (int i = 0; i < _equipmentIcons.Length; ++i)
        {
            _equipmentIcons[i].enabled = false;
        }

        if (null == _handAnimator)
        {
            _handAnimator = GameObject.FindGameObjectWithTag("Hand").GetComponent<Animator>();
        }


        // 메시지 유지 시간
        _delay = new WaitForSeconds(_messageShowingTime);

        // 라이트 조절용
        if (null == _directionalLight)
        {
            _directionalLight = GameObject.Find("Directional Light").GetComponent<Light>();
        }

        if (null != _directionalLight)
        {
            // 조명의 원래 밝기 저장
            _directionalLightOriginIntencity = _directionalLight.intensity;

            // 현재 조명의 밝기를 0으로
            _directionalLight.intensity = 0.0f;
        }

        if (null != _pointLights)
        {
            for (int i = 0; i < _pointLights.Length; ++i)
            {
                // 포인트 라이트들 비활성화
                _pointLights[i].enabled = false;
            }
        }

        if (null != _gameStartCo)
        {
            StopCoroutine(_gameStartCo);
        }
        _gameStartCo = StartCoroutine(this.GameStartCoroutine());
    }

    private IEnumerator GameStartCoroutine()
    {
        // 내레이션 끝날 때 까지 대기
        yield return new WaitForSeconds(10.0f);

        if (null != _lightOnCo)
        {
            StopCoroutine(_lightOnCo);
        }
        _lightOnCo = StartCoroutine(this.LightOnCoroutine());

        //BroadcastGameStart();
    }

    private IEnumerator LightOnCoroutine()
    {
        WaitForSeconds wfs = new WaitForSeconds(0.5f);

        // Directional Light 의 Intensity 조정
        yield return wfs;
        //_directionalLight.intensity = _directionalLightOriginIntencity;

        // 라이트 켜지는 효과음 재생
        //_fxAudioSource.clip = _fx_LightOn_Clip;
        //_fxAudioSource.Play();

        for (int i = 0; i < _pointLights.Length; ++i)
        {
            // 라이트 켜기
            yield return wfs;
            _pointLights[i].enabled = true;

            // 라이트 켜지는 효과음 재생
            _fxAudioSource.clip = _fx_LightOn_Clip;
            _fxAudioSource.Play();
        }

        _directionalLight.intensity = _directionalLightOriginIntencity;
        _fxAudioSource.clip = _fx_LightOn_Clip;
        _fxAudioSource.Play();

        BroadcastGameStart();
    }
    
    // 게임 시작 알림
    public void BroadcastGameStart()
    {
        if (!started)
        {
            int currUserCount = FTF.RoomInfo.main.currentUserCount;
            started = true;
            Broadcast("GameStart", currUserCount);
        }
    }
    public void BroadcastCatch(CoinChanceItem item)
    {
        item.gameObject.SetActive(false);
        if (item.Count > 0)
        {
            Networker.Send(C_Scene.UpdateScore(PlayerInstance.main.instanceId, C_Scene.UpdateType.Relative, item._ItemScore));
            Broadcast("ItemCountDown", item.idx);
        }
        Finding(item.idx, PlayerInstance.main.instanceId);
    }

    //잡는 애니메이션, 안내 메세지
    public void CatchItem(CoinChanceItem item)
    {
        // 집은 아이템의 타입에 따라 아이템 획득 애니메이션 재생
        switch (item._equipmentType)
        {
            // 아이템 집어온 뒤 들어올리는 모션 애니메이션
            case 0: {   _handAnimator.SetTrigger("GogglePickUp");   }   break;
            case 1: {   _handAnimator.SetTrigger("GlovePickUp");    }   break;
            case 2: {   _handAnimator.SetTrigger("LabCoatPickUp");  }   break;
            case 3: {   _handAnimator.SetTrigger("ShoesPickUp");    }   break;

            default:{   _handAnimator.SetTrigger("Idle");   }           break;
        }

        // 아이템 획득 안내 메시지 띄움
        if (_co != null)
        {
            StopCoroutine(_co);
        }
        _co = StartCoroutine(this.ShowInfoMessageCoroutine(item._equipmentType));

        // 획득한 아이템에 해당하는 아이콘 깜빡임
        if (_blinkCoroutine != null)
        {
            StopCoroutine(_blinkCoroutine);
        }
        _blinkCoroutine = StartCoroutine(this.BlinkEquipmentIconCoroutine(item._equipmentType));

        // 아이템 획득시의 효과음 재생
        _fxAudioSource.clip = _fx_item_01_Clip;
        _fxAudioSource.Play();
    }

    private IEnumerator ShowInfoMessageCoroutine(int equipmentType)
    {
        // 안내 메시지창이 활성화 되어 있으면
        if (_infoMessage.activeSelf)
        {
            // 안내 메시지창 비활성화
            _infoMessage.SetActive(false);
            yield return null;
        }

        const string str0 = "고글을 획득하였습니다.";
        const string str1 = "글러브를 획득하였습니다.";
        const string str2 = "실험복을 획득하였습니다.";
        const string str3 = "안전화를 획득하였습니다.";
        const string tmp = "...을 획득하였습니다.";
        
        // 안내 메시지 설정
        switch (equipmentType)
        {
            case 0: {   _infoMessage.GetComponentInChildren<Text>().text = str0;    }   break;
            case 1: {   _infoMessage.GetComponentInChildren<Text>().text = str1;    }   break;
            case 2: {   _infoMessage.GetComponentInChildren<Text>().text = str2;    }   break;
            case 3: {   _infoMessage.GetComponentInChildren<Text>().text = str3;    }   break;

            default:{   _infoMessage.GetComponentInChildren<Text>().text = tmp;     }   break;
        }
        
        // 안내 메시지 활성화
        _infoMessage.SetActive(true);

        // 일정시간 후 안내 메시지 비활성화
        yield return _delay;
        _infoMessage.SetActive(false);
    }

    private IEnumerator BlinkEquipmentIconCoroutine(int equipmentType)
    {
        WaitForSeconds delay = new WaitForSeconds(0.2f);

        // 깜빡임 3번 반복
        for (int i = 0; i < 3; ++i)
        {
            _equipmentIcons[equipmentType].enabled = true;
            yield return delay;
            _equipmentIcons[equipmentType].enabled = false;
            yield return delay;
        }

        // 아이콘 활성화
        _equipmentIcons[equipmentType].enabled = true;
    }


    private IEnumerator SpawnItem(int count)
    {
        for (int idx = 0; idx < count; ++idx)
        {
            items[idx].coin.SetActive(true);
            yield return null;
            if (items[idx].effect)
                items[idx].effect.SetActive(false);
        }
    }


    // 클라이언트들에게 게임 시작 알림, count는 유저 수
    [OnCommand]
    void GameStart(int userCount)
    {
        _bgmAudioSource.Play();

        started = true;

        _infoMessage.SetActive(false);

        // 준비 완료되면 캔버스 비활성화?
        canvasGroup.twnAlpha(0, 0.5f).SetOnComplete(() => canvasGroup.gameObject.SetActive(false));

        // 제한시간 활성화
        countdown.SetActive(true);

        Debug.Log("현재 장비 갯수: " + _currentGearCount);
        //StartCoroutine(SpawnItem(_currentGearCount));
        //return;
        for (int idx = 0; idx < _currentGearCount; ++idx)
        {
            items[idx].coin.SetActive(true);
        }
        /*
        //아이템 활성화 수 설정. 4명 초과라면 아이템 12개 생성 & count 증가, count 초기값 1
        if(userCount > 4)
        {
            int num = (userCount * 3) - items.Length;
            for (int i = 0; i < num; ++i)
            {
                items[i % items.Length].Count++;
            }
        }
        */
        // 참가자 수가 4명보다 많으면
        userCount = 10;
        if (userCount > 4)
        {
            int num = items.Length * (userCount / 2); // 12 * (참가자 수 / 2)            
            for (int i = 0; i < num; ++i)
            {
                // 아이템 0 ~ 11번 까지 1개씩 증가
                items[i % items.Length].Count += 1;
            }
        }        
    }
    public void GameEnd()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].idx = i;
            //items[i].coin.SetActive(false);
            items[i].gameObject.SetActive(false);
            if (items[i].effect != null)
                items[i].effect.SetActive(false);
        }
        if (gameEndObj != null)
        {
            gameEndObj.gameObject.SetActive(true);
        }
    }
    [OnCommand]
    void SetActive(int idx)
    {
        // 해당 idx의 코인을 활성화

        //items[idx].coin.SetActive(true);
        items[idx].gameObject.SetActive(true);
        if (items[idx].effect)
        {
            items[idx].effect.SetActive(false);
        }            
    }
    [OnCommand]
    void ItemCountDown(int id)
    {
        items[id].Count--;
        items[id].coin.SetActive(false);

        if (items[id].Count > 0)
        {
            SetItemAgain(id);
        }
    }

    // [OnCommand] 가 붙은 함수에서 Coroutine 이 호출되지 않는 문제가 있어 만든 함수
    private void SetItemAgain(int id)
    {
        if (null != _setItemAgainCoroutine)
        {
            StopCoroutine(_setItemAgainCoroutine);
        }
        _setItemAgainCoroutine = StartCoroutine(this.SetItemAgainCoroutine(id));
    }

    private IEnumerator SetItemAgainCoroutine(int id)
    {
        //아이템 재생성 딜레이
        yield return new WaitForSeconds(1.0f);
        items[id].coin.SetActive(true);
    }
    
    void Finding(int idx, int instanceId)
    {
        //items[idx].coin.SetActive(false);
        //items[idx].gameObject.SetActive(false);
        //items[idx].Count--;
        //아이템 먹기
        if (PlayerInstance.main.instanceId == instanceId || instanceId == PlayerInstance.lookMain.instanceId)
        {
            // 아이템 획득시 연출 처리
            CatchItem(items[idx]);

            // 아이템 획득시 아이템별 파티클 이펙트 재생을 해야하는 곳
            SafetyTrainingSceneManager.main.PlayScoreEffect();
        }

        if (instanceId == PlayerInstance.lookMain.instanceId)
        {
            // 실제 아이템 획득 파티클 이펙트 재생을 하는 곳
            var go = Instantiate(Resources.Load("ItemGetEffect"), items[idx].transform.position, items[idx].transform.rotation, null);

            // 1.1초 뒤에 Destroy
            STween.delayCall(1.1f, () => Destroy(go));
        }
        ////////////////////////////////////////////////////////
        --_currentGearCount;

        // 획득해야 할 장비가 남아있지 않으면
        if (0 >= _currentGearCount)
        {
            // 제한시간 이내라도 종료시킴
            if (countdown.GetComponent<FTF.Earth.Countdown>().onComplete != null)
            {
                STween.alpha(countdown.gameObject, 0, 1).SetOnComplete(() => countdown.gameObject.SetActive(false));
                gameEndObj.GetComponentInChildren<Text>().text = "안전용구를 착용하였습니다.";
                countdown.GetComponent<FTF.Earth.Countdown>().onComplete.Invoke();
            }
        }
    }
}