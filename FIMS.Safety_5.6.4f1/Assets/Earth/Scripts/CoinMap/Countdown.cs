﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    /// <summary>
    /// 코인맵에서 제한시간을 나타낼때 사용했습니다.
    /// startValue부터 0까지 1초에 1씩 줄어들며 0초가 되면 onComplete로 알려줍니다.
    /// </summary>
    public class Countdown : SMonoBehaviour
    {
        public Text text;
        public int startValue;
        public UnityEvent onComplete;

        private CustomTimer _timer;

        private IEnumerator Start()
        {
            _timer = transform.GetChild(2).GetComponent<CustomTimer>();

            if (_timer != null)
            {
                _timer.duration = startValue;
                _timer.StartTimer();
            }

            for (int value = startValue; value >= 0; value--)
            {
                text.text = value.ToString();
                yield return Waits.Wait(1);
            }
            yield return Waits.Wait(2);
            STween.alpha(gameObject, 0, 1).SetOnComplete(() => gameObject.SetActive(false));

            if (onComplete != null)
                onComplete.Invoke();
        }
    }
}