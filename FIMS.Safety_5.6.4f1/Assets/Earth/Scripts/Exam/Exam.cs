﻿using FTF;
using FTF.Packet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 쪽지시험을 내기 위한 컴포넌트입니다.
/// 
/// BroadcastShow           시험 창 열기 (선생님)
/// BraodcastHide           시험 창 닫기 (선생님)
/// BroadcastStart          시험 시작 (선생님)
/// BroadcastEnd            시험 종료 (선생님)
/// BroadcastSelectAnser    문제 답하기 (학생)
/// 
/// </summary>
public class Exam : AutoCommander
{
    public static Exam current;
    public CanvasGroup canvas;
    public Text countdownText;
    public int addScore;
    public int rightAnswer;
    public ExamAnswer[] answers;
    public int count = 10;

    bool animate;
    bool isShow;
    bool isStart;
    bool isEnd;

    public Color normalColor = Color.black;
    public Color rightAnswerColor = Color.yellow;
    public Color wrongAnswerColor = Color.red;
    public GameObject activeSuccess;
    public GameObject activeFailed;

    public Dictionary<int, int> answersData = new Dictionary<int, int>();

    protected override void Awake()
    {
        base.Awake();

        if (canvas.gameObject.activeSelf)
            canvas.gameObject.SetActive(false);

        if (current != this && current)
            Destroy(current.gameObject);

        current = this;

        for (int i = 0; i < answers.Length; i++)
        {
            int idx = i;
            var toggle = answers[i].toggle;
            toggle.onValueChanged.AddListener((v) =>
            {
                if (v)
                {
                    if (toggle.interactable)
                    {
                        BroadcastSelectAnswer(idx);
                    }
                    else
                    {
                        toggle.isOn = false;
                    }
                }
            });
        }
    }
    private void OnDestroy()
    {
        if (current == this)
            current = null;
    }

    private void Update()
    {
        if (KeyStack.Is(this) && VRInput.BackButton)
        {
            BroadcastHide();
        }
    }

    [Button]
    public void BroadcastShow()
    {
        if (!animate)
        {
            animate = true;
            Broadcast("Show");
        }
    }
    [Button]
    public void BroadcastHide()
    {
        if (!animate)
        {
            animate = true;
            Broadcast("Hide");
        }
    }
    [Button]
    public void BroadcastStart()
    {
        if (!animate && !isStart)
        {
            isStart = true;
            Broadcast("ExamStart");
        }
    }
    public void BroadcastEnd()
    {
        if (!animate && !isEnd)
        {
            isEnd = true;
            Broadcast("ExamEnd");
        }
    }
    public void BroadcastSelectAnswer(int idx)
    {
        Broadcast("SelectAnswer", PlayerInstance.main.instanceId, idx);
    }

    [OnCommand]
    private void Show()
    {
        if (isShow)
        {
            animate = false;
            return;
        }

        animate = true;
        isShow = true;
        isStart = false;
        isEnd = false;

        canvas.alpha = 0;
        canvas.gameObject.SetActive(true);
        canvas.twnAlpha(0, 1, 1f);
        countdownText.text = "";

        foreach (var t in answers)
            t.msg.color = normalColor;

        answersData.Clear();
        //answersData.Put(PlayerInstance.main.instanceId, rightAnswer);
        Refresh();

        STween.delayCall(1.1f, () =>
        {
            animate = false;
        });

        KeyStack.Push(this);
    }
    [OnCommand]
    private void Hide()
    {
        if (!isShow)
        {
            animate = false;
            return;
        }

        animate = true;

        canvas.twnAlpha(1, 0, 0.5f);
        STween.delayCall(0.6f, () =>
        {
            isShow = false;
            animate = false;
            canvas.gameObject.SetActive(false);

            var btn = GameObject.FindObjectOfType<ExamButton>();
            if (btn)
            {
                var toggle = btn.GetComponent<Toggle>();
                if (toggle)
                    toggle.isOn = false;
            }
        });
        KeyStack.Pop(this);

        if (activeSuccess)
            activeSuccess.SetActive(false);
        if (activeFailed)
            activeFailed.SetActive(false);

        state.TryStop();
    }
    [OnCommand]
    private void SelectAnswer(int instanceId, int idx)
    {
        answersData.Put(instanceId, idx);
        Refresh();
    }

    STweenState state;
    [OnCommand]
    private void ExamStart()
    {
        isStart = true;
        state = STween.valueInt(count + 1, 0, count, (v) =>
          {
              countdownText.text = Mathf.Min(count, v).ToString();
              if (v == 0)
              {
                  if (PlayerInstance.main.IsRoomMaster)
                  {
                      Broadcast("ExamEnd");
                  }
              }
          }).SetOnComplete(() => state = null);
        Refresh();
    }

    [OnCommand]
    private void ExamEnd()
    {
        isEnd = true;

        if (PlayerInstance.main.IsRoomMaster)
        {
            foreach (var pair in answersData)
            {
                if (pair.Value == rightAnswer)
                {
                    Networker.Send(C_Scene.UpdateScore(pair.Key, C_Scene.UpdateType.Relative, addScore));
                }
            }
        }

        int result;
        if (!answersData.TryGetValue(PlayerInstance.lookMain.instanceId, out result))
        {
            result = -1;
        }

        bool success = false;
        for (int i = 0; i < answers.Length; i++)
        {
            if (i == rightAnswer)
            {
                answers[i].msg.color = rightAnswerColor;

                if (i == result)
                {
                    if (activeSuccess)
                        activeSuccess.SetActive(true);
                    if (answers[i].activeSuccess)
                        answers[i].activeSuccess.SetActive(true);

                    success = true;
                    continue;
                }
            }
            else if (i == result)
            {
                answers[i].msg.color = wrongAnswerColor;
                if (answers[i].activeFailed)
                    answers[i].activeFailed.SetActive(true);
            }
        }
        if (!PlayerInstance.lookMain.IsRoomMaster && !success && activeFailed)
            activeFailed.SetActive(true);
    }

    public void Refresh()
    {
        int result;

        if (answersData.TryGetValue(PlayerInstance.lookMain.instanceId, out result))
        {
            for (int i = 0; i < answers.Length; i++)
            {
                answers[i].toggle.isOn = (i == result);
                answers[i].toggle.interactable = false;
            }
        }
        else
        {
            for (int i = 0; i < answers.Length; i++)
            {
                bool interactable = isStart;
#if !UNITY_EDITOR
                interactable &= !PlayerInstance.main.IsRoomMaster;
#endif
                answers[i].toggle.interactable = interactable;
                answers[i].toggle.isOn = false;
            }
        }

        for (int i = 0; i < answers.Length; i++)
        {
            if (answers[i].counter)
                answers[i].counter.text = CalcCount(i).ToString();

            if (answers[i].rightAnswer)
                answers[i].rightAnswer.SetActive(i == rightAnswer);
        }
    }

    public int CalcCount(int idx)
    {
        int count = 0;
        var e = answersData.GetEnumerator();
        while (e.MoveNext())
        {
            if (e.Current.Value == idx)
            {
                count++;
            }
        }
        return count;
    }
}
