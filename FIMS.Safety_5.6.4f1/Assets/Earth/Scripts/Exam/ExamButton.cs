﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 시험내기 열기 버튼에 추가하면 됩니다.
/// 
/// Exam 컴포넌트는 각 360의 프리팹에 속해있기때문입니다.
/// </summary>
public class ExamButton : MonoBehaviour
{
    private void Awake()
    {
        var btn = GetComponent<Toggle>();
        btn.onValueChanged.AddListener((v) =>
        {
            if (v)
            {
                if (Exam.current)
                {
                    Exam.current.BroadcastShow();
                }
            }
        });
    }
}
