﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafetyIndexManager : MonoBehaviour {

    public static SafetyIndexManager main = null;
    public int index = -1;

    public void Awake()
    {
        SafetyIndexManager.main = this;
        main.index = -1;
        DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
