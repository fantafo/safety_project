﻿Shader "Custom/HandWounds" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MainTex2 ("Albedo (RGB)", 2D) = "white" {}
		_ColorR("R Value", Range(0,1)) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM
		#pragma surface surf Standard noambient

		sampler2D _MainTex;
		sampler2D _MainTex2;

		float _ColorR;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
			float4 color:COLOR;
		};
		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 d = tex2D (_MainTex2, IN.uv_MainTex);
			
			o.Albedo = lerp(c.rgb, d.rgb, IN.color.r * _ColorR);
			//o.Albedo = lerp(o.Albedo, e.rgb, IN.color.g * _ColorG);
			//o.Albedo = lerp(o.Albedo, f.rgb, IN.color.b * _ColorB);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
