﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointLightBlink : MonoBehaviour
{
    public float _blinkInterval = 0.0f;
    private Light _pointLight;
    private Coroutine _co = null;

    private void Start()
    {
        _pointLight = this.gameObject.GetComponent<Light>();

        if (_co != null)
        {
            StopCoroutine(_co);
        }
        _co = StartCoroutine(this.PointLightBlinkCoroutine());
    }

    private IEnumerator PointLightBlinkCoroutine()
    {
        WaitForSeconds blinkDelay = new WaitForSeconds(_blinkInterval);

        while (true)
        {
            _pointLight.enabled = true;

            yield return blinkDelay;

            _pointLight.enabled = false;

            yield return blinkDelay;
        }
    }
}