﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFrontImageRightToLeft : MonoBehaviour
{
    private RectTransform _rectTr;
    private Vector3 _originPos = Vector3.zero;
    private Vector3 _currPos = Vector3.zero;

    private void Awake()
    {
        _rectTr = this.gameObject.GetComponent<RectTransform>();
        _originPos = _rectTr.localPosition;       // 초기 localPosition : 400.0f, 0.0f, 0.0f
    }

    private void OnEnable()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", _originPos.x,
            "to", _originPos.x * (-1.0f),
            "time", 1.0f,
            "easetype", iTween.EaseType.easeOutQuad,
            "delay", 0.5f,
            "onupdate", "MoveFromRightToLeft"));
    }

    public void MoveFromRightToLeft(float xPos)
    {
        _currPos = _rectTr.localPosition;
        _currPos.x = xPos;
        _rectTr.localPosition = _currPos;
    }
}