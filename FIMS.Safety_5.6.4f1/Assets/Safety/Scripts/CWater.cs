﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CWater : MonoBehaviour
{
    private Transform _tr;

    private Vector3 _scaleValue = Vector3.one;

    private float _currLimit = 0.0f;
    private const float _limit = 1.0f;

    public float _decreaseLate = 0.5f;
    public float _increaseLate = 2.0f;
    public float _targetAmountValue = 2.0f;

    private Vector3 _originPos;
    private Vector3 _originScale;

    private bool _isFull = false;

    private void Awake()
    {
        _tr = this.transform;
    }

    private void Start()
    {
        _originPos = _tr.position;
        _originScale = _tr.localScale;

        Init();
    }

    public void Init()
    {
        Debug.Log("Init");

        _isFull = false;

        _currLimit = 0.0f;

        _tr.localScale = _originScale;

        _tr.position = _originPos;

        //StopCoroutine(this.DecreaseLimitTimeCoroutine());
        //StartCoroutine(this.DecreaseLimitTimeCoroutine());
    }

    //private IEnumerator DecreaseLimitTimeCoroutine()
    private void Update()
    {
        if (_isFull)
        {
            Debug.Log("_isFull is true");
            return;
        }
        if (_currLimit >= _limit)
        {
            return;
        }

        if (_currLimit <= 0.0f)
        {
            _currLimit = 0.0f;
            return;
        }

        _currLimit -= (Time.deltaTime * _decreaseLate);
    }

    public float GetTargetAmountLate()
    {
        return ((_tr.localScale.y - _originScale.y) / _targetAmountValue);
    }

    public float GetCurrLimit()
    {
        return _currLimit;
    }

    private void OnCollisionEnter(Collision collision)
    {
        // 액체방울이 닿으면
        if (collision.gameObject.CompareTag("WaterDrops"))
        {
            // 액체 방울 비활성화
            collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            collision.gameObject.SetActive(false);

            _scaleValue = _tr.localScale;

            if (1.0f <= GetTargetAmountLate())
            {
                _isFull = true;
                return;
            }

            // 액체의 y 스케일을 확대함
            _scaleValue.y += 0.01f;

            _tr.localScale = _scaleValue;

            // 한 번 부을 때 늘어나는 위험 게이지 증가량
            _currLimit += (Time.deltaTime * _increaseLate);
        }
    }
}