﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTF;
using FTF.Packet;
using UnityEngine.UI;

//public class LabEscapeManager : MonoBehaviour
public class LabEscapeManager : AutoCommander
{    
    public GameObject _extinguishHand;
    public GameObject _chemicalHand;
    public GameObject _showerHandle;
    public GameObject _startMap;
    public GameObject _playMap;

    public GameObject[] _labType;

    public GameObject _successBox;
    public GameObject _failedBox;

    public GameObject _infoMsgImage;
    private Text _infoMsgText;

    private Coroutine _msgImageCo = null;

    public CustomTimer _labTimer;
    public MoveCtrl _playerMoveCtrl;

    public Light _directionalLight;
    private bool _isIncrease = false;

    public GameObject _chemPlasmaExplosionEffect;
    public GameObject _brokenScreenEffect;
    public GameObject _damageScreenEffect;
    public GameObject _chemicalWaterfallEffect;
    private GameObject _brokenEffectObj = null;
    private GameObject _bloodEffectObj = null;

    public GameObject _plasmaExplosionEffect;
    public GameObject _flameParticleEffect;
    public GameObject _bigExplosionEffect;
    public GameObject _lastFlameParticleEffect;
    public GameObject _electricityLabEndingBigExplosionEffect;
    public GameObject _exclamationImage;
    public GameObject _healingEffectObj;

    public SafetyTrainingSceneManager _safetyTrainingSceneManager;

    public FTF.FTFPlayerController _ftfPlayerController;

    private Coroutine _startFireCo = null;
    private Coroutine _redLightCo = null;
    private Coroutine _switchMapCo = null;

    private Coroutine _startBioCo = null;
    private Coroutine _startChemCo = null;


    private Coroutine _showClearMsgBoxCo = null;
    private bool _isCleared = false;
    public bool _isFinished = false;

    public int _curContentIndex = -1;

    public GameObject[] _arrows;
   
    protected override void OnEnable()
    {
        base.OnEnable();
        // 손 애니메이션 초기화
        _playerMoveCtrl._extinguishHandCtrl._handAnimator.SetTrigger("Idle");

        for (int i = 0; i < _playerMoveCtrl._extinguishHandCtrl._grabbedItem.Length; ++i)
        {
            _playerMoveCtrl._extinguishHandCtrl._grabbedItem[i].SetActive(false);
        }

        // 소화기에서 나오는 물줄기 비활성화
        _playerMoveCtrl._extinguishHandCtrl._waterfall.SetActive(false);

        // 1번 화살표 비활성화
        if (null != _arrows[0])
        {
            _arrows[0].SetActive(false);
        }
    }

    private void Start ()
    {
        //_mainCam.enabled = false;
        //_startCam.enabled = true;

#if FTF_OBSERVER
        _extinguishHand = SafetyTrainingSceneManager.main._labExtinguishHand;

        _playerMoveCtrl = GameObject.Find("LabEscape").GetComponent<MoveCtrl>();
        ObserverMain.main.GetComponent<MoveCtrl>()._points = _playerMoveCtrl._points;
        ObserverMain.main.GetComponent<MoveCtrl>()._lines = _playerMoveCtrl._lines;
        ObserverMain.main.GetComponent<MoveCtrl>()._triggerPoints = _playerMoveCtrl._triggerPoints;
        ObserverMain.main.GetComponent<MoveCtrl>()._triggerEffects = _playerMoveCtrl._triggerEffects;
        ObserverMain.main.GetComponent<MoveCtrl>()._fxSounds = _playerMoveCtrl._fxSounds;
        ObserverMain.main.GetComponent<MoveCtrl>()._bgmSound = _playerMoveCtrl._bgmSound;
        ObserverMain.main.GetComponent<MoveCtrl>()._endingBigExplosionEffect = _playerMoveCtrl._endingBigExplosionEffect;
        ObserverMain.main.GetComponent<MoveCtrl>()._extinguishHandCtrl = SafetyTrainingSceneManager.main._labExtinguishHand.GetComponent<ExtinguishHandController>();
        ObserverMain.main.GetComponent<MoveCtrl>()._labEscapeManager = _playerMoveCtrl._labEscapeManager;
        _playerMoveCtrl = ObserverMain.main.GetComponent<MoveCtrl>();

        _ftfPlayerController = GameObject.Find("NewObserver(Clone)").GetComponent<ObserverMain>();
#endif
        for (int i = 0; i < _labType.Length; ++i)
        {
            _labType[i].SetActive(false);
        }

        int labIndex = 0;

        if (-1 ==_curContentIndex)
        {
            labIndex = (SafetyTrainingSceneManager.main.GetContentIndex() - 1);
        }
        else
        {
            labIndex = _curContentIndex;
        }
//        Debug.Log("labIndex: " + labIndex);
        _labType[labIndex].SetActive(true);

        // 손(생명)
        // 없음
        // 바늘 찔릴 때 나타날 느낌표
        _exclamationImage.SetActive(false);

        // 손(화학)
        if (null != _chemicalHand && (1==labIndex))
        {
            _chemicalHand.SetActive(false);
            _showerHandle = _labType[labIndex].transform.GetChild(1).Find("NGon001_hand").gameObject;
        }        

        // 손(전기화재)
        _extinguishHand.SetActive(false);

        // 연출용 맵
        _startMap = _labType[labIndex].transform.FindChildTag("START_MAP").gameObject;
        _startMap.SetActive(true);

        // 체험용 맵
        _playMap = _labType[labIndex].transform.FindChildTag("PLAY_MAP").gameObject;
        _playMap.SetActive(false);

        _infoMsgText = _infoMsgImage.GetComponentInChildren<Text>();
        _infoMsgImage.SetActive(false);

        // 타이머 안 보이게
        _labTimer.gameObject.SetActive(false);

        // 플레이어 이동 스크립트 컴포넌트
        _playerMoveCtrl.enabled = false;

        _directionalLight.color = Color.white;

        _plasmaExplosionEffect.SetActive(false);
        _flameParticleEffect.SetActive(false);
        _bigExplosionEffect.SetActive(false);
        _electricityLabEndingBigExplosionEffect.SetActive(false);

        //_infoMsgImage.transform.parent.parent = Camera.main.transform;
        //Vector3 pos = Vector3.zero;
        //pos.z = 0.02f;
        ////_infoMsgImage.transform.parent.localPosition = pos;
        //_infoMsgImage.transform.parent.GetComponent<RectTransform>().localPosition = pos;

        switch (_labType[labIndex].name)
        {
            // 생명
            case "Bio":
                {
                    // 바늘에 손 찔리는 연출용 코루틴
                    if (null != _startBioCo)
                    {
                        StopCoroutine(_startBioCo);
                    }
                    _startBioCo = StartCoroutine(this.StartBioCoroutine());
                }
                break;
            // 화학
            case "Chemical":
                {
                    // 얼굴에 약품 튀는 연출용 코루틴
                    if (null != _startChemCo)
                    {
                        StopCoroutine(_startChemCo);
                    }
                    _startChemCo = StartCoroutine(this.StartChemCoroutine());
                }
                break;
            // 전기
            case "Electricity":
                {
                    // 불 나는 연출용 코루틴
                    if (null != _startFireCo)
                    {
                        StopCoroutine(_startFireCo);
                    }
                    _startFireCo = StartCoroutine(this.StartFireCoroutine());
                }
                break;
        }
    }

    // 생명 연구실 시작 연출용 코루틴
    private IEnumerator StartBioCoroutine()
    {
        yield return new WaitForSeconds(2.0f);

        // 바늘에 찔리는 효과음
        _playerMoveCtrl._fxSounds[4].Play();

        // 화면에 느낌표 이미지 출력후 끔
        _exclamationImage.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        _exclamationImage.SetActive(false);

        yield return new WaitForSeconds(1.0f);        
        // BGM 재생
        _playerMoveCtrl._bgmSound.Play();

        yield return new WaitForSeconds(3.0f);
        StartCoroutine(this.SwitchToBioPlayCoroutine());
    }

    private IEnumerator SwitchToBioPlayCoroutine()
    {
        yield return null;
        _startMap.SetActive(false);
        // 연출 맵 켜기
        _playMap.SetActive(true);

        // 타이머 보이게
        _labTimer.GetComponent<CustomTimer>().duration = 25.0f;
        _labTimer.gameObject.SetActive(true);
        
        // 상황설명 메시지 출력
        ShowInfoMessageBox("사용한 주사기에 찔렸습니다.\n응급처치를 해야 합니다.", 3.0f);
        _extinguishHand.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        ShowInfoMessageBox("화살표를 따라가 주세요.", 3.0f);
        yield return new WaitForSeconds(3.0f);
        _extinguishHand.GetComponent<Animator>().SetTrigger("Walk");

#if FTF_OBSERVER
#else
        _ftfPlayerController.enabled = false;
#endif
        _playerMoveCtrl.enabled = true;
        _labTimer.StartTimer();
        _playerMoveCtrl._fxSounds[3].Play();    // 숨소리
    }

    // 화학 연구실 시작 연출용 코루틴
    private IEnumerator StartChemCoroutine()
    {
        GameObject camObj = Camera.main.gameObject;
        yield return new WaitForSeconds(2.0f);
        //1.실험대 위에서 파티클 폭파
        _chemPlasmaExplosionEffect.SetActive(true);
        _playerMoveCtrl._fxSounds[1].Play();
        yield return new WaitForSeconds(0.5f);
        //2.화면 깨짐, 피 흐르는 붉어짐 효과 카메라에 적용
        _brokenEffectObj = Instantiate(_brokenScreenEffect, camObj.transform.position, camObj.transform.rotation, camObj.transform);

        // BGM 재생
        _playerMoveCtrl._bgmSound.Play();
        yield return new WaitForSeconds(2.0f);
        _bloodEffectObj = Instantiate(_damageScreenEffect, camObj.transform.position, camObj.transform.rotation, camObj.transform);
        yield return new WaitForSeconds(4.0f);
        DestroyImmediate(_brokenEffectObj);
        DestroyImmediate(_bloodEffectObj);
        StartCoroutine(this.SwitchToChemPlayCoroutine());
    }
        
    private IEnumerator SwitchToChemPlayCoroutine()
    {
        yield return null;
        _startMap.SetActive(false);
        // 연출 맵 켜기
        _playMap.SetActive(true);

        // 타이머 보이게
        _labTimer.GetComponent<CustomTimer>().duration = 30.0f;
        _labTimer.gameObject.SetActive(true);

        ShowInfoMessageBox("화학약품이 튀었습니다.\n세면대를 찾아야 합니다.", 3.0f);
        _extinguishHand.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        ShowInfoMessageBox("화살표를 따라가 주세요.", 3.0f);
        yield return new WaitForSeconds(3.0f);
        _extinguishHand.GetComponent<Animator>().SetTrigger("Walk");
#if FTF_OBSERVER
#else
        _ftfPlayerController.enabled = false;
#endif
        _playerMoveCtrl.enabled = true;
        _labTimer.StartTimer();
        _playerMoveCtrl._fxSounds[3].Play();    // 숨소리
    }

    // 전기 연구실 시작 연출용 코루틴
    private IEnumerator StartFireCoroutine()
    {
        yield return new WaitForSeconds(2.0f);
        _plasmaExplosionEffect.SetActive(true);
        _playerMoveCtrl._fxSounds[0].Play();
        yield return new WaitForSeconds(2.0f);
        _plasmaExplosionEffect.SetActive(false);
        _playerMoveCtrl._fxSounds[0].Stop();
        yield return null;
        _plasmaExplosionEffect.SetActive(true);
        _playerMoveCtrl._fxSounds[0].Play();

        yield return new WaitForSeconds(2.0f);
        // 화염 폭발
        _bigExplosionEffect.SetActive(true);
        _playerMoveCtrl._fxSounds[1].Play();

        yield return null;
        _flameParticleEffect.SetActive(true);
        _playerMoveCtrl._fxSounds[2].loop = true;
        _playerMoveCtrl._fxSounds[2].Play();
        

        // BGM 재생
        _playerMoveCtrl._bgmSound.Play();
        yield return new WaitForSeconds(2.0f);
        if (null != _redLightCo)
        {
            StopCoroutine(_redLightCo);
        }
        _redLightCo = StartCoroutine(this.RedLightCoroutine());

        yield return new WaitForSeconds(2.0f);

        if (null != _switchMapCo)
        {
            StopCoroutine(_switchMapCo);
        }
        _switchMapCo = StartCoroutine(this.SwitchMapCoroutine());
    }

    private IEnumerator SwitchMapCoroutine()
    {   
        yield return new WaitForSeconds(5.0f);

        for (int i = 0; i < _playerMoveCtrl._fxSounds.Length; ++i)
        {
            _playerMoveCtrl._fxSounds[i].Stop();
        }

        _startMap.SetActive(false);
        // 연출 맵 켜기
        _playMap.SetActive(true);

        // 타이머 보이게
        _labTimer.GetComponent<CustomTimer>().duration = 30.0f;
        _labTimer.gameObject.SetActive(true);

        ShowInfoMessageBox("화재가 발생했습니다.\n소화기를 찾아야 합니다.", 3.0f);
        _extinguishHand.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        ShowInfoMessageBox("화살표를 따라가 주세요.", 3.0f);
        yield return new WaitForSeconds(3.0f);
        _extinguishHand.GetComponent<Animator>().SetTrigger("Walk");
#if FTF_OBSERVER
#else
        _ftfPlayerController.enabled = false;
#endif
        _playerMoveCtrl.enabled = true;
        _labTimer.StartTimer();
        _playerMoveCtrl._fxSounds[3].Play();    // 숨소리
    }

    

    public void ShowInfoMessageBox(string txt, float duration)
    {
        if (null != _msgImageCo)
        {
            StopCoroutine(_msgImageCo);
        }
        _msgImageCo = StartCoroutine(this.ShowInfoMessageBoxCoroutine(txt, duration));
    }

    private IEnumerator ShowInfoMessageBoxCoroutine(string txt, float duration)
    {
        _infoMsgImage.SetActive(false);
        yield return null;
        _infoMsgText.text = txt;
        _infoMsgImage.SetActive(true);
        yield return new WaitForSeconds(duration);
        _infoMsgImage.SetActive(false);
    }

    public void TimerEnd()
    {
        _infoMsgImage.SetActive(false);

        if (!_isCleared)
        {
            ShowFailedMsgBox();
        }
        else
        {
            ShowSuccessMsgBox();
        }

        StartCoroutine(this.SetLabEscapeFinishCoroutine());
    }

    public void BioLabSuccess()
    {
        // 회복 파티클 이펙트 재생
        GameObject obj = Instantiate(_healingEffectObj);
        obj.transform.position = Camera.main.transform.position;
        obj.transform.parent = this.gameObject.transform;

        // 성공 효과음 재생
        _playerMoveCtrl._fxSounds[5].Play();

        ShowClearMsgBox("안전보건 관리자에게 보고.");
    }

    public void ChemicalLabSuccess()
    {
        // 맵에 있는 손잡이 모델 비활성화
        _showerHandle.SetActive(false);

        // 세면대 손잡이 당기는 애니메이션 객체 Active
        _chemicalHand.SetActive(true);

        // 기본 손 비활성화
        _extinguishHand.SetActive(false);

        // Waterfall 이펙트 재생
        _chemicalWaterfallEffect.SetActive(true);

        // 회복 파티클 이펙트 재생
        GameObject obj = Instantiate(_healingEffectObj);
        obj.transform.position = Camera.main.transform.position;
        obj.transform.parent = this.gameObject.transform;

        // 성공 효과음 재생
        _playerMoveCtrl._fxSounds[5].Play();

        ShowClearMsgBox("약품을 무사히 씻어냈습니다.");
    }

    public void ElectricityLabSuccess()
    {
        _electricityLabEndingBigExplosionEffect.SetActive(true);
        //_labEscapeManager.StopTimer();

        // 성공 효과음 재생
        _playerMoveCtrl._fxSounds[5].Play();

        ShowClearMsgBox("화재를 무사히 진압했습니다.");
        _extinguishHand.GetComponent<ExtinguishHandController>()._waterfall.SetActive(true);
        _extinguishHand.GetComponent<Animator>().SetTrigger("UseExtinguisher");        
    }

    public void ShowClearMsgBox(string txt)
    {
        _isCleared = true;
        if (null != _showClearMsgBoxCo)
        {
            StopCoroutine(_showClearMsgBoxCo);
        }
        _showClearMsgBoxCo = StartCoroutine(this.ShowClearMsgBoxCoroutine(txt));
    }
    
    private IEnumerator ShowClearMsgBoxCoroutine(string txt)
    {
        yield return new WaitForSeconds(5.0f);
        ShowInfoMessageBox(txt, 3.0f);

        // 불 꺼짐
        //_lastFlameParticleEffect.SetActive(false);

        yield return new WaitForSeconds(3.5f);
        _infoMsgText.text = "다른 참가자를 기다리고 있습니다.";
        _infoMsgImage.SetActive(true);
    }
    
    private void ShowFailedMsgBox()
    {
        if (SafetyTrainingSceneManager.main.GetContentIndex() == 1)
        {
            ShowInfoMessageBox("안전보건 관리자에게 보고", 3.0f);
        }
        else
        {
            ShowInfoMessageBox("제한시간이 초과되었습니다.", 3.0f);
        }
        
        _extinguishHand.GetComponent<Animator>().SetTrigger("HandInjury");
        // 실패 박스 보이기
        _failedBox.SetActive(true);

        Networker.Send(C_Scene.UpdateScore(PlayerInstance.main.instanceId, C_Scene.UpdateType.Relative, 10));

        SafetyTrainingSceneManager.main.PlayScoreEffect();
    }

    private void ShowSuccessMsgBox()
    {
        if (null != _showClearMsgBoxCo)
        {
            StopCoroutine(_showClearMsgBoxCo);
        }
        _infoMsgImage.SetActive(false);

        _infoMsgImage.transform.parent.parent = transform;

        _successBox.SetActive(true);
        // 성공 박스 보이기
        Networker.Send(C_Scene.UpdateScore(PlayerInstance.main.instanceId, C_Scene.UpdateType.Relative, 50));

        SafetyTrainingSceneManager.main.PlayScoreEffect();
    }

    private IEnumerator SetLabEscapeFinishCoroutine()
    {
        yield return new WaitForSeconds(3.0f);
        _safetyTrainingSceneManager.BroadcastLabEscapeEnd();
    }

    // 빨간 불 깜빡이는 코루틴
    private IEnumerator RedLightCoroutine()
    {
        // Directional Light의 컬러를 R값이 255 ~130 사이에서 왔다갔다 하도록 하는 코루틴
        // R : 255.0f ~ 130.0f
        // G : 54.0f
        // B : 54.0f
         Color lightColor = _directionalLight.color;
        lightColor.b = lightColor.g = 0.2117647058823529f;  // 54.0f
        _directionalLight.intensity = 0.52f;
        _directionalLight.color = lightColor;
        while (true)
        {
            lightColor = _directionalLight.color;

            if (lightColor.r >= 1.0f)
            {
                _isIncrease = false;
            }
            else if (lightColor.r <= 0.5098039215686275f)   // 130.0f
            {
                _isIncrease = true;
            }

            if (_isIncrease)
            {
                lightColor.r += Time.deltaTime;
            }
            else
            {
                lightColor.r -= Time.deltaTime;
            }

            _directionalLight.color = lightColor;
            yield return null;
        }
    }
    /*
    private void OnDisable()
    {
        _extinguishHand.SetActive(false);
        _playerMoveCtrl._fxSounds[3].Stop();
        _playerMoveCtrl._bgmSound.Stop();
        _playerMoveCtrl.enabled = false;
        
    }
    */

    private void OnDestroy()
    {
        _extinguishHand.SetActive(false);

        if (null != _chemicalHand)
        {
            _chemicalHand.SetActive(false);
        }
        
        _playerMoveCtrl._fxSounds[3].Stop();
        _playerMoveCtrl._bgmSound.Stop();
        _playerMoveCtrl.enabled = false;
        _ftfPlayerController.enabled = true;

        if (null != _brokenEffectObj)
        {
            Destroy(_brokenEffectObj);
        }

        if (null != _bloodEffectObj)
        {
            Destroy(_bloodEffectObj);
        }
    }
}