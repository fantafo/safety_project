﻿using System.Collections;
using System.Collections.Generic;
using FTF;
using UnityEngine;

public class MoveCtrl : AutoCommander
{
    public Transform _tr;
    public Rigidbody _rigidbody;
    public Transform _mainCameraTr;

    private float _originSpeed = 3.0f;
    private float _currentSpeed = 0.0f;

    private Vector3 _moveDirection = Vector3.zero;
    private Vector3 _dir = Vector3.zero;

    public Transform[] _points;
    public int _targetIdx = 0;
    public GameObject[] _lines;

    private Quaternion _rot = Quaternion.identity;

    private Vector3 _v1 = Vector3.zero;
    private Vector3 _v2 = Vector3.zero;
    private Vector3 _v3 = Vector3.zero;

    public CapsuleCollider _cc;

    //public GameObject _PickupAnimModel;
    //public GameObject _WalkAnimModel;
    //public GameObject _FireReadyAnimModel;
    //public GameObject _ClearAnimModel;

    //private Animator _currAnimator;

    public LabEscapeManager _labEscapeManager;

    private Coroutine _animCo = null;
    private Coroutine _updatePosCo = null;

    public GameObject[] _triggerPoints;
    public GameObject[] _triggerEffects;
    public GameObject _endingBigExplosionEffect;

    public ExtinguishHandController _extinguishHandCtrl;
    public AudioSource[] _fxSounds;
    public AudioSource _bgmSound;

    private void Start()
    {
        _tr = transform;
        _rigidbody = GetComponent<Rigidbody>();
        _mainCameraTr = Camera.main.transform;
        _cc = GetComponent<CapsuleCollider>();
        _cc.center = new Vector3(-1.25f, 1.2f, 0.0f);
        _cc.enabled = false;
        _currentSpeed = _originSpeed;


        StartCoroutine(this.CapsuleColliderEnableCoroutine());

        if (PlayerInstance.main.IsRoomMaster)
        {
            if (null != _updatePosCo)
            {
                StopCoroutine(_updatePosCo);
            }
            _updatePosCo = StartCoroutine(this.UpdatePosCoroutine());
        }        
    }

    //private void OnEnable()
    protected override void OnEnable()
    {
        base.OnEnable();
    
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Game");

        // 현재 활성화되어 있는 탈출 컨텐츠 찾기
        for (int i = 0; i < objs.Length; ++i)
        {
            if (objs[i].name.Contains("LabEscape"))
            {
                _labEscapeManager = objs[i].GetComponent<LabEscapeManager>();
                break;
            }
        }

        _points = GameObject.Find("WayPointGroup").GetComponentsInChildren<Transform>();

        for (int i = 1; i < _points.Length; ++i)
        {
            _points[i].gameObject.SetActive(false);
        }
        _points[1].gameObject.SetActive(true);

        // 목표지점의 인덱스
        _targetIdx = 0;

        // 손 애니메이션 초기화
        _extinguishHandCtrl._handAnimator.SetTrigger("Idle");

        for (int i = 0; i < _extinguishHandCtrl._grabbedItem.Length; ++i)
        {
            _extinguishHandCtrl._grabbedItem[i].SetActive(false);
        }

        // 소화기에서 나오는 물줄기 비활성화
        _extinguishHandCtrl._waterfall.SetActive(false);

        // 현재 탈출 컨텐츠가 전기이면
        if (_labEscapeManager._curContentIndex == 2 ||
            SafetyTrainingSceneManager.main.GetContentIndex() == 3)
        {
            /*
            _lines[0] = GameObject.Find("line001").gameObject;
            _lines[1] = GameObject.Find("line002").gameObject;
            for (int i = 0; i < _lines.Length; ++i)
            {
                _lines[i].SetActive(false);
            }
            _lines[1].SetActive(true);
            */

            for (int i = 0; i < _triggerEffects.Length; ++i)
            {
                if (null != _triggerEffects[i])
                {
                    _triggerEffects[i].SetActive(false);
                }
            }

            _triggerPoints = GameObject.FindGameObjectsWithTag("EFFECT_TRIGGER");
            for (int i = 0; i < _triggerPoints.Length; ++i)
            {
                _triggerPoints[i].SetActive(false);
            }
            _triggerPoints[0].SetActive(true);
            _triggerPoints[1].SetActive(true);
            _triggerPoints[2].SetActive(true);
        }
    }

    private IEnumerator CapsuleColliderEnableCoroutine()
    {
        yield return null;
        _cc.enabled = true;
    }

    private void Update()
    {
        //test.transform.position += new Vector3(0, 0, -0.1f);
        //_mainCameraTr.position += new Vector3(0, 0, -0.1f);
#if FTF_OBSERVER
#else
        if (_rigidbody != null)
        {
            if (_points.Length <= _targetIdx)
            {
                _rigidbody.velocity = Vector3.zero;
                return;
            }
            _v1 = _points[_targetIdx].position;
            _v2 = _mainCameraTr.position;
            _v3 = _mainCameraTr.forward;

            _v1.y = 0.0f;
            _v2.y = 0.0f;
            _v3.y = 0.0f;

            _dir = _v1 - _v2;
            float angle = Vector3.Angle(_v3, _dir.normalized);
            Vector3 cross = Vector3.Cross(_v3, _dir.normalized);

            //_dir = _points[_targetIdx].position - _mainCameraTr.position;
            //float angle = Vector3.Angle(_mainCameraTr.forward, _dir.normalized);
            //Vector3 cross = Vector3.Cross(_mainCameraTr.forward, _dir.normalized);
            /*
            if (90.0f < angle)
            {
                _currentSpeed = 0.0f;
            }
            else if (90.0f >= angle && 20.0f < angle)
            {

                //_currentSpeed = (_originSpeed * Mathf.Abs((90.0f / angle) * 0.1f));
                _currentSpeed = (_originSpeed * (Mathf.Abs((90.0f / angle) * 0.1f)));
            }
            else
            {
                _currentSpeed = _originSpeed;
            }
            Debug.Log("angle => " + angle);
            */
            _moveDirection = _mainCameraTr.forward * _currentSpeed;
            _rigidbody.velocity = _moveDirection;

            // 서버 통신 부하를 줄이기 위해 코루틴으로 뺌
            //if (PlayerInstance.main.IsRoomMaster)
            //{
            //    Broadcast("UpdatePos", PlayerInstance.main.instanceId, _mainCameraTr.position, _targetIdx);
            //}
        }
#endif
    }

    private IEnumerator UpdatePosCoroutine()
    {
        WaitForSeconds delay = new WaitForSeconds(0.1f);

        while (true)
        {
            Broadcast("UpdatePos", PlayerInstance.main.instanceId, _mainCameraTr.position, _targetIdx);

            yield return delay;
        }        
    }

    [OnCommand]
    public void UpdatePos(int id, Vector3 aa, int index)
    {
#if FTF_OBSERVER
        if (PlayerInstance.lookMain.instanceId == id)
        {
            _mainCameraTr.position = aa;
        }

#endif
    }


    [OnCommand]
    public void UpdateAction(int lab, int sate)
    {
#if FTF_OBSERVER
        switch (lab)
        {
            case 2:
                {
                    switch (sate)
                    {
                        case 1:
                            {
                                _lines[0].SetActive(false);
                                _lines[1].SetActive(true);

                                _triggerPoints[3].SetActive(true);
                                _triggerPoints[4].SetActive(true);

                                _labEscapeManager.ShowInfoMessageBox("소화기 발견.\n화재지점으로 가세요.", 3.0f);
                                _extinguishHandCtrl._handAnimator.SetTrigger("GrabExtinguisher");
                                break;
                            }
                        case 2:
                            {
                                _labEscapeManager.ElectricityLabSuccess();
                                break;
                            }
                        case 11:
                            {
                                _triggerEffects[0].SetActive(true);
                                _fxSounds[1].Play();
                                break;
                            }
                        case 12:
                            {
                                _extinguishHandCtrl._handAnimator.SetTrigger("LeftHandFright");
                                _triggerEffects[1].SetActive(true);
                                _fxSounds[1].Play();
                                break;
                            }
                        case 13:
                            {
                                _extinguishHandCtrl._handAnimator.SetTrigger("RightHandFright");
                                _triggerEffects[2].SetActive(true);
                                _fxSounds[1].Play();
                                break;
                            }
                        case 14:
                            {
                                _triggerEffects[3].SetActive(true);
                                _fxSounds[1].Play();
                                break;
                            }
                        case 15:
                            {
                                _triggerEffects[4].SetActive(true);
                                break;
                            }
                        case 16:
                            {
                                GameObject.Find("TriggerPoint_A(0)").gameObject.transform.GetChild(0).gameObject.SetActive(true);
                                _fxSounds[1].Play();
                                break;
                            }
                    }
                    break;
                }
        }
#endif
    }


    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("OnCollisionEnter");
        if (collision.gameObject.CompareTag("WAY_POINT"))
        {
            //Destroy(collision.gameObject);
            collision.gameObject.SetActive(false);

            if (_points.Length <= _targetIdx)
            {
                return;
            }

            ++_targetIdx;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("====OnTriggerEnter : " + other.name);
        if (other.CompareTag("WAY_POINT"))
        {
            //Destroy(other.gameObject);
            other.gameObject.SetActive(false);
            // 생명
            //if (SafetyTrainingSceneManager.main.GetContentIndex() == 1)
            if (SafetyTrainingSceneManager.main.GetContentIndex() == 1 ||
                _labEscapeManager._curContentIndex == 0)
            {
                if (other.gameObject.name.Contains("EndPoint"))
                {
                    // 응급처치상자 발견
                    _extinguishHandCtrl._handAnimator.SetTrigger("RightHandFright");                    

                    _labEscapeManager.BioLabSuccess();
                }
            }
            // 화학
            //else if (SafetyTrainingSceneManager.main.GetContentIndex() == 2)
            else if (SafetyTrainingSceneManager.main.GetContentIndex() == 2 ||
                _labEscapeManager._curContentIndex == 1)
            {
                if (other.gameObject.name.Contains("EndPoint"))
                {
                    // 세면대 발견
                    if (null != _labEscapeManager._chemicalHand)
                    {
                        _labEscapeManager.ChemicalLabSuccess();
                    }
                }
            }
            // 전기
            //else if (SafetyTrainingSceneManager.main.GetContentIndex() == 3)
            else if (SafetyTrainingSceneManager.main.GetContentIndex() == 3 ||
                _labEscapeManager._curContentIndex == 2)
            {
                if (other.gameObject.name.Contains("Extinguisher"))
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 1);
                    //_lines[0].SetActive(false);
                    //_lines[1].SetActive(true);

                    _triggerPoints[3].SetActive(true);
                    _triggerPoints[4].SetActive(true);

                    _labEscapeManager.ShowInfoMessageBox("소화기 발견.\n화재지점으로 가세요.", 3.0f);
                    _extinguishHandCtrl._handAnimator.SetTrigger("GrabExtinguisher");
                    _fxSounds[5].Play();
                }
                else if (other.gameObject.name.Contains("EndPoint"))
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 2);
                    _labEscapeManager.ElectricityLabSuccess();
                }
            }


            ++_targetIdx;

            if (_points.Length <= _targetIdx)
            {
                _rigidbody.velocity = Vector3.zero;
                return;
            }

            _points[_targetIdx].gameObject.SetActive(true);
            if (_targetIdx == _points.Length - 1)
            {
                _points[_targetIdx].GetComponent<SphereCollider>().radius = 3.0f;
            }


        }
        else if (other.CompareTag("EFFECT_TRIGGER"))
        {
            other.GetComponent<BoxCollider>().enabled = false;
            // 전기화재 체험
            if (SafetyTrainingSceneManager.main.GetContentIndex() == 3)
            {
                if (other.name == "TriggerPoint_A")
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 11);
                    //_triggerEffects[0].SetActive(true);
                    //_fxSounds[1].Play();
                }
                else if (other.name == "TriggerPoint_B")
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 12);
                    //_extinguishHandCtrl._handAnimator.SetTrigger("LeftHandFright");
                    //_triggerEffects[1].SetActive(true);
                    //_fxSounds[1].Play();
                }
                else if (other.name == "TriggerPoint_C")
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 13);
                    //_extinguishHandCtrl._handAnimator.SetTrigger("RightHandFright");
                    //_triggerEffects[2].SetActive(true);
                    //_fxSounds[1].Play();
                }
                else if (other.name == "TriggerPoint_C (1)")
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 14);
                    _triggerEffects[3].SetActive(true);
                    _fxSounds[1].Play();
                }
                else if (other.name == "TriggerPoint_C (2)")
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 15);
                    _triggerEffects[4].SetActive(true);
                }
                else if (other.name == "TriggerPoint_A (0)")
                {
                    Broadcast("UpdateAction", SafetyTrainingSceneManager.main.GetContentIndex(), 16);
                    //other.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    //_fxSounds[1].Play();
                }
            }
        }
    }
}
