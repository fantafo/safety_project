﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotLightIntensityPingpong : MonoBehaviour
{
    private bool _isIncrease = false;
    private Light _spotLight;

    private void Start()
    {
        _spotLight = GetComponent<Light>();
        _spotLight.intensity = 10.0f;
    }

    private void Update()
    {
        if (10.0f < _spotLight.intensity)
        {
            _isIncrease = false;
        }
        else if (0.0f >= _spotLight.intensity)
        {
            _isIncrease = true;
        }

        if (_isIncrease)
        {
            _spotLight.intensity += (Time.deltaTime * 10.0f);
        }
        else
        {
            _spotLight.intensity -= (Time.deltaTime * 10.0f);
        }
    }
}