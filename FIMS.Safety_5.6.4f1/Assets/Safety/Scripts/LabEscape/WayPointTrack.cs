﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointTrack : MonoBehaviour
{
    public Color lineColor = Color.yellow;
    private Transform[] points;

    private void OnDrawGizmos()
    {
        // 라인의 색상 지정
        Gizmos.color = lineColor;
        // WayPointGroup 게임오브젝트 아래에 있는 모든 Point 게임오브젝트 추출
        points = GetComponentsInChildren<Transform>();

        int nextIdx = 1;

        // points[0] 에는 WayPointGroup의 Transform이 들어가고 첫번째 Waypoint 의 Transform 은 points[1] 에 들어감.
        if (nextIdx >= points.Length)
        {
            return;
        }
        Vector3 currPos = points[nextIdx].position;
        Vector3 nextPos;

        // Point 게임오브젝트를 순회하면서 라인을 그림
        for (int i = 0, max = points.Length; i < max; ++i)
        {
            /*
            // 마지막 Point일 경우 첫 번째 Point 로 지정(끝까지 가면 처음 위치를 향하도록)
            ++nextIdx;
            nextPos = (nextIdx >= max) ? points[1].position : points[nextIdx].position;

            // 시작 위치에서 종료 위치까지 라인을 그림
            Gizmos.DrawLine(currPos, nextPos);

            currPos = nextPos;
            */
            // 마지막 Point일 경우 그리지 않음
            ++nextIdx;
            if (nextIdx >= max)
            {
                break;
            }
            else
            {
                nextPos = points[nextIdx].position;
            }
            

            // 시작 위치에서 종료 위치까지 라인을 그림
            Gizmos.DrawLine(currPos, nextPos);

            currPos = nextPos;
        }
    }
}
