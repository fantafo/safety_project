﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CFloor : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("WaterDrops"))
        {
            // 액체 방울 비활성화
            collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            collision.gameObject.SetActive(false);
        }
    }
}