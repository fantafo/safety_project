﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using UnityEngine.EventSystems;

/// <summary>
/// 숨은그림찾기 맵에서 사용하는 숨은그림 아이템입니다.
/// </summary>
public class HiddenFinderItem : SMonoBehaviour, IPointerClickHandler
{
    internal int idx;
    public GameObject coin;
    public GameObject effect;

    public int _ItemScore;

    public int _hiddenFinderItemType;

    private void Reset()
    {
        coin = GetComponentInChildren<Renderer>().gameObject;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GetComponentInParent<HiddenFinder>().BroadcastCatch(this);
    }

    public void OnClick()
    {
        GetComponentInParent<HiddenFinder>().BroadcastCatch(this);
    }
}
