﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RenderHeads.Media.AVProVideo
{
    public class InitializePage : MonoBehaviour
    {

        public GameObject[] Gstep;

        private int curStep;



        // Use this for initialization
        void Awake()
        {
            curStep = 0;
            Debug.Log(gameObject.name+" : Page6 On");
            Gstep[0].SetActive(true);
            for (int i = 1; i < Gstep.Length; i++)
            {
                Gstep[i].SetActive(false);
            }
        }

        public void NextStep()
        {
            curStep++;
            Gstep[curStep].SetActive(true);
            Gstep[curStep - 1].SetActive(false);
        }
    }
}