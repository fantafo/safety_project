﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CExperimentSceneManager : MonoBehaviour
{
    public Text _currLimitText;
    public Image _stabilityLimitBarImage;
    public Image _amountBarImage;

    public GameObject _pourButtonPanel;
    public Text _resultText;
    public GameObject _retryButton;

    private bool _isCleared = false;
    private bool _isBlowing = false;

    private CFaucet _faucet;
    private CWater _water;

    private void Start()
    {
        _faucet = GameObject.Find("Faucet").GetComponent<CFaucet>();
        _water = GameObject.Find("Water").GetComponent<CWater>();

        Init();
    }

    public void Init()
    {
        _isCleared = false;
        _isBlowing = false;

        _retryButton.SetActive(false);
        _resultText.gameObject.SetActive(false);
        _pourButtonPanel.SetActive(true);

        _stabilityLimitBarImage.fillAmount = 0.0f;
        _amountBarImage.fillAmount = 0.0f;
    }

    private void Update()
    {
        if (!_pourButtonPanel.activeSelf)
        {
            return;
        }

        _amountBarImage.fillAmount = _water.GetTargetAmountLate();

        _stabilityLimitBarImage.fillAmount = _water.GetCurrLimit();

        if (1.0f <= _amountBarImage.fillAmount)
        {
            _isCleared = true;
        }

        else if (1.0f <= _stabilityLimitBarImage.fillAmount)
        {
            _isBlowing = true;
        }

        if (_isCleared || _isBlowing)
        {
            _faucet.StopPouring();

            _pourButtonPanel.SetActive(false);
            _resultText.text = (_isCleared ? "Success!" : "BOOM!!!");
            _resultText.gameObject.SetActive(true);

            StopCoroutine(this.SetRetryButtonActiveCoroutine());
            StartCoroutine(this.SetRetryButtonActiveCoroutine());
        }        
    }

    public void OnClickPourButton()
    {
        Debug.Log("OnClickPourButton");
        _faucet._isPour = true;
    }

    public void OnClickStopButton()
    {
        Debug.Log("OnClickStopButton");
        _faucet._isPour = false;
    }

    public void OnClickRetryButton()
    {
        Debug.Log("OnClickRetryButton");

        // 다시하기 누르면 상태들 초기화
        //_retryButton.SetActive(false);

        Init();
        _faucet.Init();
        _water.Init();
    }

    private IEnumerator SetRetryButtonActiveCoroutine()
    {
        yield return new WaitForSeconds(2.0f);
        _retryButton.SetActive(true);
    }
}