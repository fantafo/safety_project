﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

using UnityEngine.UI;

public class SummaryQuizManager : AutoCommander
{
    public static SummaryQuizManager main;

    // SummaryQuiz 가 모두 끝났음을 알리는 플래그
    public bool _isFinished = false;

    public GameObject[] _labType;

    public AudioSource _quizBGM;

    public int _curContentIndex = -1;

    private int _labTypeIdx = -1;

    //private SimpleQuizManager[] _quizManagers;
    private QuizManager[] _quizManagers;
    private int _currQuizManagersIdx = 0;

    private Coroutine _co = null;
    private Coroutine _activeQuizCo = null;

    protected override void Awake()
    {
        base.Awake();
        main = this;
    }

    private void Start()
    {
        if (-1 == _curContentIndex)
        {
            _labTypeIdx = (SafetyTrainingSceneManager.main.GetContentIndex() - 1);
        }
        else
        {
            _labTypeIdx = _curContentIndex;
        }

        for (int i = 0; i < _labType.Length; ++i)
        {
            _labType[i].SetActive(false);
        }

        _labType[_labTypeIdx].SetActive(true);

        _quizManagers = _labType[_labTypeIdx].GetComponentsInChildren<QuizManager>();

        if (null != _quizManagers)
        {
            for (int i = 0; i < _quizManagers.Length; ++i)
            {
                _quizManagers[i].gameObject.SetActive(false);
            }

            //_quizManagers[_currQuizManagersIdx].gameObject.SetActive(true);
        }

        // 퀴즈 시작 전 몇 번째 퀴즈를 진행해야 할지 서버에 요청
        Networker.Send(C_Safety.RequestDetailMapStage());
    }

    public void QuizStart()
    {
        if (null != _co)
        {
            StopCoroutine(_co);
            _co = null;
        }
        _co = StartCoroutine(this.UpdateCoroutine());
    }

    private IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (_quizManagers.Length > _currQuizManagersIdx)
            {
                // 현재 풀고있는 문제가 끝났고 끝났다는 사실을 서버에 보내지 않았다면 
                if (true == _quizManagers[_currQuizManagersIdx]._isFinished &&
                    false == _quizManagers[_currQuizManagersIdx]._isQuizEndSent)
                {
                    Debug.Log("========== QuizEnd " + _currQuizManagersIdx + " / " + _quizManagers.Length + " ==========");

                    // 문제들 끄기
                    for (int i = 0; i <= _currQuizManagersIdx; ++i)
                    {
                        _quizManagers[i].gameObject.SetActive(false);
                    }

                    // 서버에 문제를 풀었다고 알림 플래그 true
                    _quizManagers[_currQuizManagersIdx]._isQuizEndSent = true;

                    // 서버에 문제를 풀었다고 알리고 대기할 시간 보냄
                    Networker.Send(C_Safety.QuizEnd((short)_currQuizManagersIdx, 3000));


                    //++_currQuizManagersIdx;
                    //
                    //if (null != _activeNextQuizCo)
                    //{
                    //    StopCoroutine(_activeNextQuizCo);
                    //    _activeNextQuizCo = null;
                    //}
                    //_activeNextQuizCo = StartCoroutine(this.ActiveNextQuizCoroutine());
                }
            }

            else
            {
                
                // SummaryQuiz 가 모두 끝났음
                _isFinished = true;
                _currQuizManagersIdx = 0;
                Debug.Log("Summary Quiz Finished is true");
            }

            yield return null;
        }
    }

    private IEnumerator ActiveQuizCoroutine(short idx)
    {
        Debug.Log("========== ActiveQuizCoroutine ==========");

        _currQuizManagersIdx = idx;

        yield return new WaitForSeconds(0.5f);

        if (_quizManagers.Length > idx)
        {            
            _quizManagers[idx].gameObject.SetActive(true);
        }
    }

    public void ActiveQuiz(short index)
    {
        Debug.Log("========== ActiveQuiz " + index + " ==========");
        if (null != _activeQuizCo)
        {
            StopCoroutine(_activeQuizCo);
            _activeQuizCo = null;
        }
        _activeQuizCo = StartCoroutine(this.ActiveQuizCoroutine(index));
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _quizBGM.Play();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _quizBGM.Stop();
    }

    private void OnDestroy()
    {
        _quizBGM.Stop();
    }
}