﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveImage : MonoBehaviour
{
    private Hashtable _moveToHt = null;

    private Hashtable _returnHt = null;

    public float _targetValue = 1.5f;
    public float _originValue = 0.0f;

    public Vector3 _movePos = Vector3.zero;
    public Vector3 _originPos = Vector3.zero;

    private void Awake()
    {
            // 0.0f : -552.4856
        _originPos = new Vector3( -0.1f, 1.1f, -0.1f);   // world position( 0.2f, 1.1f, -0.2f ), local position(-594.0f, 0.3f, 0.0f);
    }

    private void Start()
    {
        Transform mainPlayer = GameObject.Find("MainPlayer").GetComponent<Transform>();

        _movePos = mainPlayer.position - transform.position;

        _moveToHt = new Hashtable();
        //_moveToHt.Add("y", _targetValue);
        _moveToHt.Add("position", Vector3.zero);
        _moveToHt.Add("time", 2);
        _moveToHt.Add("delay", 0);
        _moveToHt.Add("onupdate", "myUpdateFunction");
        _moveToHt.Add("looptype", iTween.LoopType.none);
        
        _returnHt = new Hashtable();
        //_returnHt.Add("y", _originValue);
        //_returnHt.Add("z", -0.2203202f);
        _returnHt.Add("position", _originPos);
        _returnHt.Add("time", 2);
        _returnHt.Add("delay", 0);
        _returnHt.Add("onupdate", "myUpdateFunction");
        _returnHt.Add("looptype", iTween.LoopType.none);
    }

    public void OnPointerEnterButton()
    {
        //Debug.Log("OnPointerEnterButton: " + transform.localPosition);
        //Debug.Log("OnPointerEnterButton: " + transform.position);
        //Debug.Log("OnPointerEnterButton: " + );
        Debug.Log(GameObject.Find("MainPlayer").GetComponent<Transform>().position - transform.position);// 플레이어에게 오는 벡터값(-1.4, -1.1, 0.2)
        //_moveToHt.Add("z", );
        ButtonMoveToPlayer();
    }

    public void OnPointerExitButton()
    {
        Debug.Log("OnPointerExitButton");
        ButtonReturnToOriginPosition();
    }

    private void ButtonMoveToPlayer()
    {
        Debug.Log("ButtonCloseToPlayer");
        iTween.MoveTo(gameObject, _moveToHt);
        //iTween.ScaleTo(gameObject, _enlargeHt);
    }

    private void ButtonReturnToOriginPosition()
    {
        Debug.Log("ButtonReturnToOriginPosition");
        iTween.MoveTo(gameObject, _returnHt);
        //iTween.ScaleTo(gameObject, _reductionHt);
    }

    
}