﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyLookAtCamera : MonoBehaviour
{
    private Transform _tr;
    private Transform _mainCameraTr;

    private void Start()
    {
        _tr = transform;
        _mainCameraTr = Camera.main.transform;

        _tr.LookAt(_mainCameraTr.position);
    }
}
