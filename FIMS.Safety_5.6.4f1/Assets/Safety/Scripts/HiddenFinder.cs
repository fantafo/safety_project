﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

using UnityEngine.UI;

public class HiddenFinder : AutoCommander
{
    public CanvasGroup canvasGroup;
    public GameObject gameEndObj;
    public GameObject countdown;
    private HiddenFinderItem[] items; // 코인들 참조할 배열
    public GameObject _container;
    public int count;               // 한 번에 출현할 코인 갯수
    bool started;

    private int _currentSpotCount;

    public Animator _handAnimator;

    public GameObject _infoMessage;
    public float _messageShowingTime = 3.0f;
    private WaitForSeconds _delay;
    private Coroutine _co = null;
    private Coroutine _blinkCoroutine = null;

    private Coroutine _lightOnCo = null;
    private Coroutine _gameStartCo = null;

    public GameObject _hiddenFinderParticleEffect;

    public AudioSource _fxAudioSource;
    public AudioClip _fx_found_clip;
    public AudioClip _fx_LightOn_Clip;
    public AudioSource _bgmSound;

    public Light _directionalLight;
    private float _directionalLightOriginIntencity = 0.0f;
    public Light[] _pointLights;

    protected override void Awake()
    {
        base.Awake();

        //for (int i = 0; i < items.Length; i++)
        //{
        //    items[i].idx = i;
        //    items[i].coin.SetActive(false);
        //    if (items[i].effect != null)
        //        items[i].effect.SetActive(false);
        //}
    }


    private void Start()
    {
        if (null == _directionalLight)
        {
            _directionalLight = GameObject.Find("Directional Light").GetComponent<Light>();
        }

        if (null != _directionalLight)
        {
            _directionalLightOriginIntencity = _directionalLight.intensity;
            _directionalLight.intensity = 0.0f;
        }

        if (null != _pointLights)
        {
            for (int i = 0; i < _pointLights.Length; ++i)
            {
                _pointLights[i].enabled = false;
            }
        }

        Init();
    }

    public void Init()
    {
        items = _container.GetComponentsInChildren<HiddenFinderItem>();

        for (int i = 0; i < items.Length; i++)
        {
            items[i].idx = i;
            //items[i].coin.SetActive(false);
            items[i].gameObject.SetActive(false);
            if (items[i].effect != null)
                items[i].effect.SetActive(false);
        }

        // 현재 장비 갯수
        _currentSpotCount = count;
        //_currentGearCount = FTF.RoomInfo.main.currentUserCount / 2;     // 플레이어 수에 비례하여 아이템 출현
        if (_currentSpotCount <= 0)
        {
            _currentSpotCount = 1;
        }

        if (null == _handAnimator)
        {
            _handAnimator = GameObject.FindGameObjectWithTag("Hand").GetComponent<Animator>();
        }

        // 메시지 유지 시간
        _delay = new WaitForSeconds(_messageShowingTime);

        // 처음엔 메시지 박스 안 보이게
        //_infoMessage.SetActive(false);

        gameEndObj.SetActive(false);
        countdown.SetActive(false);

        if (null != _gameStartCo)
        {
            StopCoroutine(_gameStartCo);
        }
        _gameStartCo = StartCoroutine(this.GameStartCoroutine());
    }

    private IEnumerator GameStartCoroutine()
    {
        yield return new WaitForSeconds(10.0f);

        if (null != _lightOnCo)
        {
            StopCoroutine(_lightOnCo);
        }
        _lightOnCo = StartCoroutine(this.LightOnCoroutine());

        //BroadcastGameStart();
    }

    // 내레이션 끝나면 조명 켜질 연출용 코루틴
    private IEnumerator LightOnCoroutine()
    {
        WaitForSeconds wfs = new WaitForSeconds(0.5f);

        // Directional Light 의 Intensity 조정
        yield return wfs;
        //_directionalLight.intensity = _directionalLightOriginIntencity;

        // 라이트 켜지는 효과음 재생
        //_fxAudioSource.clip = _fx_LightOn_Clip;
        //_fxAudioSource.Play();

        for (int i = 0; i < _pointLights.Length; ++i)
        {
            // 라이트 켜기
            yield return wfs;
            _pointLights[i].enabled = true;

            // 라이트 켜지는 효과음 재생
            _fxAudioSource.clip = _fx_LightOn_Clip;
            _fxAudioSource.Play();
        }

        _directionalLight.intensity = _directionalLightOriginIntencity;
        _fxAudioSource.clip = _fx_LightOn_Clip;
        _fxAudioSource.Play();

        BroadcastGameStart();
    }

    // 게임 시작 알림
    public void BroadcastGameStart()
    {
        if (!started)
        {
            //_infoMessage.SetActive(false);
            started = true;
            Broadcast("GameStart");
        }
    }
    //아이템 먹을때 발생하는 이벤트
    public void BroadcastCatch(HiddenFinderItem item)
    {
        //옵저버를 위해서 패킷을 전달.
        Broadcast("ObserverFind", PlayerInstance.main.instanceId, item.idx);

        //먹고 점수처리
        Find(PlayerInstance.main.instanceId, item.idx);
    }

    [OnCommand]
    void ObserverFind(int instanceId, int idx)
    {
#if FTF_OBSERVER
        if (instanceId == PlayerInstance.lookMain.instanceId)
            Find(PlayerInstance.main.instanceId, idx);
#endif
    }

    void Find(int instanceId, int idx)
    {
        // 해당 idx의 코인이 활성화 상태이면
        if (items[idx].coin.activeSelf)
        {
            // 클릭된 코인 비활성화
            if (items[idx].coin != null)
            {
                items[idx].coin.SetActive(false);
            }
            Networker.Send(C_Scene.UpdateScore(instanceId, C_Scene.UpdateType.Relative, items[idx]._ItemScore));

            SafetyTrainingSceneManager.main.PlayScoreEffect();
            
            Finding(idx, instanceId);
        }
    }

    void Finding(int idx, int instanceId)
    {
        items[idx].gameObject.SetActive(false);

        //아이템 먹기
        CatchItem(items[idx]);

        // 아이템 획득시 아이템별 파티클 이펙트 재생을 해야하는 곳
        if (items[idx].effect != null)
        {
            //items[idx].effect.SetActive(true);
        }

        // 실제 아이템 획득 파티클 이펙트 재생을 하는 곳
        //var go = Instantiate(Resources.Load("CoinEffect"), items[idx].transform.position, items[idx].transform.rotation, null);
        var go = Instantiate(_hiddenFinderParticleEffect, items[idx].transform.position, items[idx].transform.rotation, null);
        // 1.1초 뒤에 Destroy
        STween.delayCall(1.1f, () => Destroy(go));

        ////////////////////////////////////////////////////////
        --_currentSpotCount;

        // 획득해야 할 장비가 남아있지 않으면
        if (0 >= _currentSpotCount)
        {
            // 제한시간 이내라도 종료시킴
            if (countdown.GetComponent<FTF.Earth.Countdown>().onComplete != null)
            {
                STween.alpha(countdown.gameObject, 0, 1).SetOnComplete(() => countdown.gameObject.SetActive(false));
                gameEndObj.GetComponentInChildren<Text>().text = "위험요소를 모두 발견하였습니다.";
                countdown.GetComponent<FTF.Earth.Countdown>().onComplete.Invoke();
            }
        }
    }

    //잡는 애니메이션, 안내 메세지
    public void CatchItem(HiddenFinderItem item)
    {
        // 손 뻗어서 잡는 모션 애니메이션
        _handAnimator.SetTrigger("RightHandFright");

        // 아이템 집어온 뒤 들어올리는 모션 애니메이션
        //_handAnimator.SetTrigger("PickUp");

        // 아이템 획득 안내 메시지 띄움
        if (_co != null)
        {
            StopCoroutine(_co);
        }
        _co = StartCoroutine(this.ShowInfoMessageCoroutine(item._hiddenFinderItemType));

        _fxAudioSource.clip = _fx_found_clip;
        _fxAudioSource.Play();
    }

    private IEnumerator ShowInfoMessageCoroutine(int itemType)
    {
        if (_infoMessage.activeSelf)
        {
            _infoMessage.SetActive(false);
            yield return null;
        }

        string str = string.Empty;

        switch (itemType)
        {
            case 0:
                {
                    str = "가스통 가스 분출을";
                }
                break;
            case 1:
                {
                    str = "환기구 막힘을";
                }
                break;
            case 2:
                {
                    str = "배전선 노출을";
                }
                break;
            case 3:
                {
                    str = "실험실에서 취식을";
                }
                break;
            case 4:
                {
                    str = "대피로를 막은 적재물을";
                }
                break;
            case 5:
                {
                    str = "소화기 관리부실을";
                }
                break;
            case 6:
                {
                    str = "시약 누출을";
                }
                break;
            case 7:
                {
                    str = "안전보호함 관리소홀을";
                }
                break;
            case 8:
                {
                    str = "콘센트 과부하를";
                }
                break;
            case 9:
                {
                    str = "실험실에서 취식을";
                }
                break;
        }
        _infoMessage.GetComponentInChildren<Text>().text = str + " 발견했습니다.";
        _infoMessage.SetActive(true);

        yield return _delay;

        _infoMessage.SetActive(false);
    }

    // 클라이언트들에게 게임 시작 알림
    [OnCommand]
    void GameStart()
    {
        _bgmSound.Play();

        started = true;

        _infoMessage.SetActive(false);

        // 준비 완료되면 캔버스 비활성화?
        canvasGroup.twnAlpha(0, 0.5f).SetOnComplete(() => canvasGroup.gameObject.SetActive(false));

        // 제한시간 활성화
        countdown.SetActive(true);

        // 내가 선생님(방장)이면
        if (PlayerInstance.main.IsRoomMaster)
        {
            //for (int i = 0; i < count; i++)
            for (int i = 0; i < _currentSpotCount; i++)
            {
                /*
                while (true)
                {
                    // 생성된 코인 중 랜덤하게 하나를 선택
                    var coin = items.Random();

                    // 해당 코인이 비활성화 상태이면
                    if (!coin.coin.activeSelf)
                    {
                        // 활성화 상태로 바꿈
                        coin.coin.SetActive(true);

                        // 사용자들에게 해당 코인이 활성화 상태로 바뀌었음을 알림
                        Broadcast("SetActive", items.IndexOf(coin));
                        break;
                    }
                }
                */
                /*
                var coin = items[i];
                if (!coin.coin.activeSelf)
                {
                    coin.coin.SetActive(true);
                    Broadcast("SetActive", items.IndexOf(coin));
                }
                */
                if (!items[i].gameObject.activeSelf)
                {
                    items[i].gameObject.SetActive(true);

                    Broadcast("SetActive", items[i].idx);
                }
            }
        }
    }

    public void GameEnd()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].idx = i;
            //items[i].coin.SetActive(false);
            //items[i].coin.transform.parent.gameObject.SetActive(false);
            items[i].gameObject.SetActive(false);
            if (items[i].effect != null)
                items[i].effect.SetActive(false);
        }
        if (gameEndObj != null)
        {
            gameEndObj.gameObject.SetActive(true);
        }
    }
    [OnCommand]
    void SetActive(int idx)
    {
        // 해당 idx의 코인을 활성화

        //items[idx].coin.SetActive(true);
        items[idx].gameObject.SetActive(true);

        if (items[idx].effect)
            items[idx].effect.SetActive(false);
    }
}
