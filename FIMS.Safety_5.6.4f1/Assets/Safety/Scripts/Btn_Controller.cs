﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

using System.IO;

namespace RenderHeads.Media.AVProVideo
{
    public class Btn_Controller : MonoBehaviour
    {
        StringBuilder m_Builder;

        [SerializeField]
        private static string str_InputTxt = "";

        public GameObject DummyManager;

        private string str_Num = "";

        public GameObject[] go_Input;

        private Text txt;

        private int pageCnt;

        private string _strPath = "./";

        // Use this for initialization
        void Awake()
        {
            pageCnt = DummyManager.GetComponent<DummyControl>().iNowCnt;
            //GameObject.Find("Page" + (pageCnt - 1).ToString()).SetActive(false);
            StartCoroutine(CheckPage());
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log("확인중...." + str_InputTxt + " >> " + str_InputTxt.Length);
            
        }

        IEnumerator CheckPage()
        {
            while (true)
            {
                Debug.Log("코루틴");

                if (go_Input[8].GetComponentInChildren<Text>().text != "")
                {
                    yield return new WaitForSeconds(3.0f);
                    Debug.Log("코루틴 if문");
                    if (go_Input[8].GetComponentInChildren<Text>().text != "")
                    {

                        DummyManager.GetComponent<DummyControl>().Next();
                        break;
                        //다음페이지로...
                    }
                }
                Debug.Log("코루틴 if문 다음");
                yield return null;
            }
            yield return null;
        }

        public void CheckTxt(int num)
        {
            str_Num = num.ToString();
        }

        public void Input1()
        {
            if (str_InputTxt.Length < 9)
            {
                str_InputTxt = str_InputTxt.Insert(str_InputTxt.Length, str_Num);
            }
            go_Input[str_InputTxt.Length - 1].GetComponentInChildren<Text>().text = str_Num;

        }

        public void Delete()
        {
            if (str_InputTxt.Length > 0)
            {
                str_InputTxt = str_InputTxt.Substring(0, str_InputTxt.Length - 1);
                go_Input[str_InputTxt.Length].GetComponentInChildren<Text>().text = "";
            }
        }

        public void Initialize()
        {
            Debug.Log(go_Input.Length);
            for (int i = 0; i < go_Input.Length; i++)
            {
                go_Input[i].GetComponentInChildren<Text>().text = "";
            }
            str_InputTxt = "";
        }

        public void NextBtn()
        {
            if (go_Input[8].GetComponentInChildren<Text>().text != "")
            {
                // 로그인 다음 장면으로 넘어가기
                //DummyManager.GetComponent<DummyControl>().Next();

                // 여기서 파일 생성
                string studentId = string.Empty;
                for (int i = 0; i < go_Input.Length; ++i)
                {
                    studentId += go_Input[i].GetComponentInChildren<Text>().text;
                }

                FileStream f = new FileStream(_strPath + "Id", FileMode.Append, FileAccess.Write);
                StreamWriter writer = new StreamWriter(f, System.Text.Encoding.Unicode);
                writer.WriteLine(studentId);
                writer.Close();

                //DummyManager.GetComponent<DummyControl>().Next();
                Debug.Log(_strPath + "에 Id 파일 생성");
            }
        }
    }
}