﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CFaucet : MonoBehaviour
{
    public GameObject _waterDropsPrefab;
    public float _delayTime = 1.0f;
    public Transform _dropPoint;
    public Transform _waterDropsObjectPool;

    public float _anglarValue = 0.01f;

    public float _currDelayTime;

    private Transform _tr;
    private WaitForSeconds _delay;

    public bool _isPour = false;
    private Vector3 _faucetLocalEulerAngles = Vector3.zero;

    private const float MAX_ANGLE = 120.0f;

    private void Awake()
    {
        _tr = this.transform;
        for (int i = 0; i < 50; ++i)
        {
            GameObject waterDrops = Instantiate(_waterDropsPrefab, _dropPoint.position, Quaternion.identity);
            waterDrops.transform.SetParent(_waterDropsObjectPool);
            waterDrops.SetActive(false);
        }

        _currDelayTime = _delayTime;
    }

    private IEnumerator Start()
    {
        while (true)
        {
            _delay = new WaitForSeconds(_currDelayTime);

            if (_tr.localEulerAngles.z > 90.0f)
            {
                int i = 0;

                // _delayTime 마다 오브젝트 풀에서 물방울 활성화
                for (i = 0; i < _waterDropsObjectPool.childCount; ++i)
                {
                    if (_waterDropsObjectPool.GetChild(i).gameObject.activeSelf == false)
                    {
                        _waterDropsObjectPool.GetChild(i).transform.position = _dropPoint.position;
                        _waterDropsObjectPool.GetChild(i).gameObject.SetActive(true);
                        break;
                    }
                }

                // 오브젝트 풀에 비활성화 된 물방울이 없을 경우
                if (_waterDropsObjectPool.childCount <= i)
                {
                    // 새 물방울 생성
                    GameObject waterDrops = Instantiate(_waterDropsPrefab, _dropPoint.position, Quaternion.identity);

                    //오브젝트 풀에 자식으로 추가
                    waterDrops.transform.SetParent(_waterDropsObjectPool);
                }
            }

            yield return _delay;
        }
    }

    public void Init()
    {
        _isPour = false;

        _tr.localEulerAngles = Vector3.zero;
        _currDelayTime = 0.0f;

        for (int i = 0; i < _waterDropsObjectPool.childCount; ++i)
        {
            _waterDropsObjectPool.GetChild(i).position = _dropPoint.position;
            _waterDropsObjectPool.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            _waterDropsObjectPool.GetChild(i).gameObject.SetActive(false);
        }

        _waterDropsObjectPool.gameObject.SetActive(true);
    }

    private void Update()
    {
        _faucetLocalEulerAngles = _tr.localEulerAngles;

        if (_isPour)
        {
            _faucetLocalEulerAngles.z += _anglarValue;

            if (MAX_ANGLE < _faucetLocalEulerAngles.z)
            {
                _faucetLocalEulerAngles.z = MAX_ANGLE;
            }
        }
        else
        {
            _faucetLocalEulerAngles.z -= _anglarValue;

            if (_faucetLocalEulerAngles.z < 0.0f)
            {
                _faucetLocalEulerAngles = Vector3.zero;
            }
        }

        _tr.localEulerAngles = _faucetLocalEulerAngles;

        _currDelayTime = _delayTime * (90.0f / (_faucetLocalEulerAngles.z * 3.0f));

        if (_currDelayTime > _delayTime)
        {
            _currDelayTime = _delayTime;
        }
    }

    public void StopPouring()
    {
        _waterDropsObjectPool.gameObject.SetActive(false);
        
        Debug.Log("Pouring Stopped");
    }
}