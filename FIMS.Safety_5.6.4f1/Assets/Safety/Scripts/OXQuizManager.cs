﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

using UnityEngine.UI;

//public class OXQuizManager : AutoCommander
public class OXQuizManager : QuizManager
{
    //public bool _isFinished = false;

    public int QuizScore;
    public GameObject[] _answerCheckImageObj;

    public GameObject _resultSuccessImage;
    public GameObject _resultFailedImage;
    public GameObject _commentaryTextObj;

    public ParticleSystem _ScoreParticle;//2018.0731김영민 추가. 내 점수 부분 이펙트.
    public GameObject[] _effectBtn;

    public int _rightAnswer;

    public AudioSource _buttonFx;
    public AudioSource _clockTickBgm;

    public Text _o_percentageText;
    public Text _x_percentageText;

    private Coroutine _waitingCo = null;
    private Coroutine _onQuizCo = null;
    private Button[] _answerButtons;
    private CustomTimer _timer;
    private int _answerNum = -1;

    // 2018.08.21 영민 추가
    public Button[] _answerBtn;
    private bool _onQuiz = false;

    public bool _isAnswerSelected = false;
    private int _answerO_Count = 0;
    private int _answerX_Count = 0;

    private int _prevSelectedAnswer = -1;

    private void Start()
    {
        _answerButtons = this.gameObject.GetComponentsInChildren<Button>();

        for (int i = 0; i < _answerButtons.Length; ++i)
        {
            // 버튼 테두리 초기화
            _effectBtn[i].SetActive(false);
            _answerBtn[i].enabled = false;

            // 선택한 답 체크용 V 초기화
            _answerCheckImageObj[i].SetActive(false);
        }
        if (null != _waitingCo)
        {
            StopCoroutine(_waitingCo);
        }

        _timer = this.gameObject.GetComponentInChildren<CustomTimer>();

        if (null != _onQuizCo)
        {
            StopCoroutine(_onQuizCo);
        }
        _onQuizCo = StartCoroutine(this.OnQuizCoroutine());

        // 정답, 오답 파티클 위치 지정
        Vector3 btn1Pos = new Vector3( -230.0f, -109.0f, 0.0f );
        Vector3 btn2Pos = new Vector3(  230.0f, -109.0f, 0.0f );

        // O 가 정답이면
        if (_rightAnswer == 0)
        {
            //_resultSuccessImage.transform.position = _answerButtons[0].transform.position;
            //_resultFailedImage.transform.position  = _answerButtons[1].transform.position;
            _resultSuccessImage.transform.parent = _answerButtons[0].transform;
            _resultFailedImage.transform.parent = _answerButtons[1].transform;
        }

        // X 가 정답이면
        else if (_rightAnswer == 1)
        {
            //_resultSuccessImage.transform.position = _answerButtons[1].transform.position;
            //_resultFailedImage.transform.position  = _answerButtons[0].transform.position;
            _resultSuccessImage.transform.parent = _answerButtons[1].transform;
            _resultFailedImage.transform.parent = _answerButtons[0].transform;
        }

        // 해설 텍스트 끄기
        _commentaryTextObj.SetActive(false);
    }

    // 퀴즈 시작되면 유예시간을 두고 시작하게 하는 코루틴
    private IEnumerator OnQuizCoroutine()
    {
        yield return new WaitForSeconds(3.0f);

        // 시계 소리 재생
        _clockTickBgm.Stop();
        if (!_clockTickBgm.loop)
        {
            _clockTickBgm.loop = true;
        }
        _clockTickBgm.Play();

        for (int i = 0; i < _answerBtn.Length; ++i)
        {
            _answerBtn[i].enabled = true;
        }
        _onQuiz = true;
        _timer.StartTimer();
    }

    private IEnumerator WaitingCoroutine()
    {
        yield return new WaitForSeconds(3.0f);
        Broadcast("QuizFinished", PlayerInstance.main.instanceId);
    }

    public void BroadcastQuizFinished()
    {
        // 시계 소리 정지
        _clockTickBgm.Stop();

        for (int i = 0; i < _answerButtons.Length; ++i)
        {
            _answerButtons[i].interactable = false;
        }        

        BroadcastQuizScoreProcess(_answerNum);

        if (null != _waitingCo)
        {
            StopCoroutine(_waitingCo);
        }
        _waitingCo = StartCoroutine(this.WaitingCoroutine());
    }
    [OnCommandAttribute]
    private void QuizFinished(int instanceId)
    {
        _isFinished = true;
    }

    // OX 버튼 눌렀을 때 호출되는 함수
    public void OnClickQuizAnswerButton(int answerNum)
    {
#if FTF_OBSERVER
#else
        if (PlayerInstance.main.IsRoomMaster)
        {
            //Broadcast("OnClickQuizAnswer", PlayerInstance.main.instanceId, answerNum);                        
        }

        // 내가 선택한 답이 O 이면
        if (0 == answerNum)
        {
            Networker.Send(Quiz.OX(Quiz_OX.True));
        }
        // 내가 선택한 답이 X 이면
        else if (1 == answerNum)
        {
            Networker.Send(Quiz.OX(Quiz_OX.False));
        }

        if (0 == _prevSelectedAnswer)
        {
            Broadcast("CmdOnDecreaseAnswerOCount");
        }
        else if (1 == _prevSelectedAnswer)
        {
            Broadcast("CmdOnDecreaseAnswerXCount");
        }

#endif
        _answerNum = answerNum;

        _prevSelectedAnswer = _answerNum;

        // 누른 퀴즈 버튼은 다른 버튼 눌리기 전 까지 비활성화
        for (int i = 0; i < _answerButtons.Length; ++i)
        {
            _answerButtons[i].interactable = true;

            _answerCheckImageObj[i].SetActive(false);
        }

        _answerButtons[answerNum].interactable = false;

        // 선택한 답안 표시용 체크
        _answerCheckImageObj[answerNum].SetActive(true);

        Broadcast("CmdOnClickQuizAnswerButton", answerNum);

        //SetAnswerSelected(true);
    }
    [OnCommandAttribute]
    public void CmdOnDecreaseAnswerOCount()
    {
        if (0 < _answerO_Count)
        {
            --_answerO_Count;
        }
    }
    [OnCommandAttribute]
    public void CmdOnDecreaseAnswerXCount()
    {
        if (0 < _answerX_Count)
        {
            --_answerX_Count;
        }
    }

    [OnCommandAttribute]
    public void CmdOnClickQuizAnswerButton(int answerNum)
    {
        int totalStudentCount = RoomInfo.main.maxUser;
        if (0 >= totalStudentCount)
        {
            totalStudentCount = 1;
        }

        if (0 == answerNum)
        {
            ++_answerO_Count;
        }
        else if (1 == answerNum)
        {
            ++_answerX_Count;
        }

        float percentO = (float)_answerO_Count / (float)totalStudentCount;
        float percentX = (float)_answerX_Count / (float)totalStudentCount;

        _o_percentageText.text = _answerO_Count + " 명 (" + string.Format("{0:P1}", percentO) + ")";
        _x_percentageText.text = _answerX_Count + " 명 (" + string.Format("{0:P1}", percentX) + ")";
    }

    private void SetAnswerSelected(bool isSelected)
    {
        _isAnswerSelected = isSelected;
    }

    [OnCommandAttribute]
    private void OnClickQuizAnswer(int id, int answerNum)
    {
#if FTF_OBSERVER
        if (id == PlayerInstance.lookMain.instanceId)
        {
            OnClickQuizAnswerButton(answerNum);
        }
#endif
    }

    private void BroadcastQuizScoreProcess(int answerNum)
    {
        if (_rightAnswer == answerNum)
        {
            Networker.Send(C_Scene.UpdateScore(PlayerInstance.main.instanceId, C_Scene.UpdateType.Relative, QuizScore));

            SafetyTrainingSceneManager.main.PlayScoreEffect();

            _resultFailedImage.GetComponentInChildren<AudioSource>().playOnAwake = false;
        }
        else
        {
            _resultSuccessImage.GetComponentInChildren<AudioSource>().playOnAwake = false;
        }

        ShowQuizResult();
    }

    private void ShowQuizResult()
    {        

        _resultSuccessImage.SetActive(true);
        _resultFailedImage.SetActive(true);

        _commentaryTextObj.SetActive(true);

        _onQuiz = false;

        // 정답 확인 후 서버에 선택내용 초기화 보냄
        Networker.Send(Quiz.OX(Quiz_OX.None));
    }

    public void OnPointerEnterQuizAnswerButton(int index)
    {
#if FTF_OBSERVER
#else
        if (PlayerInstance.main.IsRoomMaster)
        {
            Broadcast("OnPointerEnter", PlayerInstance.main.instanceId, index);
        }
#endif
        if (_onQuiz)
        {
            QuizAnswerButtonEnlargeScale(index);
            _effectBtn[index].SetActive(true);

            if (_buttonFx.isPlaying)
            {
                _buttonFx.Stop();
            }
            _buttonFx.Play();
        }
    }

    public void OnPointerExitQuizAnswerButton(int index)
    {
#if FTF_OBSERVER
#else
        if (PlayerInstance.main.IsRoomMaster)
        {
            Broadcast("OnPointerExit", PlayerInstance.main.instanceId, index);
        }
#endif
        if (_onQuiz)//0821
        {
            QuizAnswerButtonReturnToOriginScale(index);
            //if (_simpleQuizButtons[index].IsInteractable())
            {
                _effectBtn[index].SetActive(false);
            }
        }
    }

    [OnCommand]
    private void OnPointerEnter(int id, int index)
    {
#if FTF_OBSERVER
        if (id == PlayerInstance.lookMain.instanceId)
        {        
            OnPointerEnterQuizAnswerButton(index);
        }
#endif
    }
    [OnCommand]
    private void OnPointerExit(int id, int index)
    {
#if FTF_OBSERVER
        if (id == PlayerInstance.lookMain.instanceId)
        {        
            OnPointerExitQuizAnswerButton(index);
        }
#endif
    }

    private void QuizAnswerButtonEnlargeScale(int index)
    {
        iTween.ScaleTo(_answerButtons[index].gameObject, Vector3.one * 1.5f, 1.0f);
    }

    private void QuizAnswerButtonReturnToOriginScale(int index)
    {
        iTween.ScaleTo(_answerButtons[index].gameObject, Vector3.one, 1.0f);
    }

    //0731김영민 추가. 내 점수 부분 이펙트.
    public void PlayParticle()
    {
        if (_ScoreParticle)
        {
            if (_ScoreParticle.isPlaying == true)
            {
                _ScoreParticle.Stop();
                _ScoreParticle.Clear();
            }
            else
            {
                _ScoreParticle.Play();
            }
        }
    }
}