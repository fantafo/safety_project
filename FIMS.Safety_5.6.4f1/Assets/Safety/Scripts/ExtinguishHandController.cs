﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtinguishHandController : MonoBehaviour
{
    public Animator _handAnimator;
    public Renderer _handRenderer;

    private Transform _mainCameraTr;

    public Transform _handTr;

    public GameObject[] _grabbedItem;

    public int _grabbedItemIdx;

    private Coroutine _handFadingCo = null;
    private Coroutine _handRotatingCo = null;

    public GameObject _waterfall;

    private Vector3 _handEulerAngles;

    private void Start()
    {
        //GameObject labEscape = GameObject.Find("LabEscapeTest");

        _mainCameraTr = GameObject.FindGameObjectWithTag("MainCamera").transform;

        //if (labEscape.activeSelf == false || labEscape == null)
        if (_mainCameraTr == null)
        {
            Debug.Log("=============================");
            return;
        }
        if (null == _handTr)
        {
            
        }
        _handTr = transform;

        for (int i = 0; i < _grabbedItem.Length; ++i)
        {
            _grabbedItem[i].SetActive(false);
        }

        //_handFadingCo = StartCoroutine(this.HandFadingCoroutine());
        //_handRotatingCo = StartCoroutine(this.HandRotatingCoroutine());

        _waterfall.SetActive(false);
    }

    private void Update()
    {
        _handEulerAngles = Vector3.zero;
        _handEulerAngles = _mainCameraTr.eulerAngles;
        _handEulerAngles.x = 0.0f;
        _handEulerAngles.z = 0.0f;
        _handTr.eulerAngles = _handEulerAngles;
        //Debug.Log("_mainCameraTr: " + _mainCameraTr.eulerAngles + ", _handEulerAngle : " + _handTr.eulerAngles);
    }
    // 카메라가 Y축으로 회전하면 손도 따라서 움직이는 처리
    private IEnumerator HandRotatingCoroutine()
    {
        Vector3 handEulerAngles = Vector3.zero;
        Debug.Log("=======HandRotatingCoroutine()");
        while (true)
        {
            handEulerAngles = _mainCameraTr.eulerAngles;
            handEulerAngles.x = 0.0f;
            handEulerAngles.z = 0.0f;
            _handTr.eulerAngles = handEulerAngles;
            Debug.Log("_mainCameraTr: " + _mainCameraTr.eulerAngles + ", _handEulerAngle : " + _handTr.eulerAngles);
            yield return null;
        }
    }

    public void ActivateWaterFall()
    {
        if (!_waterfall.activeSelf)
        {
            _waterfall.SetActive(true);
        }
    }
    /*
    public void DisableAllGrabbedItem()
    {
        for (int i = 0; i < _grabbedItem.Length; ++i)
        {
            _grabbedItem[i].SetActive(false);
        }
    }

    public void ActiveGrabbedItemFire01()
    {
        _grabbedItem[0].SetActive(true);
    }

    public void DisableGrabbedItemFire01()
    {
        _grabbedItem[0].SetActive(false);
    }

    public void ActiveGrabbedItemFire02()
    {
        _grabbedItem[1].SetActive(true);
    }

    public void DisableGrabbedItemFire02()
    {
        _grabbedItem[1].SetActive(false);
    }

    public void ActiveGrabbedItemFire03()
    {
        _grabbedItem[2].SetActive(true);
    }

    public void DisableGrabbedItemFire03()
    {
        _grabbedItem[2].SetActive(false);
    }
    */
}