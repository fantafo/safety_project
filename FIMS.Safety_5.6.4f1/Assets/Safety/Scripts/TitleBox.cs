﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class TitleBox : MonoBehaviour
{
    private WaitForSeconds _wfs;

    //private Text _titleText;

    private void Awake()
    {
        _wfs = new WaitForSeconds(3.0f);

        //_titleText = GetComponentInChildren<Text>();
    }

    private void OnEnable()
    {
        StopCoroutine(this.TitleBoxOffCoroutine());
        StartCoroutine(this.TitleBoxOffCoroutine());
    }

    private IEnumerator TitleBoxOffCoroutine()
    {
        yield return _wfs;

        this.gameObject.SetActive(false);
    }
}