﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;
using RenderHeads.Media.AVProVideo;     // 360 비디오 제어용

using UnityEngine.UI;

using UnityEngine.Video;

public class SafetyTrainingSceneManager : AutoCommander
{
    public static SafetyTrainingSceneManager main = null;
    
    //게임 콘텐츠 번호(1 생명, 2 화학, 3 전기 순)
    // (4.화학/생물), (5.화학/전기)
    // (6.전기/생물), (7.화학/전기/생물)
    public int contentIndex = 1;

    public Transform _scorePanel;
    public GameObject _scoreEffectObj;
    public GameObject _playerHand;
    public SimpleQuizManager _quizManager;
    public OXQuizManager _oxQuizManager;
    public GameObject _riskElementVote;
    public RiskElementVoteManager _voteManager;
    public GameObject _HiddenFinder;
    public GameObject _EquipmentFinder;

    public Image _blackBillboard;
    public GameObject _titleForHiddenFinder;
    public GameObject _infoMessageForHiddenFinder;

    public GameObject _titleForEquipmentFinder;
    public GameObject _infoMessageForEquipmentFinder;

    public GameObject _skipButtonObj;

    public Button[] _simpleQuizButtons;

    public GameObject[] _controlList;
    public int _controlListIndex = 0;

    public GameObject[] _controlArray;
    public int _controlArrayIndex = 0;

    private MediaPlayer _3Dvideo;
    private VideoPlayer _2Dvideo;
    private GameObject _content;

    public bool _isOXQuizStart = false;

    // OX 퀴즈 끝났는지 확인하는 플래그
    public bool _isOXQuizEnd = false;

    private bool _isHiddenFinderEnd = false;
    private bool _isEquipmentFinderEnd = false;
    private bool _isLabEscapeEnd = false;
    // 360 VR 동영상 파일 경로
    private const string _filePath = "/storage/emulated/0/Fantafo/360VRVideo/";
    private const string _filePathForPC = "D:/MyWork/Project/360VRVideo/";

    private double _curr2DVideoTime = 0.0;
    private Coroutine _cinemaDisableCo = null;

    public AudioSource[] _bgmLists;
    public AudioSource _clockTickBgm;
    //옵저버일시 사용되는 변수
    public GameObject _labExtinguishHand = null;

    // 스킵 기능용 플래그
    public bool _isSkip = false;

    public LabEscapeManager _labEscapeManager;

    public SummaryQuizManager _summaryQuizManager;

    public GameObject _infoMessageForMainCamera;

    private Coroutine _displayInfoMessageCo = null;

    private float _waitingTime = 0.0f;

    // 기본안전교육용 타이틀 이미지
    private GameObject _safetyTitleImageObj = null;
    
    private void Start()
    {
        main = this;

        // "다른 참가자를 기다리고 있습니다" 메시지 박스
        if (null == _infoMessageForMainCamera)
        {
            _infoMessageForMainCamera = GameObject.Find("InfoMessageForMainCamera");
        }

        if (null != _infoMessageForMainCamera)
        {
            _infoMessageForMainCamera.SetActive(false);
        }
        

        if (Application.platform == RuntimePlatform.Android)
        {
            if (null != RoomInfo.main)
            {
                contentIndex = RoomInfo.main._contentIndex;
            }
        }

#if FTF_OBSERVER
        _playerHand = GameObject.FindGameObjectWithTag("Hand");  
        _labExtinguishHand = GameObject.Find("exitinguish02");
        _labExtinguishHand.SetActive(false);    
#endif
        if (_HiddenFinder.activeSelf ||
            _EquipmentFinder.activeSelf)
        {
            _playerHand.SetActive(true);
        }
        else
        {
            _playerHand.SetActive(false);
        }

        string path = string.Empty;
        if (Application.platform == RuntimePlatform.Android)
        {
            path = _filePath;
        }
        else
        {
            path = _filePathForPC;
        }
        for (int i = 0; i < _controlList.Length; ++i)
        {
            if (_controlList[i] == null)
                continue;

            switch (_controlList[i].name)
            {
                case "Cinema_Ready_360": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "00_CNU_Readyroom_index.mp4"; } break;
                case "Introduce_360_01": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "01_MC1_1.mp4"; } break;
                case "Introduce_360_02": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "02_MC1_2_President_BS.mp4"; } break;
                case "Warm-upQuiz_PR_360_01": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "05_MC1_3_President.mp4"; } break;

                case "Warm-upQuiz_MC_A": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_1.mp4"; } break;
                case "Warm-upQuiz_MC_B": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_1.mp4"; } break;
                case "Warm-upQuiz_MC_C": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_1.mp4"; } break;

                case "Warm-upQuiz_Basic_A": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_Basic.mp4"; } break;
                case "Warm-upQuiz_Basic_B": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_Basic.mp4"; } break;
                case "Warm-upQuiz_Basic_C": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_Basic.mp4"; } break;

                case "Warm-upQuiz_360_02__A": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_Bio_Basic.mp4"; } break;
                case "Warm-upQuiz_360_02__B": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_Chem_Basic.mp4"; } break;
                case "Warm-upQuiz_360_02__C": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "08_MC2_Electric_Basic.mp4"; } break;

                case "LithumAccident_360": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "LithumAccident_360.mp4"; } break;
                case "Drama_360": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "09_CinematicVR.mp4"; } break;
                case "VRPrologue_360": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "RE_10_MC3.mp4"; } break;
                case "VRLab_PR_360__A": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "11_Lab_B_PR.mp4"; } break;
                case "VRLab_PR_360__B": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "13_Lab_C_PR.mp4"; } break;
                case "VRLab_PR_360__C": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "15_Lab_E_PR.mp4"; } break;
                case "VRLab_EP_360__A": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "12_Lab_B_EP.mp4"; } break;
                case "VRLab_EP_360__B": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "14_Lab_C_EP.mp4"; } break;
                case "VRLab_EP_360__C": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "16_Lab_E_EP.mp4"; } break;
                case "Summary_360": { _controlList[i].GetComponentInChildren<MediaPlayer>().m_VideoPath = path + "17_Summary.mp4"; } break;
                default:
                    break;
            }
        }

        

        switch (contentIndex)
        {
            // 생명, 화학(단일)
            case 1: case 2:
                {
                    _controlArray = new GameObject[28];
                    for (int i = 0, k = 0; i < _controlArray.Length; ++i)
                    {
                        //if (_controlList[k].name.Contains("Lithum"))
                        //{
                        //    ++k;
                        //}
                        _controlArray[i] = _controlList[k];

                        if ((k+1 < _controlList.Length) && (_controlList[k+1].name.Contains("__")|| _controlList[k + 1].name.Contains("MC")|| _controlList[k + 1].name.Contains("Basic")))
                        {
                            k += contentIndex;
                            ++i;
                            _controlArray[i] = _controlList[k];
                            if      (1 == contentIndex){    k += 3; }
                            else if (2 == contentIndex){    k += 2; }
                            else if (3 == contentIndex){    k += 1; }
                        }
                        else
                        {
                            ++k;
                        } 
                    }
                }
                break;
            // 전기
            case 3:
                {
                    _controlArray = new GameObject[28];
                    for (int i = 0, k = 0; i < _controlArray.Length; ++i)
                    {
                        _controlArray[i] = _controlList[k];

                        if ((k + 1 < _controlList.Length) && (_controlList[k + 1].name.Contains("__") || _controlList[k + 1].name.Contains("MC") || _controlList[k + 1].name.Contains("Basic")))
                        {
                            k += contentIndex;
                            ++i;
                            _controlArray[i] = _controlList[k];
                            if (1 == contentIndex) { k += 3; }
                            else if (2 == contentIndex) { k += 2; }
                            else if (3 == contentIndex) { k += 1; }
                        }
                        else
                        {
                            ++k;
                        }
                    }
                }
                break;
            case 4: // 화학/생물
                {
                    _controlArray = new GameObject[34];
                    for (int i = 0, k = 0; i < _controlArray.Length; ++i)
                    {
                        /*
                        if (_controlList[k].name.Contains("Lithum"))
                        {
                            ++k;
                        }
                        */
                        if (_controlList.Length < k || _controlList[k].name.Contains("Summary"))
                        {
                            break;
                        }

                        // _controlList 에서 _controlArray 에 차례대로 넣음
                        _controlArray[i] = _controlList[k];

                        // 다음 차례 _controlList가 실험실 별로 나뉘는 컨텐츠면(영상)
                        if ((k + 1 < _controlList.Length) && (_controlList[k + 1].name.Contains("__") || _controlList[k + 1].name.Contains("MC") || _controlList[k + 1].name.Contains("Basic")))
                        {
                            // 화학 실험실 컨텐츠로 세팅
                            k += 2;
                            ++i;
                            _controlArray[i] = _controlList[k];
                            k += 2;
                        }
                        else
                        {
                            ++k;
                        }
                    }

                    // 탈출(화학으로 세팅)
                    _controlArray[23].GetComponent<LabEscapeManager>()._curContentIndex = 1;

                    // 결과 퀴즈(화학)
                    int quizIdx = -1;
                    for (int i = 0; i < _controlList.Length; ++i)
                    {
                        if (_controlList[i].name == "Summary_Quiz")
                        {
                            quizIdx = i;
                            _controlArray[25] = _controlList[i];
                            _controlArray[25].GetComponent<SummaryQuizManager>()._curContentIndex = 1;
                            break;
                        }
                    }

                    // 기본안전수칙, 체험전 Prologue 영상, 탈출체험, 예방영상, Summary, 생물퀴즈, 결과
                    _controlArray[26] = _controlList[8];   // 기본안전수칙(생물)MC
                    _controlArray[27] = _controlList[16];   // 기본안전수칙(생물)Lab
                    //_controlArray[27] = _controlList[20];   // 리튬 영상 추가.
                    _controlArray[28] = _controlList[28];   // Prologue 영상(생물)
                    _controlArray[29] = Instantiate(_controlList[31]);   // 탈출
                    _controlArray[29].GetComponent<LabEscapeManager>()._curContentIndex = 0;    // 탈출(생물로 세팅)
                    _controlArray[29].name = _controlList[31].name;      // 탈출 컨텐츠 이름을 LabEscape (clone) 에서 LabEscape 로 변경
                    _controlArray[30] = _controlList[32];   // 예방영상(생물)
                    _controlArray[31] = Instantiate(_controlList[quizIdx]);   // 퀴즈
                    _controlArray[31].GetComponent<SummaryQuizManager>()._curContentIndex = 0;  // 퀴즈(생물로 세팅)
                    _controlArray[31].name = _controlList[quizIdx].name;
                    _controlArray[32] = _controlList[35];   // Summary영상
                    _controlArray[33] = _controlList[37];   // 종료
                }
                break;
            case 5: // 화학/전기
                {
                    //_controlArray = new GameObject[23];
                    _controlArray = new GameObject[34];
                    for (int i = 0, k = 0; i < _controlArray.Length; ++i)
                    {
                        /*
                        if (_controlList[k].name.Contains("Lithum"))
                        {
                            ++k;
                        }
                        */

                        if (_controlList.Length < k || _controlList[k].name.Contains("Summary"))
                        {
                            break;
                        }

                        _controlArray[i] = _controlList[k];

                        if ((k + 1 < _controlList.Length) && (_controlList[k + 1].name.Contains("__") || _controlList[k + 1].name.Contains("MC") || _controlList[k + 1].name.Contains("Basic")))
                        {
                            k += 2;
                            ++i;
                            _controlArray[i] = _controlList[k];
                            k += 2;
                        }
                        else
                        {
                            ++k;
                        }
                    }

                    // 탈출(화학으로 세팅)
                    _controlArray[23].GetComponent<LabEscapeManager>()._curContentIndex = 1;

                    // 결과 퀴즈(화학)
                    int quizIdx = -1;
                    for (int i = 0; i < _controlList.Length; ++i)
                    {
                        if (_controlList[i].name == "Summary_Quiz")
                        {
                            quizIdx = i;
                            _controlArray[25] = _controlList[i];
                            _controlArray[25].GetComponent<SummaryQuizManager>()._curContentIndex = 1;
                            break;
                        }                        
                    }

                    // 기본안전수칙, 체험전 Prologue 영상, 탈출체험, 예방영상, Summary, 전기 퀴즈, 결과
                    _controlArray[26] = _controlList[10];   // 기본안전수칙(전기)MC
                    _controlArray[27] = _controlList[18];   // 기본안전수칙(전기)Lab
                    //_controlArray[27] = _controlList[20];   // 리튬 영상 추가.
                    _controlArray[28] = _controlList[30];   // Prologue 영상(전기)
                    _controlArray[29] = Instantiate(_controlList[31]);   // 탈출
                    _controlArray[29].GetComponent<LabEscapeManager>()._curContentIndex = 2;    // 탈출(전기로 세팅)
                    _controlArray[29].name = _controlList[31].name;      // 탈출 컨텐츠 이름을 LabEscape (clone) 에서 LabEscape 로 변경
                    _controlArray[30] = _controlList[34];   // 예방영상(전기)
                    _controlArray[31] = Instantiate(_controlList[quizIdx]);   // 퀴즈
                    _controlArray[31].GetComponent<SummaryQuizManager>()._curContentIndex = 2;  // 퀴즈(전기로 세팅)
                    _controlArray[31].name = _controlList[quizIdx].name;
                    _controlArray[32] = _controlList[35];   // Summary영상
                    _controlArray[33] = _controlList[37];   // 종료

                }
                break;
            case 6: // 전기/생물
                {
                    _controlArray = new GameObject[34];
                    for (int i = 0, k = 0; i < _controlArray.Length; ++i)
                    {                        
                        if (_controlList.Length < k || _controlList[k].name.Contains("Summary"))
                        {
                            break;
                        }

                        // _controlList 에서 _controlArray 에 차례대로 넣음
                        _controlArray[i] = _controlList[k];

                        // 다음 차례 _controlList가 실험실 별로 나뉘는 컨텐츠면(영상)
                        if ((k + 1 < _controlList.Length) && (_controlList[k + 1].name.Contains("__") || _controlList[k + 1].name.Contains("MC") || _controlList[k + 1].name.Contains("Basic")))
                        {
                            // 전기 실험실 컨텐츠로 세팅
                            k += 3;
                            ++i;
                            _controlArray[i] = _controlList[k];
                            k += 1;
                        }
                        else
                        {
                            ++k;
                        }
                    }

                    // 탈출(전기로 세팅)
                    _controlArray[23].GetComponent<LabEscapeManager>()._curContentIndex = 2;

                    // 결과 퀴즈(전기)
                    int quizIdx = -1;
                    for (int i = 0; i < _controlList.Length; ++i)
                    {
                        if (_controlList[i].name == "Summary_Quiz")
                        {
                            quizIdx = i;
                            _controlArray[25] = _controlList[i];
                            _controlArray[25].GetComponent<SummaryQuizManager>()._curContentIndex = 2;
                            break;
                        }
                    }

                    // 기본안전수칙, 체험전 Prologue 영상, 탈출체험, 예방영상, Summary, 생물퀴즈, 결과
                    _controlArray[26] = _controlList[8];   // 기본안전수칙(생물)MC
                    _controlArray[27] = _controlList[16];   // 기본안전수칙(생물)Lab                    
                    _controlArray[28] = _controlList[28];   // Prologue 영상(생물)
                    _controlArray[29] = Instantiate(_controlList[31]);   // 탈출
                    _controlArray[29].GetComponent<LabEscapeManager>()._curContentIndex = 0;    // 탈출(생물로 세팅)
                    _controlArray[29].name = _controlList[31].name;      // 탈출 컨텐츠 이름을 LabEscape (clone) 에서 LabEscape 로 변경
                    _controlArray[30] = _controlList[32];   // 예방영상(생물)
                    _controlArray[31] = Instantiate(_controlList[quizIdx]);   // 퀴즈
                    _controlArray[31].GetComponent<SummaryQuizManager>()._curContentIndex = 0;  // 퀴즈(생물로 세팅)
                    _controlArray[31].name = _controlList[quizIdx].name;
                    _controlArray[32] = _controlList[35];   // Summary영상
                    _controlArray[33] = _controlList[37];   // 종료
                }
                break;
            case 7: // 화학/전기/생물
                {
                    _controlArray = new GameObject[40];
                    for (int i = 0, k = 0; i < _controlArray.Length; ++i)
                    {
                        /*
                        if (_controlList[k].name.Contains("Lithum"))
                        {
                            ++k;
                        }
                        */
                        if (_controlList.Length < k || _controlList[k].name.Contains("Summary"))
                        {
                            break;
                        }

                        _controlArray[i] = _controlList[k];

                        if ((k + 1 < _controlList.Length) && (_controlList[k + 1].name.Contains("__") || _controlList[k + 1].name.Contains("MC") || _controlList[k + 1].name.Contains("Basic")))
                        {
                            k += 2;
                            ++i;
                            _controlArray[i] = _controlList[k];
                            k += 2;
                        }
                        else
                        {
                            ++k;
                        }
                    }

                    // 탈출(화학으로 세팅)
                    _controlArray[23].GetComponent<LabEscapeManager>()._curContentIndex = 1;

                    // 결과 퀴즈(화학)
                    int quizIdx = -1;
                    for (int i = 0; i < _controlList.Length; ++i)
                    {
                        if (_controlList[i].name == "Summary_Quiz")
                        {
                            quizIdx = i;
                            _controlArray[25] = _controlList[i];
                            _controlArray[25].GetComponent<SummaryQuizManager>()._curContentIndex = 1;
                            break;
                        }
                    }

                    // 기본안전수칙, 체험전 Prologue 영상, 탈출체험, 예방영상, 전기 퀴즈
                    _controlArray[26] = _controlList[10];   // 기본안전수칙(전기)MC
                    _controlArray[27] = _controlList[18];   // 기본안전수칙(전기)Lab
                    //_controlArray[27] = _controlList[20];   // 리튬 영상 추가.
                    _controlArray[28] = _controlList[30];   // Prologue 영상(전기)
                    _controlArray[29] = Instantiate(_controlList[31]);   // 탈출
                    _controlArray[29].GetComponent<LabEscapeManager>()._curContentIndex = 2;    // 탈출(전기로 세팅)
                    _controlArray[29].name = _controlList[31].name;      // 탈출 컨텐츠 이름을 LabEscape (clone) 에서 LabEscape 로 변경
                    _controlArray[30] = _controlList[34];   // 예방영상(전기)
                    _controlArray[31] = Instantiate(_controlList[quizIdx]);   // 퀴즈
                    _controlArray[31].GetComponent<SummaryQuizManager>()._curContentIndex = 2;  // 퀴즈(전기로 세팅)
                    _controlArray[31].name = _controlList[quizIdx].name;

                    //_controlArray[21] = _controlList[10];   // 기본안전수칙(전기)
                    //_controlArray[22] = _controlList[21];   // Prologue 영상(전기)
                    //_controlArray[23] = Instantiate(_controlList[22]);   // 탈출
                    //_controlArray[23].GetComponent<LabEscapeManager>()._curContentIndex = 2;    // 탈출(전기로 세팅)
                    //_controlArray[23].name = _controlList[22].name;
                    //_controlArray[24] = _controlList[25];   // 예방영상(전기)
                    //_controlArray[25] = Instantiate(_controlList[quizIdx]);   // 퀴즈
                    //_controlArray[25].GetComponent<SummaryQuizManager>()._curContentIndex = 2;  // 퀴즈(전기로 세팅)
                    //_controlArray[25].name = _controlList[quizIdx].name;



                    // 기본안전수칙, 체험전 Prologue 영상, 탈출체험, 예방영상, Summary 영상, 생물 퀴즈
                    _controlArray[32] = _controlList[8];   // 기본안전수칙(생물)
                    _controlArray[33] = _controlList[16];   // 기본안전수칙(생물)
                    _controlArray[34] = _controlList[28];   // Prologue 영상(생물)
                    _controlArray[35] = Instantiate(_controlList[31]);   // 탈출
                    _controlArray[35].GetComponent<LabEscapeManager>()._curContentIndex = 0;    // 탈출(생물로 세팅)
                    _controlArray[35].name = _controlList[31].name;      // 탈출 컨텐츠 이름을 LabEscape (clone) 에서 LabEscape 로 변경
                    _controlArray[36] = _controlList[32];   // 예방영상(생물)
                    _controlArray[37] = Instantiate(_controlList[quizIdx]);   // 퀴즈
                    _controlArray[37].GetComponent<SummaryQuizManager>()._curContentIndex = 0;  // 퀴즈(생물로 세팅)
                    _controlArray[37].name = _controlList[quizIdx].name;
                    _controlArray[38] = _controlList[35];   // Summary영상
                    _controlArray[39] = _controlList[37];   // 종료
                }
                break;
            
        }

        

        for (int i = 0, j = 0; i < _controlList.Length; ++i)
        {
            for (j = 0; j < _controlArray.Length; ++j)
            {
                if (_controlList[i] == _controlArray[j])
                {
                    break;
                }
            }
            if (j >= _controlArray.Length)
            {
                DestroyImmediate(_controlList[i]);
                Resources.UnloadUnusedAssets();
            }
        }
                
        _controlListIndex = -1;
        _controlArrayIndex = -1;

        //PlayerInstance.main이 준비된 후 할 동작들...
        StartCoroutine(this.WaitingCoroutine());
    }

    ///////////////////////////////////
    // 윤규씨 코드
    ///////////////////////////////////
    private IEnumerator WaitingCoroutine()
    {
        WaitForSeconds wait = new WaitForSeconds(0.1f);

        while (null == PlayerInstance.main)
        {
            yield return wait;
        }

        MediaPlayer mediaPlayer = _controlArray[0].GetComponentInChildren<MediaPlayer>();
        WaitForSeconds wfs = new WaitForSeconds(1.0f);
        if (null != mediaPlayer)
        {
            bool canPlay = false;
            // 동영상 재생 가능할 때 까지 대기
            while (false == canPlay)
            {
                // 1초 대기
                yield return wfs;

                // 영상 재생 가능한지 플래그 확인
                canPlay = mediaPlayer.Control.CanPlay();                
            }            
        }
        
        
        // 동영상 재생이 가능하면
        //if (true == canPlay)
        {
            // 서버에게 시작 패킷 보냄
            Networker.Send(C_Safety.Start());   // 첫 번째 영상(극장) 로딩이 끝나면 패킷을 보내도록 하기
        }
        

        // 방장일 경우 영상시작 패킷 전달
        /*
        if (PlayerInstance.main.IsRoomMaster)
        {
            BroadcastPlayContent(_controlArrayIndex);
        }
        // 방장이 아닐 경우 스킵 버튼 비활성화
        else
        {
            _skipButtonObj.SetActive(false);
        }
        */
        yield return null;
    }

    private void Update()
    {
        
        //서버로 받은 방의 재생인덱스보다 적다면 해당 인덱스 실행
        if (_controlArrayIndex < RoomInfo.main.mapStage)
        {
            _controlArrayIndex = RoomInfo.main.mapStage;
            Play(_controlArray[_controlArrayIndex]);
            return;
        }

        if (null == PlayerInstance.main)
        {
            Debug.Log("==========PlayerInstance.main is null");
            return;
        }
        if (null == _content)
        {
            Debug.Log("==========_content is null");
            return;
        }

        switch (_content.tag)
        {
            case "MediaPlayer":
                {
                    if (null != _3Dvideo)
                    {
                        // 영상이 끝나지 않았고 재생되고 있지 않으면
                        if( false == _3Dvideo.Control.IsFinished() &&
                            false == _3Dvideo.Control.IsPlaying() )
                        {
                            // '다른 참가자를 기다리고 있습니다' 메시지가 활성화 되어 있지 않으면
                            if (false == _infoMessageForMainCamera.activeSelf)
                            {
                                // 메시지 활성까지 대기하는 시간 증가
                                _waitingTime += Time.deltaTime;

                                // 대기한 시간이 3초 이상이면
                                if (3.0f <= _waitingTime)
                                {
                                    // 메시지 박스 활성화
                                    _infoMessageForMainCamera.SetActive(true);
                                }
                            }                            
                        }
                        // 영상이 재생중이면
                        else if (true == _3Dvideo.Control.IsPlaying())
                        {
                            // '다른 참가자를 기다리고 있습니다' 메시지가 활성화되어 있으면
                            if (true == _infoMessageForMainCamera.activeSelf)
                            {
                                // 메시지 활성까지 대기한 시간 초기화
                                _waitingTime = 0.0f;

                                // 메시지 박스 비활성화
                                _infoMessageForMainCamera.SetActive(false);
                            }

                            // 현재 재생중인 영상이 기본안전수칙이면
                            if (_content.name.Contains("Warm-upQuiz_360_02"))
                            {
                                // 기본안전수칙 타이틀이 비활성화 되어 있으면
                                if (_safetyTitleImageObj != null && false == _safetyTitleImageObj.activeSelf)
                                {
                                    // 현재 재생중인 비디오의 재생시간이 12.783초 이상이면
                                    if (_3Dvideo.Control.GetCurrentTimeMs() >= 12783.0f)
                                    {
                                        // 기본안전수칙 타이틀 활성화
                                        _safetyTitleImageObj.SetActive(true);
                                    }
                                }                                
                            }
                        }
                    }

                    if ((_3Dvideo != null && _3Dvideo.Control != null && _3Dvideo.Control.IsFinished()) ||
                        true == _isSkip)
                    {
                       


                        float durationMs = _3Dvideo.Info.GetDurationMs();   // 영상 길이
                        //bool videoFinished = _3Dvideo.Control.IsFinished(); // 영상 종료 여부
                        int ms = 0;
                        if (durationMs < 60000.0f) { ms = 3000; }
                        else if (60000.0f <= durationMs) { ms = (int)(durationMs * 0.05f); }
                        Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, ms, _isSkip));
                        Debug.Log("MediaPlayer._controlArrayIndex : " + _controlArrayIndex);
                        
                        // 다른 참가자를 기다리고 있습니다
                        if (null == _displayInfoMessageCo)
                        {
                            _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                        }

                        _content = null;
                        _3Dvideo = null;
                        _isSkip = false;
                    }
                    else
                    {
                        return;
                    }
                    break;
                }            
            case "Game":
                {
                    if (_content.name.Contains("Warm-up_Quiz"))
                    {
                        if (_quizManager._isFinished || true == _isSkip)
                        {
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000,_isSkip));
                            _content = null;
                            _isSkip = false;

                            _clockTickBgm.Stop();

                            // 여기에 영상 멈춤 추가....
                            

                            Debug.Log("Game.Warm-up_Quiz._controlArrayIndex : " + _controlArrayIndex);
                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }
                    else if (_content.name.Contains("Warm-up_OXQuiz"))
                    {
                        if (_oxQuizManager._isFinished || true == _isSkip)
                        {
                            if (_content.name.Contains("OXQuiz_3"))
                            {
                                _isOXQuizStart = false;
                                _isOXQuizEnd = true;
                            }
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000, _isSkip));
                            _content = null;
                            _isSkip = false;

                            
                            _clockTickBgm.Stop();

                            Debug.Log("Game.Warm-up_OXQuiz._controlArrayIndex : " + _controlArrayIndex);
                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }
                    else if (_content.name == "Poll")
                    {
                        if (_voteManager._isFinished || true == _isSkip)
                        {
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000, _isSkip));
                            _content = null;
                            _isSkip = false;
                            Debug.Log("Game.Poll._controlArrayIndex : " + _controlArrayIndex);

                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }
                    else if (_content.name == "Page8")
                    {
                        if (false == _playerHand.activeSelf)
                        {
                            _playerHand.SetActive(true);
                        }
                        if (_isHiddenFinderEnd || true == _isSkip)
                        {
                            Debug.Log("-----HiddenFinderEnd");
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000, _isSkip));
                            _content = null;
                            _isSkip = false;
                            _playerHand.SetActive(false);
                            
                            Debug.Log("Game.HiddenFinder._controlArrayIndex : " + _controlArrayIndex);
                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }
                    else if (_content.name == "Page9")
                    {
                        if (false == _playerHand.activeSelf)
                        {
                            _playerHand.SetActive(true);
                        }
                        if (_isEquipmentFinderEnd || true == _isSkip)
                        {
                            Debug.Log("EquipmentFinderEnd");
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000, _isSkip));
                            _content = null;
                            _isSkip = false;
                            _playerHand.SetActive(false);
                            
                            Debug.Log("Game.EquipmentFinder._controlArrayIndex : " + _controlArrayIndex);
                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }
                    else if (_content.name == "LabEscape")
                    {
                        //if (_isLabEscapeEnd || true == _isSkip)
                        if (_labEscapeManager._isFinished || true == _isSkip)
                        {
                            //_isLabEscapeEnd = false;
                            //_labEscapeManager = null;
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000, _isSkip));
                            _content = null;
                            _isSkip = false;
                            
                            Debug.Log("Game.LabEscape._controlArrayIndex : " + _controlArrayIndex);
                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }
                    else if (_content.name == "Summary_Quiz")
                    {
                        if (_summaryQuizManager._isFinished || true == _isSkip)
                        {
                            Networker.Send(C_Safety.ContentsEnd((short)_controlArrayIndex, 3000, _isSkip));
                            _content = null;
                            _isSkip = false;
                            _isOXQuizEnd = true;
                            _clockTickBgm.Stop();
                            Debug.Log("Game.Summary_Quiz._controlArrayIndex : " + _controlArrayIndex);
                            // 다른 참가자를 기다리고 있습니다
                            if (null == _displayInfoMessageCo)
                            {
                                _displayInfoMessageCo = StartCoroutine(this.DisplayInfoMessageCoroutine());
                            }
                            return;
                        }
                    }

                    break;                    
                }
        }
    }

    private IEnumerator DisplayInfoMessageCoroutine()
    {
        yield return new WaitForSeconds(3.0f);

        _infoMessageForMainCamera.SetActive(true);
    }
   
    //해당 함수는 선생님일때만 호출된다.
    public void BroadcastPlayContent(int idx)
    {
        //서버에 재생정보 업데이트
        Networker.Send(C_Room.MapStage((short)idx));
        Broadcast("PlayContent", idx);
    }

    [OnCommand]
    public void PlayContent(int idx)
    {
        Debug.Log("PlayContent" + idx);

        if (_controlArray.Length > idx)
        {
            _controlArrayIndex = idx;
            Play(_controlArray[idx]);
        }
    }
    public void Play(GameObject video)
    {
        // 현재 무엇을 진행하는지 저장하기 위해
        _content = video;

        // 다른 참가자를 기다리고 있습니다
        _infoMessageForMainCamera.SetActive(false);

        if (null != _displayInfoMessageCo)
        {
            StopCoroutine(_displayInfoMessageCo);
            _displayInfoMessageCo = null;
        }

        ////ox퀴즈 초기화
        //_isOXQuizEnd = true;
        //_isOXQuizStart = false;

        // tick tock 소리 비활성화 
        _clockTickBgm.Stop();

        if (!video.activeSelf)
        {
            video.SetActive(true);
        }

        // BGM 정지
        for (int i = 0; i < _bgmLists.Length; ++i)
        {
            _bgmLists[i].Stop();
        }

        // BGM 재생
        if (video.name == "Cinema_Example")
        {
            // CNU_BGM_ReadyRoom 재생
            _bgmLists[0].Play();
        }
        else if (video.name == "ResultPage")
        {
            // CNU_BGM_End 재생
            _bgmLists[1].Play();
        }

        switch (video.tag)
        {
            case "MediaPlayer":
                {
                    //모든 것 비활성화
                    for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                    {
                        if (-1 < i && _controlArray[i])
                        {
                            // 삭제
                            DestroyImmediate(_controlArray[i]);
                            Resources.UnloadUnusedAssets();
                        }
                    }
                    _playerHand.SetActive(false);
                    _3Dvideo = video.GetComponentInChildren<MediaPlayer>();
                    _2Dvideo = null;

                    // 비디오 재생
                    //_3Dvideo.Play();

                    // 현재 재생해야 할 영상이 기본안전수칙이면
                    if (_content.name.Contains("Warm-upQuiz_360_02"))
                    {
                        // 기본안전수칙 영상에 붙어있는 타이틀 이미지 객체 참조
                        //_safetyTitleImageObj = _content.GetComponentInChildren<Image>().gameObject;

                        if (null != _safetyTitleImageObj)
                        {
                            // 타이틀 이미지 객체 비활성화
                            _safetyTitleImageObj.SetActive(false);
                        }                        
                    }

                    break;
                }
            case "VideoPlayer":
                {
                    _playerHand.SetActive(false);
                    _2Dvideo = video.GetComponentInChildren<VideoPlayer>();
                    break;
                }
            case "Game":
                {
                    switch (_controlArray[_controlArrayIndex].name)
                    {
                        case "Warm-up_OXQuiz_1":
                            {
                                _isOXQuizStart = true;
                            }
                            break;
                        case "Warm-up_OXQuiz_2":
                        case "Warm-up_OXQuiz_3":
                            {
                                _oxQuizManager = _content.GetComponent<OXQuizManager>();

                                // 삭제
                                DestroyImmediate(_controlArray[_controlArrayIndex - 1]);
                                Resources.UnloadUnusedAssets();

                                StartCoroutine(this.QuizActiveDelayCoroutine(_content));
                            }
                            break;

                        case "Poll":
                            {
                                for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                                {
                                    if (-1 < i && _controlArray[i])
                                    {
                                        // 삭제
                                        DestroyImmediate(_controlArray[i]);
                                        Resources.UnloadUnusedAssets();
                                    }
                                }
                                break;
                            }
                        case "Page8":
                        case "Page9":
                            {
                                for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                                {
                                    if (-1 < i && _controlArray[i])
                                    {
                                        // 삭제
                                        DestroyImmediate(_controlArray[i]);
                                        Resources.UnloadUnusedAssets();
                                    }
                                }

                                if (false == _playerHand.activeSelf)
                                {
                                    _playerHand.SetActive(true);
                                }

                                break;
                            }
                        case "LabEscape":
                            {
                                _labEscapeManager = _controlArray[_controlArrayIndex].GetComponent<LabEscapeManager>();
                                for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                                {
                                    if (-1 < i && _controlArray[i])
                                    {
                                        // 삭제
                                        DestroyImmediate(_controlArray[i]);
                                        Resources.UnloadUnusedAssets();
                                    }
                                }
                                break;
                            }
                        case "Summary_Quiz":
                            {
                                _summaryQuizManager = _content.GetComponent<SummaryQuizManager>();

                                // OX 퀴즈 시작
                                _isOXQuizStart = true;
                                _isOXQuizEnd = false;

                                for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                                {
                                    if (-1 < i && _controlArray[i])
                                    {
                                        // 삭제
                                        DestroyImmediate(_controlArray[i]);
                                        Resources.UnloadUnusedAssets();
                                    }
                                }
                            }
                            break;
                        case "ResultPage":
                            {
                                for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                                {
                                    if (-1 < i && _controlArray[i])
                                    {
                                        // 삭제
                                        DestroyImmediate(_controlArray[i]);
                                        Resources.UnloadUnusedAssets();
                                    }
                                }

                                _isOXQuizStart = false;

                                // 모든 콘텐츠가 종료시, 해당 함수를 호출.
                                Networker.Send(C_Room.RoomEnd());
                                break;
                            }
                            
                        default:
                            {
                                for (int i = _controlArrayIndex - 1; 0 <= i; --i)
                                {
                                    if (-1 < i && _controlArray[i])
                                    {
                                        // 삭제
                                        DestroyImmediate(_controlArray[i]);
                                        Resources.UnloadUnusedAssets();
                                    }
                                }
                                break;
                            }
                    }
                    break;
                }
        }
    }

    public void PlayMediaPlayerVideo()
    {
        if (null != _3Dvideo)
        {
            _3Dvideo.Play();
            Debug.Log("====== 3DVideo.Play() ======");

        }
        else
        {
            Debug.Log("====== 3DVideo is null ======");
        }
    }

    public void BroadcastChangeFinder()
    {
        Broadcast("ChangeFinder");
    }

    [OnCommand]
    public void ChangeFinder()
    {
        if (_isHiddenFinderEnd == false)
        {
            _isHiddenFinderEnd = true;

            _HiddenFinder.SetActive(false);
            _EquipmentFinder.SetActive(true);

            StartCoroutine(this.EquipmentFinderStartCoroutine());
        }
        else
        {
            _isEquipmentFinderEnd = true;
            _playerHand.SetActive(false);
            //if (PlayerInstance.main.IsRoomMaster)
            {
                BroadcastCoinFinderEnd();
            }
        }
    }

    public void BroadcastHiddenFinderEnd()
    {
        Broadcast("HiddenFinderEnd");
    }
    [OnCommand]
    public void HiddenFinderEnd()
    {
        _HiddenFinder.SetActive(false);
        _blackBillboard.gameObject.SetActive(false);
        _playerHand.SetActive(false);
        _isHiddenFinderEnd = true;
    }

    public void BroadcastCoinFinderEnd()
    {
        Broadcast("CoinFinderEnd");
    }
    [OnCommand]
    public void CoinFinderEnd()
    {
        _EquipmentFinder.SetActive(false);
        _blackBillboard.gameObject.SetActive(false);
        _playerHand.SetActive(false);
        _isEquipmentFinderEnd = true;
    }

    public void BroadcastLabEscapeEnd()
    {
        Broadcast("LabEscapeEnd");
    }
    [OnCommand]
    public void LabEscapeEnd()
    {
        //_isLabEscapeEnd = true;
        _labEscapeManager._isFinished = true;
    }

    private IEnumerator EquipmentFinderStartCoroutine()
    {
        Color billboardColor = _blackBillboard.color;
        billboardColor.a = 1.0f;
        _blackBillboard.color = billboardColor;

        WaitForSeconds delayBeforeFinder = new WaitForSeconds(5.0f);
        WaitForSeconds titleShowDelay = new WaitForSeconds(3.0f);
        WaitForSeconds d = new WaitForSeconds(0.1f);
        yield return delayBeforeFinder;


        while (0.0f < _blackBillboard.color.a)
        {
            billboardColor = _blackBillboard.color;
            billboardColor.a -= 0.05f;
            _blackBillboard.color = billboardColor;
            yield return d;
        }

    }

    public void PlayScoreEffect()
    {
        GameObject obj = Instantiate(_scoreEffectObj);
        obj.transform.parent = _scorePanel;
        obj.transform.localPosition = new Vector3(-66.5f, -137.5f, -281.0f);
        obj.transform.localRotation = Quaternion.identity;
        obj.transform.localScale = Vector3.one;
        Destroy(obj, 1.0f);
    }

    public void OnClickSkipButton()
    {
        if (null != PlayerInstance.main)
        {
            if (PlayerInstance.main.IsRoomMaster)
            {
                Networker.Send(C_Room.MapStage((short)(_controlArrayIndex + 1)));
            }
        }
    }

    public int GetContentIndex()
    {
        return contentIndex;
    }

    private IEnumerator QuizActiveDelayCoroutine(GameObject quizContent)
    {
        quizContent.SetActive(false);
        Debug.Log("quizContent : " + quizContent);
        yield return new WaitForSeconds(0.5f);
        quizContent.SetActive(true);
        Debug.Log("quizContent : " + quizContent);  
    }
}