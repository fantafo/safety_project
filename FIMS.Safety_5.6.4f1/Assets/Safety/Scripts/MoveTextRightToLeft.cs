﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTextRightToLeft : MonoBehaviour
{
    private RectTransform _rectTr;
    private Vector3 _originPos = Vector3.zero;
    private Vector3 _currPos = Vector3.zero;

    private void Awake()
    {
        _rectTr = this.gameObject.GetComponent<RectTransform>();
        _originPos = _rectTr.localPosition;
    }

    private void OnEnable()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", _originPos.x,
            "to", _originPos.x - 35.0f,
            "time", 0.8f,
            "easetype", iTween.EaseType.easeOutQuad,
            "delay", 0.5f,
            "onupdate", "MoveFromRightToLeft"));
    }

    public void MoveFromRightToLeft(float xPos)
    {
        _currPos = _rectTr.localPosition;
        _currPos.x = xPos;
        _rectTr.localPosition = _currPos;
    }
}