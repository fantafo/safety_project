﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Video;

public class CinemaNarrationDrama : MonoBehaviour {

    public GameObject[] _narration;
    public GameObject _mainCon;
    SafetyTrainingSceneManager safetyManager;

    private AudioSource _audio;
    private VideoPlayer _video;
    private int _narIndex = 0;
    private Vector3 _myScale;

    // Use this for initialization
    void Awake()
    {
        if (_mainCon != null)
        {
            _myScale = _mainCon.transform.localScale;
            _mainCon.transform.localScale = new Vector3(0, 0, 0);
            _video = _mainCon.GetComponent<VideoPlayer>();
        }

        _audio = _narration[_narIndex].GetComponentInChildren<AudioSource>();
        _narration[_narIndex].SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_audio != null && !_audio.isPlaying )
        {
            if (_narIndex < _narration.Length)
            {   
                _narration[_narIndex].SetActive(false);
                _narIndex++;
                _audio = _narration[_narIndex].GetComponentInChildren<AudioSource>();
                _narration[_narIndex].SetActive(true);
            }
            else
            {
                _audio = null;
                if (_mainCon != null)
                {
                    _mainCon.transform.localScale = _myScale;
                    _video.Play();
                }
            }
        }
    }
}
