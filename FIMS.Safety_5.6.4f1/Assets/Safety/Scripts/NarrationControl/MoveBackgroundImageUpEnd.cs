﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackgroundImageUpEnd : MonoBehaviour
{
    private RectTransform _rectTr;
    private Vector3 _originPos = Vector3.zero;
    private Vector3 _currPos = Vector3.zero;

    private void Awake()
    {
        _rectTr = this.gameObject.GetComponent<RectTransform>();
        _originPos = _rectTr.localPosition;
    }

    private void OnEnable()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", -100.0f,
            //"to", -35.0f,
            "to", -15.0f,
            "time", 0.8f,
            "easetype", iTween.EaseType.easeOutQuad,
            "delay", 0.0f,
            "onupdate", "MoveFromDownToUp"));

        iTween.ValueTo(gameObject, iTween.Hash(
            //"from", -35.0f,
            "from", -15.0f,
            "to", -100.0f,
            "time", 0.2f,
            "eastype", iTween.EaseType.easeInQuad,
            "delay", 7.0f,
            "onupdate", "ReturnToOriginPos"));
    }

    public void MoveFromDownToUp(float yPos)
    {
        _currPos = _rectTr.localPosition;
        _currPos.y = yPos;
        _rectTr.localPosition = _currPos;
    }

    public void ReturnToOriginPos(float yPos)
    {
        _currPos = _rectTr.localPosition;
        _currPos.y = yPos;
        _rectTr.localPosition = _currPos;
    }
}
