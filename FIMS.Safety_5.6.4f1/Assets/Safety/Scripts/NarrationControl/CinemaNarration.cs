﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Video;

public class CinemaNarration : MonoBehaviour {

    public GameObject[] _narration;
    public GameObject _mainCon;
    SafetyTrainingSceneManager safetyManager;

    private AudioSource _audio;
    private VideoPlayer _video;
    private int _narIndex = 0;
    private Vector3 _myScale;

    private Coroutine _co = null;

    // Use this for initialization
    void Awake()
    {
        /*
        if (_mainCon != null)
        {
            _myScale = _mainCon.transform.localScale;
            _mainCon.transform.localScale = new Vector3(0, 0, 0);
            _video = _mainCon.GetComponent<VideoPlayer>();
        }
        */

        _audio = _narration[_narIndex].GetComponentInChildren<AudioSource>();
        _narration[_narIndex].SetActive(true);

        /////////////////////////////////////////
        // 조진환 수정(2018-08-06 1645)
        /////////////////////////////////////////
        if (null != _co)
        {
            StopCoroutine(_co);
        }
        _co = StartCoroutine(this.UpdateCoroutine());
    }

    /////////////////////////////////////////
    // 조진환 수정(2018-08-06 1645)
    /////////////////////////////////////////
    private IEnumerator UpdateCoroutine()
    {
        WaitForSeconds wfs = new WaitForSeconds(0.1f);

        while (true)
        {
            if (_audio != null && !_audio.isPlaying)
            {
                if (_narIndex < _narration.Length)
                {
                    _narration[_narIndex].SetActive(false);
                    _narIndex++;
                    /////////////////////////////////////////
                    // 조진환 수정(2018-08-06 1645)
                    /////////////////////////////////////////
                    if (_narIndex > _narration.Length - 1)
                    {
                        yield break;
                    }
                    /////////////////////////////////////////
                    _audio = _narration[_narIndex].GetComponentInChildren<AudioSource>();
                    _narration[_narIndex].SetActive(true);
                }
                else
                {
                    _audio = null;
                    if (_mainCon != null)
                    {
                        _mainCon.transform.localScale = _myScale;
                        _video.Play();
                    }
                }
            }

            yield return wfs;
        }
    }

    /////////////////////////////////////////
    // 조진환 수정(2018-08-06 1645)
    /////////////////////////////////////////
    /*
    // Update is called once per frame
    void Update()
    {
        if (_audio != null && !_audio.isPlaying )
        {
            if (_narIndex < _narration.Length)
            {   
                _narration[_narIndex].SetActive(false);
                _narIndex++;
                _audio = _narration[_narIndex].GetComponentInChildren<AudioSource>();
                _narration[_narIndex].SetActive(true);
            }
            else
            {
                _audio = null;
                if (_mainCon != null)
                {
                    _mainCon.transform.localScale = _myScale;
                    _video.Play();
                }
            }
        }
    }
    */
}