﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class FadeInText : MonoBehaviour
{
    private Text _text;
    private Color _color = Color.white;

    private void Awake()
    {
        _text = this.gameObject.GetComponent<Text>();
        _color.a = 0.0f;
        _text.color = _color;
    }

    private void OnEnable()
    {
        _color = _text.color;
        _color.a = 0.0f;
        _text.color = _color;
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", 0.0f,
            "to", 1.0f,
            "time", 0.1f,
            "easetype", iTween.EaseType.linear,
            "delay", 0.8f,
            "onupdate", "SetTextAlpha"));

        iTween.ValueTo(gameObject, iTween.Hash(
            "from", 1.0f,
            "to", 0.0f,
            "time", 0.1f,
            "easetype", iTween.EaseType.linear,
            "delay", 5.0f,
            "onupdate", "SetTextAlpha"));
    }

    public void SetTextAlpha(float alphaValue)
    {
        _color = _text.color;
        _color.a = alphaValue;
        _text.color = _color;
    }
}