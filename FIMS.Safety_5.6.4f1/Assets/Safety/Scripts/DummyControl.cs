﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RenderHeads.Media.AVProVideo
{
    public class DummyControl : MonoBehaviour
    {

        public GameObject gVideoDummy;

        public int iNowCnt;        

        public GameObject[] Pages;

        

        // Use this for initialization
        void Start()
        {
            Initialize();
        }

        public void Next()
        {

            iNowCnt++;
            ChangeVideo(iNowCnt);
            Debug.Log("iNowCnt : " + iNowCnt);
        }

        public void Prev()
        {
            iNowCnt--;
            ChangeVideo(iNowCnt);
        }

        public void ChangeVideo(int num)
        {
            gVideoDummy.GetComponentInChildren<MediaPlayer>().m_VideoPath = "Dummy/" + num.ToString() + ".mp4";
            //gVideoDummy.GetComponentInChildren<MediaPlayer>().OpenVideoFromFile();
            gVideoDummy.GetComponentInChildren<MediaPlayer>().OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, gVideoDummy.GetComponentInChildren<MediaPlayer>().m_VideoPath);
            Pages[num].SetActive(true);
            Pages[num - 1].SetActive(false);            
        }

        public void Initialize()
        {
            iNowCnt = 0;            
            Pages[0].SetActive(true);
            for (int i = 1; i < Pages.Length; i++)
            {
                Pages[i].SetActive(false);
            }
        }

        public void ExitApp()
        {
            Application.Quit();
        }

    }
}
