﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandController : MonoBehaviour
{
    public Animator _handAnimator;
    public Renderer _handRenderer;

    private Transform _mainCameraTr;

    public Transform _handTr;

    public GameObject[] _grabbedItem;

    public int _grabbedItemIdx;

    //private Coroutine _handFadingCo = null;
    private Coroutine _handRotatingCo = null;

    private void Start()
    {
        //GameObject labEscape = GameObject.Find("LabEscapeTest");

        _mainCameraTr = GameObject.FindGameObjectWithTag("MainCamera").transform;

        //if (labEscape.activeSelf == false || labEscape == null)
        if (_mainCameraTr == null)
        {
            Debug.Log("=============================");
            return;
        }

        for (int i = 0; i < _grabbedItem.Length; ++i)
        {
            _grabbedItem[i].SetActive(false);
        }

        //_handFadingCo = StartCoroutine(this.HandFadingCoroutine());
        _handRotatingCo = StartCoroutine(this.HandRotatingCoroutine());
    }

    // 고개를 아래로 내리면 손이 투명해지는 처리
    private IEnumerator HandFadingCoroutine()
    {
        Color handColor = Color.white;
        WaitForSeconds delay = new WaitForSeconds(0.1f);
        while(true)
        {
            if (30.0f < _mainCameraTr.eulerAngles.x && _mainCameraTr.eulerAngles.x < 270.0f)
            {
                //Debug.Log("===camera angle over 30 color.a: " + _handRenderer.material.color.a);
                handColor = _handRenderer.material.color;
                handColor.a -= 0.1f;

                if (0.0f >= handColor.a )
                {
                    handColor.a = 0.0f;
                }
                
                _handRenderer.material.color = handColor;
            }
            else
            {
                //Debug.Log("===camera angle under 30 color.a: " + _handRenderer.material.color.a);
                handColor = _handRenderer.material.color;
                handColor.a += 0.1f;
                if (1.0f <= handColor.a)
                {
                    handColor.a = 1.0f;
                }
                _handRenderer.material.color = handColor;
            }

            yield return delay;
        }        
    }

    // 카메라가 Y축으로 회전하면 손도 따라서 움직이는 처리
    private IEnumerator HandRotatingCoroutine()
    {
        Vector3 _handEulerAngles = Vector3.zero;        
        while (true)
        {
            _handEulerAngles = _mainCameraTr.eulerAngles;
            _handEulerAngles.x = 0.0f;
            _handEulerAngles.z = 0.0f;
            _handTr.eulerAngles = _handEulerAngles;

            yield return null;
        }
    }

    public void DisableAllGrabbedItem()
    {
        for (int i = 0; i < _grabbedItem.Length; ++i)
        {
            _grabbedItem[i].SetActive(false);
        }
    }

    public void ActiveGrabbedItemGoggle()
    {
        _grabbedItem[0].SetActive(true);
    }

    public void DisableGrabbedItemGoggle()
    {
        _grabbedItem[0].SetActive(false);
    }

    public void ActiveGrabbedItemGlove()
    {
        _grabbedItem[1].SetActive(true);
    }

    public void DisableGrabbedItemGlove()
    {
        _grabbedItem[1].SetActive(false);
    }

    public void ActiveGrabbedItemLabCoat()
    {
        _grabbedItem[2].SetActive(true);
    }

    public void DisableGrabbedItemLabCoat()
    {
        _grabbedItem[2].SetActive(false);
    }

    public void ActiveGrabbedItemShoes()
    {
        _grabbedItem[3].SetActive(true);
    }

    public void DisableGrabbedItemShoes()
    {
        _grabbedItem[3].SetActive(false);
    }
}