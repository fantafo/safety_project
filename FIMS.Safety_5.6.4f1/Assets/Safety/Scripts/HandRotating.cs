﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandRotating : MonoBehaviour
{
    private Transform _mainCameraTr;
    private Transform _handTr;

	private void Start ()
    {
        _mainCameraTr = Camera.main.transform;
        _handTr = transform;
        StartCoroutine(this.HandRotatingCoroutine());	
	}

    // 카메라가 Y축으로 회전하면 손도 따라서 움직이는 처리
    private IEnumerator HandRotatingCoroutine()
    {
        Vector3 _handEulerAngles = Vector3.zero;

        while (true)
        {
            _handEulerAngles = _mainCameraTr.eulerAngles;
            _handEulerAngles.x = 0.0f;
            _handEulerAngles.z = 0.0f;
            _handTr.eulerAngles = _handEulerAngles;

            yield return null;
        }
    }
}
