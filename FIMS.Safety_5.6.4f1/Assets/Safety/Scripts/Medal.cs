﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medal : MonoBehaviour
{
    private Animation _anim;

    private void Start()
    {
        _anim = this.gameObject.GetComponent<Animation>();
        StartCoroutine(this.AnimationPlayTimeCheckCoroutine());
    }

    private IEnumerator AnimationPlayTimeCheckCoroutine()
    {
        float currentAnimationTime = 0.0f;

        while (_anim.clip.length > currentAnimationTime)
        {
            currentAnimationTime += Time.deltaTime;

            Debug.Log("currentAnimationTime: " + currentAnimationTime);
            
            yield return null;
        }

        Debug.Log("currentAnimationTime: " + currentAnimationTime);
        this.gameObject.SetActive(false);
    }
}
