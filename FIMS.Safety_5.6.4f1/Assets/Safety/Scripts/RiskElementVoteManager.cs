﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using UnityEngine.UI;

namespace FTF
{
    /// <summary>
    /// 위험요소 투표 메뉴를 관리하는 매니저.
    /// </summary>
    public class RiskElementVoteManager : AutoCommander
    {
        //public float _zPos = -0.552243f;
        //public float _yPos = 1.145f;
        public GameObject _poll;

        public GameObject _titleBox;
        public GameObject _msgBoxVoteStart;
        public GameObject _msgBoxVoteEnd;

        public Transform _voteItemTr;
        public GameObject[] _voteItems;
        public Button[] _voteButtons;
        public Image[] _voteImages;
        public Image[] _voteRatingGraph;
        public Text[] _voteCounterText;

        // 버튼을 누를때 마다 체크와 취소를 번걸아 가면서 하기 위한 bool배열
        private bool[] VotePick;

        public Text _voteTimerText;
        public CustomTimer _customTimer;

        
        public int[] _voteCount;

        private int TotalStudentCount = 6;
        private const float MAX_VOTE_TIME = 45.0f;

        private float _currentVoteTime = MAX_VOTE_TIME;

        private bool _isVoting = false;

        private float _visibleTimeOfMsgBox = 6.5f;

        public bool _isFinished = false;

        private float _buttonOriginPosY = 0.0f;

        private Coroutine _titleBoxDisableCo = null;
        private Coroutine _msgBoxActiveSetCo = null;
        private Coroutine[] _voteCounterTextFadeOutCo = null;
        private Coroutine _moveButtonsToPlayerCo = null;
        private Coroutine _buttonSortingCo = null;

        public AudioSource _buttonFx;
        public AudioSource _bgmSound;

        public Sprite _defaultButtonImage;
        public Sprite _checkedButtonImage;

        protected override void Awake()
        {
            base.Awake();
            _voteCounterTextFadeOutCo = new Coroutine[_voteItems.Length];
            _isVoting = false;

            // 위험요소별 투표수 체크용 배열
            _voteCount = new int[10];
            _voteCount.Initialize();

            _titleBox.SetActive(true);
            _msgBoxVoteStart.SetActive(false);
            _msgBoxVoteEnd.SetActive(false);

            VotePick = new bool[10];
            for(int i = 0; i< VotePick.Length; i++)
            {
                VotePick[i] = false;
            }
        }
        private void Start()
        {
            if (null != _titleBoxDisableCo)
            {
                StopCoroutine(_titleBoxDisableCo);
            }            
            _titleBoxDisableCo = StartCoroutine(this.TitleBoxDisableCoroutine());

            SetAllButtonsEnabled(false);

            // 현재 참가인원값 설정
            TotalStudentCount = 1;
            if (0 >= TotalStudentCount)
            {
                TotalStudentCount = 1;
            }

            if (null != _buttonSortingCo)
            {
                StopCoroutine(_buttonSortingCo);
            }
            _buttonSortingCo = StartCoroutine(this.ButtonSortingCoroutine());
        }

        private IEnumerator ButtonSortingCoroutine()
        {
            // 메인 카메라와의 거리에 따라 그려지는 순서를 정렬하는 코루틴
            int itemCnt = _voteItems.Length;
            float[] distances = new float[_voteItems.Length];
            Transform _mainCam = Camera.main.transform;
            Transform _buttonTr = null;
            while (true)
            {
                for (int i = 0; i < itemCnt; ++i)
                {
                    _buttonTr = _voteItemTr.GetChild(i);
                    distances[i] = Vector3.Distance(_mainCam.position, _buttonTr.position);                    
                }

                float temp = 0.0f;
                for (int i = 0; i < itemCnt-1; ++i)
                {
                    for (int j = 0;j < itemCnt-1; ++j)
                    {
                        if (distances[j] < distances[j+1])
                        {
                            // 카메라와의 거리를 오름차순으로 정렬
                            temp = distances[j];
                            distances[j] = distances[j + 1];
                            distances[j + 1] = temp;
                            
                            Transform tr_j = _voteItemTr.GetChild(j);
                            Transform tr_j1 = _voteItemTr.GetChild(j+1);

                            int tmp = tr_j.GetSiblingIndex();
                            tr_j.SetSiblingIndex(tr_j1.GetSiblingIndex());
                            tr_j1.SetSiblingIndex(tmp);
                        }
                    }
                }

                yield return null;
            }            
        }

        private IEnumerator TitleBoxDisableCoroutine()
        {
            Debug.Log("TitleBoxDisableCoroutine()");
            //yield return new WaitForSeconds(3.0f);
            yield return new WaitForSeconds(_visibleTimeOfMsgBox);
            _titleBox.SetActive(false);

            if (null != _msgBoxActiveSetCo)
            {
                StopCoroutine(_msgBoxActiveSetCo);
            }            
            _msgBoxActiveSetCo = StartCoroutine(this.MsgBoxActiveSetCoroutine());
        }

        private IEnumerator MsgBoxActiveSetCoroutine()
        {
            Debug.Log("MsgBoxActiveSetCoroutine()");
            yield return new WaitForSeconds(3.0f);
            _msgBoxVoteStart.GetComponentInChildren<Text>().text =
                "실험실에서 일어날 수 있는 사고는 어떤 것이 있을지 모두 골라보세요.\n(남은 시간 " + MAX_VOTE_TIME.ToString() + " 초)";
            _msgBoxVoteStart.SetActive(true);
            yield return new WaitForSeconds(_visibleTimeOfMsgBox);
            _msgBoxVoteStart.SetActive(false);

            SetAllButtonsEnabled(true);

            //StopCoroutine(this.VoteTimerCoroutine());
            //StartCoroutine(this.VoteTimerCoroutine());
            

            _isVoting = true;

            VoteTimerStart();
        }

        private IEnumerator VoteCounterTextFadeOutCoroutine(int index)
        {
            WaitForSeconds delayTime = new WaitForSeconds(0.05f);
            Color textColor = Color.white;
            Vector3 textLocalScale = Vector3.one;

            //_voteCounterText[index].color = Color.white;
            //_voteCounterText[index].transform.localScale = Vector3.one;

            while (_voteCounterText[index].color.a > 0.0f)
            {
                textColor = _voteCounterText[index].color;
                textColor.a -= 0.01f;
                _voteCounterText[index].color = textColor;

                textLocalScale = _voteCounterText[index].transform.localScale;
                textLocalScale.x += 0.0001f;
                textLocalScale.y += 0.0001f;
                textLocalScale.z += 0.0001f;
                _voteCounterText[index].transform.localScale = textLocalScale;

                yield return null;
            }

            yield return null;
            textColor.a = 1.0f;            
            _voteCounterText[index].color = textColor;
            _voteCounterText[index].transform.localScale = (Vector3.one * 0.01f);
        }

        private void SetAllButtonsEnabled(bool isEnable)
        {
            Debug.Log("SetAllButtonsEnabled");
            for (int i = 0; i < _voteButtons.Length; ++i)
            {
                if (_voteButtons[i] == null)
                {
                    continue;
                }

                _voteButtons[i].enabled = isEnable;
            }
        }

        // 투표 버튼 눌렀을 때 처리할 함수
        public void BroadcastOnClickVoteButton(int index)
        {
            Debug.Log("BroadcastOnClickVoteButton" + index + " " + !VotePick[index]);

            VotePick[index] = !VotePick[index];


            // 내가 누른 버튼은 비활성화(내 클라이언트에서만)
            //_voteButtons[index].enabled = false;
            //_voteButtons[index].interactable = false;
            // 선택과 선택완료 이미지 교체
            if (!VotePick[index])
                _voteButtons[index].GetComponent<Image>().sprite = _defaultButtonImage;
            else
                _voteButtons[index].GetComponent<Image>().sprite = _checkedButtonImage;
            Broadcast("CmdOnClickVoteButton", index, VotePick[index]);
        }
        [OnCommandAttribute]
        public void CmdOnClickVoteButton(int index, bool pick)
        {
            // 버튼 눌렸을 때 모든 사용자에게 보여져야 할 내용들
            Debug.Log("CmdOnClickVoteButton" + index);
            SLog.Debug("RiskElementVote", "OnClickVote");
            
            if(pick)
                _voteCount[index]++;
            else
                _voteCount[index]--;
            
            
            _voteCounterText[index].text = _voteCount[index].ToString();
            _voteRatingGraph[index].fillAmount = (float)_voteCount[index] / (float)TotalStudentCount;

            if (null != _voteCounterTextFadeOutCo[index])
            {
                StopCoroutine(_voteCounterTextFadeOutCo[index]);
            }             
            _voteCounterTextFadeOutCo[index] = StartCoroutine(this.VoteCounterTextFadeOutCoroutine(index));
        }

        // 투표 타이머(내가 만든 버전)
        private IEnumerator VoteTimerCoroutine()
        {
            Debug.Log("VoteTimerCoroutine Start");

            while (_currentVoteTime > 0.0f)
            {
                _currentVoteTime -= Time.deltaTime;
                _voteTimerText.text = _currentVoteTime.ToString("N1");
                yield return null;
            }
            ///////////////////
            // 타이머 종료시
            ///////////////////
            for (int i = 0; i < _voteButtons.Length; ++i)
            {
                if (_voteButtons[i] == null)
                {
                    continue;
                }
                _voteButtons[i].enabled = false;
            }

            //MoveButtonsToPlayer();
            if (null != _moveButtonsToPlayerCo)
            {
                StopCoroutine(_moveButtonsToPlayerCo);
            }             
            _moveButtonsToPlayerCo = StartCoroutine(this.MoveButtonsToPlayerCoroutine());
            Debug.Log("VoteTimerCoroutine End");
        }

        // custom timer 시작용 함수
        public void VoteTimerStart()
        {
            Debug.Log("VoteTimerStart");

            //_buttonOriginPosY = _voteButtons[0].transform.position.y;
            _buttonOriginPosY = _voteItems[0].transform.position.y;

            _customTimer.StartTimer();

            // 배경음악 재생 시작
            _bgmSound.Play();
        }

        // custom timer에서 시간이 끝났을 때 호출하는 함수
        public void VoteTimerEnd()
        {
            _isVoting = false;

           
            for (int i = 0; i < _voteButtons.Length; ++i)
            {
                if (_voteButtons[i] == null)
                {
                    continue;
                }

                // 투표 버튼 비활성화
                _voteButtons[i].enabled = false;
            }

            //MoveButtonsToPlayer();
            if (null != _moveButtonsToPlayerCo)
            {
                StopCoroutine(_moveButtonsToPlayerCo);
            }            
            _moveButtonsToPlayerCo = StartCoroutine(this.MoveButtonsToPlayerCoroutine());
        }

        private void MoveButtonsToPlayer()
        {
            //Transform mainPlayer = GameObject.Find("MainPlayer").GetComponent<Transform>();
            Transform mainPlayer = GameObject.Find("PlayerCameraRig").GetComponent<Transform>();
            Vector3 movePos = Vector3.zero;

            //for (int i = 0; i < _voteButtons.Length; ++i)
            for (int i = 0; i < _voteItems.Length; ++i)
            {
                //if (_voteButtons[i] == null)
                if (_voteItems[i] == null)
                {
                    continue;
                }

                movePos = mainPlayer.position - transform.position;
                movePos.y = transform.position.y;

                //iTween.MoveTo(_voteButtons[i].gameObject, iTween.Hash(
                iTween.MoveTo(_voteItems[i].gameObject, iTween.Hash(
                    "position", (movePos.normalized * 0.1f),
                    "time", 2.0f,
                    "delay", 0.0f,
                    "easeType", iTween.EaseType.linear));
            }            
        }

        

        // 투표 끝나고 결과 보여주는 연출 코루틴
        private IEnumerator MoveButtonsToPlayerCoroutine()
        {
            //Transform mainPlayer = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
            
            // 투표 끝났음을 알리는 메시지 박스 띄우기
            _msgBoxVoteEnd.SetActive(true);

            yield return new WaitForSeconds(8.0f);

            _msgBoxVoteEnd.SetActive(false);

            // 득표수가 1표 이하인 항목들 페이드 아웃
            //for (int i = 0; i < _voteCount.Length; ++i)
            //{
            //    if (1 >= _voteCount[i])
            //    {
            //
            //    }
            //}

            //3등까지만 나오도록 하는 부분
            int[] voteCountCopy;
            voteCountCopy = (int[])_voteCount.Clone();
            Array.Sort(voteCountCopy);
            Array.Reverse(voteCountCopy);
            int thirdVotePoint = voteCountCopy[2];
            
            


            Vector3[] targetPos = new Vector3[_voteItems.Length];
            
            Vector3[] startPos = new Vector3[_voteItems.Length];

            float[] speed = new float[_voteItems.Length];
            float[] startTime = new float[_voteItems.Length];
            float[] journeyLength = new float[_voteItems.Length];

            int maxCnt = 0;
            int cnt = 0;

            for (int i = 0; i < _voteItems.Length; ++i)
            {
                if ( null == _voteItems[i])
                {
                    continue;
                }
                Debug.Log("VoteItem " + i + " " + _voteItems[i].transform.localPosition);

                    // 버튼의 시작위치
                startPos[i] = _voteItems[i].transform.localPosition;

                //Vector3 destPos = mainPlayer.position;
                //destPos.x = startPos[i].x;
                //destPos.y = startPos[i].y;

                // 버튼의 이동속도
                speed[i] = 0.2f;

                // 버튼이 이동할 때의 시작시간
                startTime[i] = Time.time;

                // 이동해야 할 위치
                targetPos[i] = startPos[i];
                if (thirdVotePoint <= _voteCount[i])
                {
                    targetPos[i].z -= (2.0f / TotalStudentCount) * _voteCount[i];
                    ++maxCnt;      
                }

                // (이동하는 최대 거리 / 전체인원수) * 투표인원수

                // 이동해야 할 거리
                journeyLength[i] = Vector3.Distance(startPos[i], targetPos[i]);
                Debug.Log("targetPos[" + i + "].z: " + targetPos[i].z + " journeyLength: " + journeyLength[i]);
                //journeyLength[i] = Vector3.Distance(startPos[i], mainPlayer.position);
                //journeyLength[i] = Vector3.Distance(startPos[i], destPos);
          
            }

            cnt = 0;
            while (cnt < maxCnt)
            {
                for (int i = 0; i < _voteItems.Length; ++i)
                {
                    
                    
                    if ( null == _voteItems[i])
                    {                        
                        continue;
                    }
                    if (thirdVotePoint > _voteCount[i])
                    {
                        _voteItems[i].GetComponent<CanvasGroup>().alpha -= 0.01f;

                    }
                    else
                    {
                        //Vector3 destPos = mainPlayer.position;
                        //destPos.x = startPos[i].x;
                        //destPos.y = startPos[i].y;

                        float distCovered = (Time.time - startTime[i]) * speed[i];
                        float fracJourney = distCovered / journeyLength[i];
                       // Debug.Log("오류 나는 부분 _voteItems[" + i+ "] : " + _voteItems[i].transform.localPosition);
                        _voteItems[i].transform.localPosition = Vector3.Lerp(startPos[i], targetPos[i], fracJourney);
                        //_voteButtons[i].transform.position = Vector3.Lerp(startPos[i], mainPlayer.position, fracJourney);
                        //_voteButtons[i].transform.position = Vector3.Lerp(startPos[i], destPos, fracJourney);

                        if (1.0f < fracJourney)
                        {
                            ++cnt;
                        }
                    }
                }

                /*
                if (cnt >= maxCnt)
                {
                    //Debug.Log("fracJourney: " + fracJourney);
                    yield break;
                }
                */
                
                yield return null;
            }

            // 3초뒤 투표창 닫음
            yield return new WaitForSeconds(3.0f);
            _isFinished = true;
            _poll.SetActive(false);            
        }

        // 포인터가 투표항목 위에 올라왔을 때 처리하는 함수
        public void OnPointerEnterButton(int index)
        {
            if (_isVoting)
            {
                //ButtonMoveToPlayer(index);
                VoteImageMoveUp(index);
                if (_buttonFx.isPlaying)
                {
                    _buttonFx.Stop();
                }

                _buttonFx.Play();
            }            
        }

        // 포인터가 투표항목 위에서 벗어났을 때 처리하는 함수
        public void OnPointerExitButton(int index)
        {
            if (_isVoting)
            {
                //ButtonAwayFromPlayer(index);
                VoteImageMoveDown(index);
            }            
        }

        private void VoteImageMoveUp(int index)
        {
            if (index < 5)
                _buttonOriginPosY = 5.0f;
            else
                _buttonOriginPosY = -2.1f;
            iTween.MoveTo(
                _voteImages[index].gameObject,
                iTween.Hash(                    
                    "y", _buttonOriginPosY + 0.5f,
                    "time", 1.0f,
                    "delay", 0.0f,
                    "onupdate", "myUpdateFunction",
                    "looptype", iTween.LoopType.none));
        }

        private void VoteImageMoveDown(int index)
        {
            if (index < 5)
                _buttonOriginPosY = 5.0f;
            else
                _buttonOriginPosY = -2.1f;
            iTween.MoveTo(
                _voteImages[index].gameObject,
                iTween.Hash(
                    "y", _buttonOriginPosY,
                    "time", 5.0f,
                    "delay", 0.0f,
                    "onupdate", "myUpdateFunction",
                    "looptype", iTween.LoopType.none));
        }

        private void ButtonMoveToPlayer(int index)
        {
            iTween.MoveTo(
                _voteButtons[index].gameObject,
                iTween.Hash(
                    //"z", -0.5f,
                    //"y", _buttonOriginPosY + 0.2f,
                    //"y", _buttonOriginPosY + 1.0f,
                    "y", _buttonOriginPosY + 0.5f,
                    "time", 1.0f,
                    "delay", 0.0f,
                    "onupdate", "myUpdateFunction",
                    "looptype", iTween.LoopType.none));
        }

        private void ButtonAwayFromPlayer(int index)
        {
            iTween.MoveTo(
                _voteButtons[index].gameObject,
                iTween.Hash(
                    //"z", -0.552243f,
                    "y", _buttonOriginPosY,
                    "time", 5.0f,
                    "delay", 0.0f,
                    "onupdate", "myUpdateFunction",
                    "looptype", iTween.LoopType.none));
        }
    }    
}