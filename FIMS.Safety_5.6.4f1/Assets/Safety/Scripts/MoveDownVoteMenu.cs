﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class MoveDownVoteMenu : MonoBehaviour
{
    public Transform _pollTr;

    private Hashtable _moveDownHt = null;
    private Hashtable _fadeInHt = null;
    private Transform _tr;
    private Image _img;
    private Color _originColor;
    private Color _currentColor;

    private Vector3 _originPos = Vector3.zero;

    private float _targetValue = 1.145f;

    public float _delayValue = 0.0f;

    

    private void Awake()
    {
        _tr = transform;
        _originPos = _tr.position;
        _img = this.gameObject.GetComponentInChildren<Image>();
        _originColor = _img.color;
        _currentColor = _originColor;

        _moveDownHt = new Hashtable();

        _moveDownHt.Add("y", _targetValue);
        _moveDownHt.Add("time", 2);
        _moveDownHt.Add("delay", _delayValue);
        _moveDownHt.Add("onupdate", "myUpdateFunction");
        _moveDownHt.Add("looptype", iTween.LoopType.none);

        _fadeInHt = new Hashtable();

        _fadeInHt.Add("from", 0.0f);
        _fadeInHt.Add("to", 1.0f);
        _fadeInHt.Add("time", _delayValue + 1.5f);
        _fadeInHt.Add("easetype", "linear");
        _fadeInHt.Add("delay", _delayValue);
        _fadeInHt.Add("onupdate", "setAlpha");
    }

    private void Start()
    {
        this.enabled = true;
        /*
        _tr = transform;
        _originPos = _tr.position;
        _img = this.gameObject.GetComponent<Image>();
        _originColor = _img.color;
        _currentColor = _originColor;

        _moveDownHt = new Hashtable();

        _moveDownHt.Add("y", _targetValue);
        _moveDownHt.Add("time", 2);
        _moveDownHt.Add("delay", _delayValue);
        _moveDownHt.Add("onupdate", "myUpdateFunction");
        _moveDownHt.Add("looptype", iTween.LoopType.none);

        _fadeInHt = new Hashtable();

        _fadeInHt.Add("from", 0.0f);
        _fadeInHt.Add("to", 1.0f);
        _fadeInHt.Add("time", _delayValue + 1.5f);
        _fadeInHt.Add("easetype", "linear");
        _fadeInHt.Add("delay", _delayValue);
        _fadeInHt.Add("onupdate", "setAlpha");
        */
    }


    public void setAlpha(float newAlpha)
    {
        _currentColor.a = newAlpha;

        _img.color = _currentColor;
    }

    private void OnEnable()
    {
        //iTween.MoveTo(gameObject, _moveDownHt);
        iTween.MoveBy(gameObject, iTween.Hash(
            "y", -1.5f,
            "time", 2.0f,
            "delay", _delayValue
            ));
        iTween.ValueTo(gameObject, _fadeInHt);
        //iTween.FadeTo(gameObject, _fadeInHt);
    }

    private void OnDisable()
    {
        _tr.position = _originPos;
        _img.color = _originColor;
    }
}