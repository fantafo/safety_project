﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RenderHeads.Media.AVProVideo
{
    public class ChangeVideo : MonoBehaviour
    {

        public GameObject[] gVideo_;

        public GameObject hVideo;

        int cnt = 0;

        // Use this for initialization
        void Start()
        {
            //Instantiate(gVideo_[0]);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ChangePrefabs()
        {
            //if (GameObject.FindWithTag("Video") != null)
            //{
            //    Destroy(GameObject.FindWithTag("Video"));
            //}

            //if (cnt==0)
            //{
            //    Instantiate(gVideo_[0]);
            //}
            Debug.Log(hVideo.GetComponentInChildren<MediaPlayer>().m_VideoPath);
            if (hVideo.GetComponentInChildren<MediaPlayer>().m_VideoPath.Contains("rGreen"))
            {
                hVideo.GetComponentInChildren<MediaPlayer>().m_VideoPath = "Sample/se003_injected.mp4";
            }
            else
            {
                hVideo.GetComponentInChildren<MediaPlayer>().m_VideoPath = "Sample/rGreen.mp4";
            }

            //hVideo.GetComponentInChildren<MediaPlayer>().OpenVideoFromFile();
            hVideo.GetComponentInChildren<MediaPlayer>().OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, hVideo.GetComponentInChildren<MediaPlayer>().m_VideoPath);


        }
    }
}
