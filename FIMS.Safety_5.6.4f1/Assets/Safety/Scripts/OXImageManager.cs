﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTF;
using UnityEngine.UI;

public class OXImageManager : MonoBehaviour
{
    public PlayerInfo _info;

    public GameObject _O_image;
    public GameObject _X_image;

    private Coroutine _co = null;

    private SafetyTrainingSceneManager _safetyTrainingSceneManager;

    private void Start()
    {
        if (null != _O_image)
        {
            _O_image.SetActive(false);
        }
        if (null != _X_image)
        {
            _X_image.SetActive(false);
        }

        _safetyTrainingSceneManager = GameObject.Find("SafetyTrainingSceneManager").GetComponent<SafetyTrainingSceneManager>();
    }

    private void OnEnable()
    {
        if (null != _co)
        {
            StopCoroutine(_co);
        }
        _co = StartCoroutine(this.ChangeAnswerChecker());
    }
    /*
    private void OnDisable()
    {
        if (null != _co)
        {
            StopCoroutine(_co);
        }
    }
    */

    private IEnumerator ChangeAnswerChecker()
    {
        //WaitForSeconds delay = new WaitForSeconds(0.5f);

        while (true)
        {
            if (null == _safetyTrainingSceneManager)
            {
                yield return Waits.Wait(0.5f);
                continue;
            }

            if (!_safetyTrainingSceneManager._isOXQuizStart)
            {
                _O_image.SetActive(false);
                _X_image.SetActive(false);
                yield return Waits.Wait(0.5f);
                continue;
            }

            // OX 퀴즈가 전부 끝났으면
            if (_safetyTrainingSceneManager._isOXQuizEnd)
            {
                _O_image.SetActive(false);
                _X_image.SetActive(false);
                //StopCoroutine(_co);
                yield return Waits.Wait(0.5f);
                continue;
            }

            switch (_info.instance.quiz)
            {
                case 0:
                    {
                        _O_image.SetActive(false);
                        _X_image.SetActive(false);
                    }
                    break;
                case 1:
                    {
                        _O_image.SetActive(true);
                        _X_image.SetActive(false);
                    }
                    break;
                case 2:
                    {
                        _O_image.SetActive(false);
                        _X_image.SetActive(true);
                    }
                    break;
                default:
                    {
                        _O_image.SetActive(false);
                        _X_image.SetActive(false);
                    }
                    break;
            }

            //yield return delay;
            yield return Waits.Wait(0.5f);
        }
    }
}