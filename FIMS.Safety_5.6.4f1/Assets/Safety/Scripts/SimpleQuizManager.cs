﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

using UnityEngine.UI;

//public class SimpleQuizManager : AutoCommander
public class SimpleQuizManager : QuizManager
{
    //public bool _isFinished = false;
    public GameObject[] _warmUpCheckImageObj;
    public GameObject _ResultImage;
    public GameObject _ResultFail_Image;
    public ParticleSystem _ScoreParticle;//0731김영민 추가. 내 점수 부분 이펙트.
    public GameObject[] _effectBtn;

    public int _rightAnswer;

    public GameObject _correctAnswerSignImageObj;

    public AudioSource _buttonFx;
    public AudioSource _clockTickBgm;

    private Coroutine _waitingCo = null;
    private Coroutine _onQuizCo = null;
    private Coroutine _showCorrectAnswerCo = null;
    private Button[] _simpleQuizButtons;    
    private CustomTimer _timer;
    private int _answerNum = -1;

    private GameObject _commentaryBGImageObj;

    //0821 김영민 추가
    public Button[] _answerBtn;
    private bool _onQuiz = false;

    private void Start()
    {
        _simpleQuizButtons = this.gameObject.GetComponentsInChildren<Button>();
        _commentaryBGImageObj = transform.Find("CommentaryBGImage").gameObject;
        if ( null == _correctAnswerSignImageObj )
        {
            _correctAnswerSignImageObj = transform.Find("CorrectAnswerSignImage").gameObject;
        }
        

        if (null != _commentaryBGImageObj)
        {
            _commentaryBGImageObj.SetActive(false);
        }

        for (int i = 0; i < _effectBtn.Length; i++)
        {
            //버튼 테두리 초기화
            _effectBtn[i].SetActive(false);
            _answerBtn[i].enabled = false;//0821

            // 선택한 답 체크용 V 초기화
            _warmUpCheckImageObj[i].SetActive(false);
        }
        if (null != _waitingCo)
        {
            StopCoroutine(_waitingCo);
        }

        if (null != _correctAnswerSignImageObj)
        {
            _correctAnswerSignImageObj.SetActive(false);
        }       

        _timer = this.gameObject.GetComponentInChildren<CustomTimer>();

        /*
        if (null != _onQuizCo)
        {
            StopCoroutine(_onQuizCo);
        }
        _onQuizCo = StartCoroutine(this.onQuizCo());
        */
        //_timer.StartTimer();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        if (null != _onQuizCo)
        {
            StopCoroutine(_onQuizCo);
        }
        _onQuizCo = StartCoroutine(this.onQuizCo());
    }

    protected override void OnDisable()
    {
        if (null != _onQuizCo)
        {
            StopCoroutine(_onQuizCo);
        }

        base.OnDisable();
    }

    // 퀴즈 시작되면 3초의 유예시간을 두고 시작하게 하는 코루틴
    private IEnumerator onQuizCo()//0821
    {
        yield return new WaitForSeconds(3.0f);

        // 시계 소리 재생
        _clockTickBgm.Stop();
        if (!_clockTickBgm.loop)
        {
            _clockTickBgm.loop = true;
        }
        _clockTickBgm.Play();

        for (int i = 0; i < _answerBtn.Length; i++)
        {
            _answerBtn[i].enabled = true;
        }
        _onQuiz = true;
        _timer.StartTimer();
    }

    private IEnumerator WaitingCoroutine()
    {
        yield return new WaitForSeconds(5.0f);
        Broadcast("QuizFinished", PlayerInstance.main.instanceId);
    }

    public void BroadcastQuizFinished()
    {
        //Broadcast("QuizFinished", PlayerInstance.main.instanceId);
        
        // 시계 소리 정지
        _clockTickBgm.Stop();

        for (int i = 0; i < _simpleQuizButtons.Length; ++i)
        {
            _simpleQuizButtons[i].interactable = false;
        }

        BroadcastQuizScoreProcess(_answerNum);

        if (null != _waitingCo)
        {
            StopCoroutine(_waitingCo);
        }
        _waitingCo = StartCoroutine(this.WaitingCoroutine());
    }
    [OnCommand]
    private void QuizFinished(int instanceId)
    {
        //if (PlayerInstance.main.IsRoomMaster)
        {
            _isFinished = true;
        }
    }

    public void OnClickQuizAnswerButton(int answerNum)
    {
        Debug.Log("====SimpleQuiz Answer Num: " + answerNum);

        // 퀴즈 버튼 눌리면 누른 사람의 버튼은 전부 비활성화 되도록.
        //for (int i = 0; i < _simpleQuizButtons.Length; ++i)
        //{
        //    _simpleQuizButtons[i].interactable = false;
        //}
        //BroadcastQuizScoreProcess(answerNum);

#if FTF_OBSERVER
#else
        if(PlayerInstance.main.IsRoomMaster)
        {
            Broadcast("OnClickQuizAnswer", PlayerInstance.main.instanceId, answerNum);
        }
#endif
        _answerNum = answerNum;

        // 누른 퀴즈 버튼은 다른 버튼 눌리기 전 까지 비활성화
        for (int i = 0; i < _simpleQuizButtons.Length; ++i)
        {
            _simpleQuizButtons[i].interactable = true;

            _warmUpCheckImageObj[i].SetActive(false);
        }

        _simpleQuizButtons[answerNum].interactable = false;

        // 선택한 답안 표시용 체크
        _warmUpCheckImageObj[answerNum].SetActive(true);
    }

    [OnCommand]
    private void OnClickQuizAnswer(int id, int answerNum)
    {
#if FTF_OBSERVER
        if (id == PlayerInstance.lookMain.instanceId)
        {        
            OnClickQuizAnswerButton(answerNum);
        }
#endif
    }

    private void BroadcastQuizScoreProcess(int answerNum)
    {
        if (_rightAnswer == answerNum)
        {
            Networker.Send(C_Scene.UpdateScore(PlayerInstance.main.instanceId, C_Scene.UpdateType.Relative, 10));

            QuizResult(true);

            SafetyTrainingSceneManager.main.PlayScoreEffect();
        }
        else
        {
            QuizResult(false);
        }

        //Broadcast("QuizScoreProcess", PlayerInstance.main.instanceId, answerNum);
    }
    /*
    [OnCommand]
    private void QuizScoreProcess(int instanceId, int answerNum)
    {
        // 내가 선생님(방장)이면
        if (PlayerInstance.main.IsRoomMaster)
        {
            if (_rightAnswer == answerNum)
            {
                //Networker.Send(C_Scene.UpdateScore(instanceId, C_Scene.UpdateType.Relative, 10));
                //Broadcast("QuizResult", instanceId, true);

                //SafetyTrainingSceneManager.main.PlayScoreEffect();
            }
            else
            {
                //Networker.Send(C_Scene.UpdateScore(instanceId, C_Scene.UpdateType.Relative, 0));
                //Broadcast("QuizResult", instanceId, false);
            }
        }
    }
    */
    void QuizResult(bool result)
    {
        // 문제 해설 띄움
        if (null != _commentaryBGImageObj)
        {
            _commentaryBGImageObj.SetActive(true);
        }
        

        //if (PlayerInstance.main.instanceId == id || PlayerInstance.lookMain.instanceId == id)
        {
            if (result)
            {
                _ResultImage.SetActive(true);
                //PlayParticle();//0731김영민 추가. 내 점수 부분 이펙트.
            }
            else
            {
                _ResultFail_Image.SetActive(true);

                // 틀렸을 경우에 정답이 어떤 건지 알려 주는 이미지를 정답 위에 띄움
                if (null != _correctAnswerSignImageObj)
                {
                    _correctAnswerSignImageObj.transform.parent = _simpleQuizButtons[_rightAnswer].transform;

                    //_correctAnswerSignImageObj.GetComponent<RectTransform>().pivot = _warmUpCheckImageObj[_rightAnswer].GetComponent<RectTransform>().pivot;
                    //_correctAnswerSignImageObj.GetComponent<RectTransform>().anchorMin = _warmUpCheckImageObj[_rightAnswer].GetComponent<RectTransform>().anchorMin;
                    //_correctAnswerSignImageObj.GetComponent<RectTransform>().anchorMax = _warmUpCheckImageObj[_rightAnswer].GetComponent<RectTransform>().anchorMax;
                    
                    // 이미지 위치 설정
                    //_correctAnswerSignImageObj.transform.position = _warmUpCheckImageObj[_rightAnswer].transform.position;

                    Vector2 pos = Vector2.zero;
                    pos.x = 20.0f;
                    pos.y = 12.0f;

                    //_correctAnswerSignImageObj.transform.localPosition = pos;
                    _correctAnswerSignImageObj.GetComponent<RectTransform>().anchoredPosition = pos;


                    if (null != _showCorrectAnswerCo)
                    {
                        StopCoroutine(_showCorrectAnswerCo);
                    }
                    StartCoroutine(this.ShowCorrectAnswer());
                }                
            }
            _onQuiz = false;
        }
    }

    private IEnumerator ShowCorrectAnswer()
    {
        yield return new WaitForSeconds(3.0f);
        _ResultFail_Image.SetActive(false);
        // 이미지 화면에 보이도록 설정
        _correctAnswerSignImageObj.SetActive(true);
    }

    /*
    [OnCommand]
    void QuizResult(int id, bool result)
    {
        if (PlayerInstance.main.instanceId == id || PlayerInstance.lookMain.instanceId == id)
        {
            if (result)
            {
                _ResultImage.SetActive(true);
                //PlayParticle();//0731김영민 추가. 내 점수 부분 이펙트.
            }
            else
            {
                _ResultFail_Image.SetActive(true);
            }
            _onQuiz = false;
        }
    }
    */


    public void OnPointerEnterSimpleQuizButton(int index)
    {
#if FTF_OBSERVER
#else
        if(PlayerInstance.main.IsRoomMaster)
        {
            Broadcast("OnPointerEnter", PlayerInstance.main.instanceId, index);
        }
#endif
        if (_onQuiz)//0821
        {
            SimpleQuizButtonEnlargeScale(index);
            _effectBtn[index].SetActive(true);

            if (_buttonFx.isPlaying)
            {
                _buttonFx.Stop();
            }
            _buttonFx.Play();
        }
    }
    public void OnPointerExitSimpleQuizButton(int index)
    {
#if FTF_OBSERVER
#else
        if(PlayerInstance.main.IsRoomMaster)
        {
            Broadcast("OnPointerExit", PlayerInstance.main.instanceId, index);
        }
#endif
        if (_onQuiz)//0821
        {
            SimpleQuizButtonReturnToOriginScale(index);
            //if (_simpleQuizButtons[index].IsInteractable())
            {
                _effectBtn[index].SetActive(false);
            }
        }
    }

    [OnCommand]
    private void OnPointerEnter(int id, int index)
    {
#if FTF_OBSERVER
        if (id == PlayerInstance.lookMain.instanceId)
        {        
            OnPointerEnterSimpleQuizButton(index);
        }
#endif
    }
    [OnCommand]
    private void OnPointerExit(int id, int index)
    {
#if FTF_OBSERVER
        if (id == PlayerInstance.lookMain.instanceId)
        {        
            OnPointerExitSimpleQuizButton(index);
        }
#endif
    }

    private void SimpleQuizButtonEnlargeScale(int index)
    {
        iTween.ScaleTo(_simpleQuizButtons[index].gameObject, Vector3.one * 1.5f, 1.0f);
    }

    private void SimpleQuizButtonReturnToOriginScale(int index)
    {
        iTween.ScaleTo(_simpleQuizButtons[index].gameObject, Vector3.one, 1.0f);
    }

    //0731김영민 추가. 내 점수 부분 이펙트.
    public void PlayParticle()
    {
        if (_ScoreParticle)
        {
            if (_ScoreParticle.isPlaying == true)
            {
                _ScoreParticle.Stop();
                _ScoreParticle.Clear();
            }
            else
            {
                _ScoreParticle.Play();
            }
        }
    }
}