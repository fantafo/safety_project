﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowOnQuiz : MonoBehaviour {
    
    public GameObject ScoreBillboard;
    private void OnEnable()
    {
        ScoreBillboard.SetActive(false);
        StartCoroutine(ShowScoreBillboard());
    }

    private IEnumerator ShowScoreBillboard()
    {
        while (true)
        {
            if (GameObject.Find("Warm-upQuiz_PR_360_01"))
                break;
            yield return Waits.Wait(1);
        }

        ScoreBillboard.SetActive(true);
        yield return Waits.Wait(1);
    }
}
