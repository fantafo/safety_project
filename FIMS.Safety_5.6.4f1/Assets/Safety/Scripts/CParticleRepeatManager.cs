﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CParticleRepeatManager : MonoBehaviour
{
    public GameObject[] _particleObj;

    private ParticleSystem[] _ps;

    private void Start()
    {
        _ps = new ParticleSystem[_particleObj.Length];

        for (int i = 0; i < _ps.Length; ++i)
        {
            _ps[i] = _particleObj[i].GetComponent<ParticleSystem>();
        }

        StopCoroutine (this.ParticleObjRepeatCoroutine());
        StartCoroutine(this.ParticleObjRepeatCoroutine());
    }

    private IEnumerator ParticleObjRepeatCoroutine()
    {
        //WaitForSeconds duration = new WaitForSeconds(_particleObj.GetComponent<ParticleSystem>().main.duration);
        Debug.Log("ParticleObjRepeatCoroutine");
        while (true)
        {
            for (int i = 0; i < _ps.Length; ++i)
            {
                if (!_particleObj[i].activeSelf)
                {
                    _particleObj[i].SetActive(true);
                }

                else
                {
                    if (_ps[i].main.duration <= _ps[i].time)
                    {
                        _particleObj[i].SetActive(false);
                    }
                }                
            }

            yield return null;
        }        
    }    
}