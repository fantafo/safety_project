﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;

public class LogoutController : MonoBehaviour
{
    public GameObject _logoutButton;
    public GameObject _logoutButtonClickEffect;
    public GameObject _msg;
    private Text _msgText;

    public Text _remainTimeText;

    private Coroutine _enableLogoutButtonCoroutine;
    private Coroutine _startCountCoroutine;

    private const string _idFileName = "Id";
    private const string _logoutFileName = "Logout";
    private const string _logoutUnauthorizedFileName = "Logout_unauthorized";

    private const string _waitForOtherUsersStr = "다른 교육생을 기다리고 있습니다.";
    private const string _unableToLogoutStr = "현재 로그아웃을 할 수 없습니다.";
    private const string _logoutButtonClickedStr = "로그아웃 버튼을 클릭했습니다.";

    private const string _filePath = "/storage/emulated/0/Fantafo/Flags/";

    private string _startCount = string.Empty;
    private bool _isReadyToStart = false;

    private void Start()
    {
        if (null != _enableLogoutButtonCoroutine)
        {
            StopCoroutine(_enableLogoutButtonCoroutine);
        }

        if (null != _msg)
        {
            _msgText = _msg.GetComponentInChildren<Text>();
            _msgText.text = _waitForOtherUsersStr;
            _msg.SetActive(true);
        }

        _logoutButtonClickEffect.SetActive(false);

        _enableLogoutButtonCoroutine = StartCoroutine(this.EnableLogoutButtonCoroutine());
    }

    private IEnumerator EnableLogoutButtonCoroutine()
    {
        while (true)
        {
            //  Logout_unauthorized 파일 존재시 로그아웃 불가능 문구로 알려주기
            if (System.IO.File.Exists(_filePath + _logoutUnauthorizedFileName) && _isReadyToStart == false)
            {
                _isReadyToStart = true;

                _logoutButton.SetActive(false);

                _msgText.text = _unableToLogoutStr;
                _msg.SetActive(true);

                _startCount = File.ReadAllText(@_filePath + _logoutUnauthorizedFileName);

                if (null != _startCountCoroutine)
                {
                    StopCoroutine(_startCountCoroutine);
                }
                _startCountCoroutine = StartCoroutine(this.StartCountCoroutine());

                yield break;
            }

            // 로그아웃 UI 및 배치, 활성화시 Logout 생성
            else if (System.IO.File.Exists(_filePath + _idFileName))
            {
                _logoutButton.SetActive(true);
                _msg.SetActive(false);                
            }
            yield return null;
        }
    }

    private IEnumerator StartCountCoroutine()
    {
        float remainTime = float.Parse(_startCount) * 0.001f;

        while (remainTime > 0.0f)
        {
            remainTime -= Time.deltaTime;
            _remainTimeText.text = string.Format("{0:f0}", remainTime);
            yield return null;
        }
    }

    public void OnClickLogoutButton()
    {
        _msgText.text = _logoutButtonClickedStr;
        _msg.SetActive(true);

        _logoutButtonClickEffect.transform.position = _logoutButton.transform.position;
        _logoutButtonClickEffect.SetActive(true);

        if (!System.IO.File.Exists(_filePath + _logoutFileName))
        {
            FileStream f = new FileStream(_filePath + _logoutFileName, FileMode.CreateNew, FileAccess.Write);
            //StreamWriter sw = new StreamWriter(f, System.Text.Encoding.Unicode);
            StreamWriter sw = new StreamWriter( f );
            sw.Close();
            f.Close();
        }
    }

    public void OnPointerEnterButton()
    {
        iTween.ScaleTo(_logoutButton, Vector3.one * 1.5f, 1.0f);
    }

    public void OnPointerExitButton()
    {
        iTween.ScaleTo(_logoutButton, Vector3.one, 1.0f);
    }
}
