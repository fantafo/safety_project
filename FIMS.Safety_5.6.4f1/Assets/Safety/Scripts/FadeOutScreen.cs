﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;

using UnityEngine.UI;


public class FadeOutScreen : MonoBehaviour
{
    public Image _blackBillboard;
    public GameObject _particle = null;
    private Coroutine _co = null;

    public SafetyTrainingSceneManager _safetyTrainingSceneManager;   

    private void OnEnable()
    {
        if (_co != null)
        {
            StopCoroutine(_co);
        }
        _co = StartCoroutine(this.FadeOutScreenCoroutine());
    }

    private IEnumerator FadeOutScreenCoroutine()
    {
        Color billboardColor = _blackBillboard.color;
        billboardColor.a = 0.0f;
        _blackBillboard.color = billboardColor;

        WaitForSeconds fadeoutDelay = new WaitForSeconds(5.0f);
        WaitForSeconds d = new WaitForSeconds(0.1f);
        yield return fadeoutDelay;

        _particle.SetActive(false);

        while (1.0f > _blackBillboard.color.a)
        {
            billboardColor = _blackBillboard.color;
            billboardColor.a += 0.1f;
            _blackBillboard.color = billboardColor;
            yield return d;
        }

        // 안전장비착용으로 교체
        //_safetyTrainingScenemanager.ChangeFinder();

        //방장의 게임이 종료되었다고 알림 -> 패킷을 받으면 모두 page7로 넘어감
        //if (PlayerInstance.main.IsRoomMaster)
        {
            //_safetyTrainingSceneManager.BroadcastChangeFinder();
            // 여기에 찾기 끝나면 어떤 찾기 끝났는지 추가하기
            if ("HiddenFinder" == this.gameObject.transform.parent.name)
            {
                _safetyTrainingSceneManager.BroadcastHiddenFinderEnd();
            }
            else if ("CoinFinder" == this.gameObject.transform.parent.name)
            {
                _safetyTrainingSceneManager.BroadcastCoinFinderEnd();
            }
        }
        /*
        // 안전용구착용까지 끝났다고 알림
        if (PlayerInstance.main.IsRoomMaster)
        {
            _safetyTrainingSceneManager.BroadcastCoinFinderEnd();
        }
        */
    }


}
