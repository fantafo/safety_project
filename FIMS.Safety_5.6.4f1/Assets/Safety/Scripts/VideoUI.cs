﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoUI : MonoBehaviour {

    public float startTimer = 0.0f;
    public float endTimer = 0.0f;
    public AudioSource titleEffect;

    private const float soundDelay = 0.5f;

    public GameObject ui;

	// Use this for initialization
	void Start () {
        StartCoroutine(this.WaitingCoroutine());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //startTimer때 ui 생성, endTimer때 ui 제거
    private IEnumerator WaitingCoroutine()
    {
        if (ui == null)
            yield break;

        if (startTimer != 0.0f) {
            yield return new WaitForSeconds(startTimer- soundDelay);
        }
        if (titleEffect != null)
        {
            titleEffect.Play();
        }
        yield return new WaitForSeconds(soundDelay);
        ui.SetActive(true);

        if (startTimer < endTimer)
        {
            yield return new WaitForSeconds(endTimer - startTimer);
            ui.SetActive(false);
        }
    }
}
