﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorBlindUtility.UGUI
{
    public class Controler : MonoBehaviour
    {
        public ColorBlindMode colorBlindMode;
        public ColorBlindFilter colorBlindFilter;
        public int curFilter = 0;


        // Use this for initialization
        void Start()
        {
            curFilter = 0;
            colorBlindFilter = this.GetComponent<ColorBlindFilter>();            
            colorBlindFilter.colorBlindMode = ColorBlindMode.None;
        }

        public void ChangeFilter()
        {
            switch (curFilter)
            {
                case 0:
                    curFilter++;
                    colorBlindFilter.colorBlindMode = ColorBlindMode.Protanopia;
                    break;
                case 1:
                    curFilter++;
                    colorBlindFilter.colorBlindMode = ColorBlindMode.Deuteranopia;
                    break;
                case 2:
                    curFilter++;
                    colorBlindFilter.colorBlindMode = ColorBlindMode.Tritanopia;
                    break;
                case 3:
                    curFilter=0;
                    colorBlindFilter.colorBlindMode = ColorBlindMode.None;
                    break;
                default:
                    break;
            }
        }
    }
}